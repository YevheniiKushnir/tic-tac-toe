# General

Link on [site](https://tic-tac-toe-ugn1.onrender.com/)

## Technologies and structure

Project is monolith and has next structure:

- backend: express, mongoDB, socket.io
- frontend: react, router-dom, rtk and query, socket.io

## Author and other projects

- [https://github.com/zenia369](https://github.com/zenia369)
