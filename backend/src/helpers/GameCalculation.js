const { onlineGameService } = require("../services/profile");
const Users = require("../data/users");

function calculateWinner(squares) {
  const lines = [
    { variants: [0, 1, 2], line: "rotateZ(0deg) scale(1.1) translateY(-112px)" },
    { variants: [3, 4, 5], line: "rotateZ(0deg) scale(1.1) " },
    { variants: [6, 7, 8], line: "rotateZ(0deg) scale(1.1) translateY(107px)" },
    { variants: [0, 3, 6], line: "rotateZ(90deg) scale(1.1) translateY(113px)" },
    { variants: [1, 4, 7], line: "rotateZ(90deg) scale(1.1) translateY(0)" },
    { variants: [2, 5, 8], line: "rotateZ(90deg) scale(1.1) translateY(-113px)" },
    { variants: [0, 4, 8], line: "rotateZ(45deg) scale(1.1)" },
    { variants: [2, 4, 6], line: "rotateZ(-45deg) scale(1.1) " },
  ];
  for (let i = 0; i < lines.length; i++) {
    const [a, b, c] = lines[i].variants;
    const line = lines[i].line;
    if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
      return {
        winner: squares[a],
        line,
      };
    }
  }
  return {
    winner: squares.every(Boolean) ? "draw" : null,
    line: null,
  };
}

async function updateUser(winner, newUser) {
  switch (true) {
    case winner === newUser.symbol:
      newUser.status = "You win!";
      return {
        ...newUser,
        ...(await onlineGameService.userWon(newUser.uid)),
      };
    case winner !== "draw" && newUser.symbol !== winner:
      newUser.status = "You lost";
      return {
        ...newUser,
        ...(await onlineGameService.userLose(newUser.uid)),
      };
    case winner === "draw":
      newUser.status = "Draw";
      return {
        ...newUser,
        ...(await onlineGameService.userDraw(newUser.uid)),
      };
  }
}

async function calculateUsersStatus(winner, users) {
  const newUsers = [];
  for (const user of users) {
    const newUser = await updateUser(winner, { ...user });
    Users.addUser(newUser);
    newUsers.push(newUser);
  }
  return newUsers;
}

function simulateBot(squares, userSymbol) {
  const squaresCopy = [...squares];
  const botSymbol = userSymbol === "X" ? "O" : "X";
  const randomIndex = Math.floor(Math.random() * squaresCopy.length);

  if (!squaresCopy[randomIndex]) {
    squaresCopy[randomIndex] = botSymbol;
    return squaresCopy;
  }

  return simulateBot(squares, userSymbol);
}

module.exports = {
  simulateBot,
  calculateWinner,
  calculateUsersStatus,
};
