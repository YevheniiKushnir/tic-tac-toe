const wrap = (middleware) => (socket, next) => middleware(socket, {}, next);
module.exports = {
  wrap,
};
