const express = require("express");
const Router = express.Router();

const { localGameService } = require("../services/profile");

Router.patch("/local-game", updateLocalProgress);

async function updateLocalProgress(req, res) {
  const { uid } = req.user;

  const history = await localGameService.saveProgress(uid, req.body.history, req.body.reWrite);
  res.status(200).json({
    history,
  });
}

module.exports = Router;
