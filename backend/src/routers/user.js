const express = require("express");
const Router = express.Router();

const {
  getUserProfile,
  updateNickname,
  updateEmail,
  updatePassword,
  getLeaders,
} = require("../services/user");

Router.get("/", getUserProfile);
Router.get("/leaders", getLeaders);

Router.patch("/nickname", updateNickname);
Router.patch("/email", updateEmail);
Router.patch("/password", updatePassword);

module.exports = Router;
