const express = require("express");
const Router = express.Router();

const secureRoutes = require("../middlewares/secureRoutes");

const auth = require("./auth");
const user = require("./user");
const profile = require("./profile");
//
Router.use("/auth", auth);
Router.use("/user", secureRoutes, user);
Router.use("/profile", secureRoutes, profile);

module.exports = Router;
