const { v4: uuidv4 } = require("uuid");
const { calculateWinner, calculateUsersStatus } = require("../helpers/GameCalculation");

const { rooms, newRoom } = require("../data/rooms");

module.exports = (io, socket) => {
  function joinRoomBuilder(args) {
    if (args?.byInvinting) {
      joinToExisRoom({ roomId: args.roomId });
    } else {
      joinRoom();
    }
  }

  function joinRoom() {
    if (!socket.user) return;
    const user = socket.user;
    const roomKeys = rooms.keys();
    let roomId = uuidv4();
    let isJoined = false;

    const newUser = {
      ...user,
      status: undefined,
      again: 0,
    };

    for (key of roomKeys) {
      const room = rooms.get(key);
      if (room.users.length === 1 && !room.users.find((u) => u.uid === user.uid)) {
        rooms.set(key, {
          ...newRoom,
          roomId: room.roomId,
          play: true,
          users: [
            ...room.users.map((u) => ({ ...u, symbol: "X", status: undefined, again: 0 })),
            {
              ...newUser,
              symbol: "O",
            },
          ],
        });
        isJoined = true;
        roomId = key;
        break;
      }
    }

    if (!isJoined) {
      rooms.set(roomId, {
        ...newRoom,
        roomId,
        users: [
          {
            ...newUser,
            symbol: "X",
          },
        ],
      });
    }

    socket.join(roomId);
    io.to(roomId).emit("update-room-info", { data: rooms.get(roomId) });
  }

  function joinToExisRoom({ roomId }) {
    const room = rooms.get(roomId);
    room.play = true;
    socket.join(roomId);
    io.to(roomId).emit("update-room-info", { data: room });
  }

  async function userSelectSquare({ roomId, square }) {
    if (!rooms.has(roomId)) return;
    const room = rooms.get(roomId);

    if (room.squares[square]) return;

    const squaresCopy = [...room.squares];
    squaresCopy[square] = room.turn;
    room.squares = squaresCopy;
    const { winner, line } = calculateWinner(room.squares);

    if (winner) {
      room.users = await calculateUsersStatus(winner, room.users);
      room.line = line;
      room.play = false;
      room.winner = winner;
    }

    room.turn = room.turn === "X" ? "O" : "X";

    io.to(roomId).emit("update-room-info", { data: room });
  }

  function userSelectPlayAgain({ roomId }) {
    if (!rooms.has(roomId)) return;
    const { uid } = socket.user;
    const room = rooms.get(roomId);
    const user = room.users.find((u) => u.uid === uid);

    if (user.again > 0) return;

    user.again = 1;

    room.playAgain = room.playAgain + user.again;

    if (room.playAgain === 2) {
      rooms.set(roomId, {
        ...newRoom,
        users: room.users.map((u) => ({
          ...u,
          again: 0,
          symbol: u.symbol === "X" ? "O" : "X",
          status: undefined,
        })),
        play: true,
        roomId: room.roomId,
      });
    }

    io.to(roomId).emit("update-room-info", { data: rooms.get(roomId) });
  }

  function userSelectReaction({ img, roomId }) {
    if (!rooms.has(roomId)) return;
    const room = rooms.get(roomId);
    const id = uuidv4();

    room.reactions = [...room.reactions, { path: img, id }];
    rooms.set(roomId, room);

    setTimeout(
      () => {
        room.reactions = room.reactions.filter((r) => r.id !== id);
        rooms.set(roomId, room);
        io.to(roomId).emit("update-room-info", { data: room });
      },
      room.reactions.length !== 0
        ? room.reactionTimer * 1000 * room.reactions.length
        : room.reactionTimer * 1000
    );

    io.to(roomId).emit("update-room-info", { data: room });
  }

  async function quickFinishGame({ roomId }) {
    if (!rooms.has(roomId)) return;

    const { uid } = socket.user;
    const room = rooms.get(roomId);

    const userWon = room.users.find((u) => u.uid !== uid);

    if (!userWon) return;

    room.users = await calculateUsersStatus(userWon.symbol, room.users);
    room.play = false;
    room.winner = userWon.symbol;
    room.line = null;
    room.quikeFinish = true
    rooms.set(roomId, room);

    io.to(roomId).emit("update-room-info", { data: room });
  }

  async function userLeaveRoom({ roomId }) {
    if (!rooms.has(roomId)) return;
    const { uid } = socket.user;
    const room = rooms.get(roomId);

    if (room.play) {
      await quickFinishGame({ roomId });
    }
    room.users = room.users.filter((u) => u.uid !== uid);

    if (room.users.length === 0) {
      rooms.delete(roomId);
    } else {
      rooms.set(roomId, room);
      io.to(roomId).emit("update-room-info", { data: room });
    }
  }

  socket.on("join-room", joinRoomBuilder);
  socket.on("user-select-square", userSelectSquare);
  socket.on("user-select-play-again", userSelectPlayAgain);
  socket.on("user-select-reaction", userSelectReaction);
  socket.on("user-faile-game", quickFinishGame);
  socket.on("user-leave-room", userLeaveRoom);
};
