const { v4: uuidv4 } = require("uuid");

const { getUserInfo } = require("../services/user");

const Users = require("../data/users");
const { rooms, newRoom } = require("../data/rooms");

const invitingSettings = {
  existTime: 60,
};

module.exports = (io, socket) => {
  function invitingInit({ uid }) {
    socket.join(uid);
  }

  function invitingOpen({ users: [from, to] }) {
    const users = [from, to];
    socket.emit("inviting-sender", {
      data: { ...invitingSettings, user: Users.getUserById(to), users },
    });
    socket.to(to).emit("inviting-recipient", {
      data: { ...invitingSettings, user: Users.getUserById(from), users },
    });
  }

  function invitingCancel({ users: [from, to] }) {
    socket.emit("inviting-canceled");
    socket.to(to).emit("inviting-canceled");
  }

  async function invitingSubmit({ users: [from, to] }, callback) {
    const roomId = uuidv4();
    const userFrom = await getUserInfo(from);
    const userTo = await getUserInfo(to);

    rooms.set(roomId, {
      ...newRoom,
      roomId,
      play: false,
      users: [
        { ...userFrom, symbol: "X", status: undefined, again: 0 },
        {
          ...userTo,
          symbol: "O",
          status: undefined,
          again: 0,
        },
      ],
    });

    callback({ roomId });
    socket.to(from).emit("inviting-submited-sender", { data: { roomId } });
  }

  socket.on("inviting-init", invitingInit);
  socket.on("inviting-open", invitingOpen);
  socket.on("inviting-cancel", invitingCancel);
  socket.on("inviting-submit", invitingSubmit);
};
