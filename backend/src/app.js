require("dotenv").config();
const morgan = require("morgan");
const Path = require("path");
const express = require("express");
const { createServer } = require("http");
const { Server } = require("socket.io");
const mongoose = require("mongoose");

const rootRouter = require("./routers");
const onlineGame = require("./socket/onlineGame");
const invitation = require("./socket/invitation");

const secureSocket = require("./middlewares/secureSocket");

const User = require("./data/users");

const PORT = process.env.PORT ?? 8080;

const app = express();
const httpServer = createServer(app);
const io = new Server(httpServer, {
  path: "/live-game",
  pingTimeout: 30_000,
});

app.use(express.json());
app.use(morgan("tiny"));
app.use(express.static(Path.join(__dirname, "../client")));

app.use("/api", rootRouter);

const onConnection = (socket) => {
  console.log("connect", socket.id);

  onlineGame(io, socket);
  socket.on("disconnect", () => {
    console.log("disconnect", socket.id);
  });
};

io.of("/").use(secureSocket).on("connection", onConnection);
io.of("/inviting")
  .use(secureSocket)
  .on("connection", (socket) => {
    console.log("inviting connect", socket.id);
    User.addUser(socket.user);

    invitation(io, socket);
    socket.on("disconnect", () => {
      console.log("inviting disconnect", socket.id);
      User.removeUser(socket.user.uid);
    });
  });

app.get("/*", (req, res, nex) => {
  res.sendFile(Path.join(__dirname, "../client", "index.html"));
});

const start = async () => {
  try {
    await mongoose.connect(process.env.MONGO_CONNECT);

    httpServer.listen(PORT);
    console.log(`Server has been started on ${PORT} port`);
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();

// ERROR HANDLER
app.use(errorHandler);

function errorHandler(err, req, res, next) {
  console.error("err: ", err);
  res.status(500).send({ message: "Server error" });
}
