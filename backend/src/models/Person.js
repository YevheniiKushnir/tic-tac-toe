const mongoose = require("mongoose");

const person = new mongoose.Schema({
  created_date: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
  name: {
    type: String,
    required: true,
  },
});

const Person = mongoose.model("Users", person);

module.exports = { Person };
