const mongoose = require("mongoose");

const profile = new mongoose.Schema({
  created_by: {
    type: String,
    required: true,
    unique: true,
  },
  onlineGame: {
    history: {
      draw: {
        type: Number,
        default: 0,
      },
      userLose: {
        type: Number,
        default: 0,
      },
      userWon: {
        type: Number,
        default: 0,
      },
    },
    raiting: {
      type: Number,
      default: 0,
    },
    topRaiting: {
      type: Number,
      default: 0,
    },
  },
  localGame: {
    history: {
      draw: {
        type: Number,
        default: 0,
      },
      userLose: {
        type: Number,
        default: 0,
      },
      userWon: {
        type: Number,
        default: 0,
      },
    },
  },
});

const Profile = mongoose.model("Profiles", profile);

module.exports = { Profile };
