const mongoose = require("mongoose");

const leader = new mongoose.Schema(
  {
    name: String,
    uid: String,
    history: { draw: Number, userLose: Number, userWon: Number },
    raiting: Number,
    topRaiting: Number,
  },
  { timestamps }
);

const Leader = mongoose.model("Leaders", leader);

module.exports = { Leader };
