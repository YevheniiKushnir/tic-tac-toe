const { Profile } = require("../models/Profile");

class Game {
  async findProfile(uid) {
    return await Profile.findOne({ created_by: uid });
  }

  userWon() {
    throw new Error("Method UserWon should exist");
  }
  userLose() {
    throw new Error("Method UserLose should exist");
  }
  userDraw() {
    throw new Error("Method UserDraw should exist");
  }
}

class OnlineGame extends Game {
  #gameResult = 1;
  #userWonCount = 10;
  #userLostCount = 2;

  async userWon(uid) {
    const profile = await this.findProfile(uid);
    profile.onlineGame.history.userWon += this.#gameResult;
    profile.onlineGame.raiting += this.#userWonCount;

    await profile.save();
    return profile.onlineGame;
  }

  async userLose(uid) {
    const profile = await this.findProfile(uid);
    profile.onlineGame.history.userLose += this.#gameResult;
    profile.onlineGame.raiting >= this.#userLostCount &&
      (profile.onlineGame.raiting -= this.#userLostCount);

    await profile.save();
    return profile.onlineGame;
  }

  async userDraw(uid) {
    const profile = await this.findProfile(uid);
    profile.onlineGame.history.draw += this.#gameResult;

    await profile.save();
    return profile.onlineGame;
  }
}

//to-do saving local data
class LocalGame extends Game {
  async userWon(uid) {
    const profile = await this.findProfile(uid);
    profile.localGame.history.userWon += 1;

    await profile.save();
    return profile.localGame;
  }

  async userLose(uid) {
    const profile = await this.findProfile(uid);
    profile.localGame.history.userLose += 1;

    await profile.save();
    return profile.localGame;
  }

  async userDraw(uid) {
    const profile = await this.findProfile(uid);
    profile.localGame.history.draw += 1;

    await profile.save();
    return profile.localGame;
  }
  async saveProgress(uid, history, reWrite = false) {
    const profile = await this.findProfile(uid);
    if (reWrite) {
      profile.localGame.history.draw = history.draw;
      profile.localGame.history.userLose = history.userLose;
      profile.localGame.history.userWon = history.userWon;
    } else {
      profile.localGame.history.draw += history.draw;
      profile.localGame.history.userLose += history.userLose;
      profile.localGame.history.userWon += history.userWon;
    }

    await profile.save();
    return profile.localGame.history;
  }
}

const onlineGameService = new OnlineGame();
const localGameService = new LocalGame();

module.exports = {
  onlineGameService,
  localGameService,
};
