const bcrypt = require("bcryptjs");

const Users = require("../data/users");
const { Person } = require("../models/Person");
const { Profile } = require("../models/Profile");

const getUserProfile = async (req, res) => {
  const user = req.user;

  const {
    name,
    email,
    _id: uid,
    created_date,
  } = await Person.findOne({ _id: user.uid, email: user.email });

  Users.updateUsers();
  const { onlineGame } = await Profile.findOne({ created_by: uid });

  res.json({
    user: {
      name,
      email,
      uid,
      date: created_date,
    },
    profile: onlineGame,
  });
};

const getUserInfo = async (uid) => {
  const { name } = await Person.findOne({ _id: uid });
  const { onlineGame } = await Profile.findOne({ created_by: uid });

  return {
    name,
    uid,
    ...onlineGame,
  };
};

const updateNickname = async (req, res) => {
  const { uid } = req.user;
  const { name } = req.body;

  const user = await Person.findOne({ _id: uid });
  user.name = name;
  user
    .save()
    .then(() => {
      res.status(200).json({ message: "User name updated" });
    })
    .catch(() => {
      res.status(404).json({ message: "User name failed" });
    });
};

const updateEmail = async (req, res) => {
  const { uid } = req.user;
  const { email } = req.body;

  const isRepitedEmail = (await Person.find({ email })).length;

  if (isRepitedEmail) {
    return res.status(404).json({ message: "User email failed" });
  }

  const user = await Person.findOne({ _id: uid });
  user.email = email;
  user
    .save()
    .then(() => {
      res.status(200).json({ message: "User email updated" });
    })
    .catch(() => {
      res.status(404).json({ message: "User email failed" });
    });
};

const updatePassword = async (req, res) => {
  const { uid } = req.user;
  const { oldPassword, newPassword } = req.body;

  const user = await Person.findOne({ _id: uid });

  const comparePasswords = await bcrypt.compare(String(oldPassword), String(user.password));

  if (user && comparePasswords) {
    user.password = await bcrypt.hash(newPassword, 10);
    user
      .save()
      .then(() => {
        res.status(200).json({ message: "User password updated" });
      })
      .catch(() => {
        res.status(404).json({ message: "User password failed" });
      });
  }
};

const getLeaders = (req, res) => {
  Users.updateUsers();
  const users = Users.getUsers();
  res.status(200).json({
    users,
  });
};

module.exports = {
  getUserProfile,
  updateNickname,
  updateEmail,
  updatePassword,
  getUserInfo,
  getLeaders,
};
