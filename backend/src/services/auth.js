const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

const { Person } = require("../models/Person");
const { Profile } = require("../models/Profile");
const getDate = require("../helpers/getData");

const register = async (req, res, next) => {
  try {
    const { email, password, name } = req.body;

    const person = new Person({
      created_date: getDate(),
      email,
      password: await bcrypt.hash(password, 10),
      name,
    });
    const profile = new Profile({ created_by: person._id });

    await profile.save();
    await person.save();

    res.status(200).json({
      message: "Profile created successfully",
    });
  } catch (error) {
    res.status(400).json({
      message: error.message || "Bad request",
    });
  }
};

const login = async (req, res, next) => {
  try {
    const { email, password } = req.body;

    const user = await Person.findOne({ email });
    if (!user) throw Error("User not exist");

    const comparePasswords = await bcrypt.compare(String(password), String(user.password));

    if (user && comparePasswords) {
      const payload = { email: user.email, userId: user._id, name: user.name };

      const jwtToken = jwt.sign(payload, process.env.JWT_SECRET_KEY);

      return res.status(200).json({
        jwt_token: jwtToken,
        uid: user._id,
      });
    } else throw Error("Not authorized");
  } catch (error) {
    res.status(400).json({ message: error.message || "Bad request" });
  }
};

module.exports = {
  register,
  login,
};
