const jwt = require("jsonwebtoken");
const { getUserInfo } = require("../services/user");

const secureSocket = (socket, next) => {
  const token = socket.handshake.auth.token;
  try {
    if (!token) {
      throw new Error("Please, include token to request");
    }

    jwt.verify(token, process.env.JWT_SECRET_KEY, async (err, { userId }) => {
      if (err) {
        throw new Error("Invalid Token");
      }
      const user = await getUserInfo(userId);
      socket.user = user;

      next();
    });
  } catch (err) {
    next(err);
  }
};

module.exports = secureSocket;
