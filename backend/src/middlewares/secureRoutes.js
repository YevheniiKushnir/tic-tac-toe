const jwt = require("jsonwebtoken");

const secureRoutes = (req, res, next) => {
  const { authorization } = req.headers;

  if (!authorization) {
    return res.status(401).json({ message: "Please, provide authorization header" });
  }

  const token = authorization;

  if (!token) {
    return res.status(401).json({ message: "Please, include token to request" });
  }

  try {
    const tokenPayload = jwt.verify(token, process.env.JWT_SECRET_KEY);
    req.user = {
      uid: tokenPayload.userId,
      email: tokenPayload.email,
      name: tokenPayload.name,
    };
    next();
  } catch (err) {
    return res.status(401).json({ message: err.message });
  }
};

module.exports = secureRoutes;
