const newRoom = {
  users: [],
  squares: Array(9).fill(null),
  timer: 10,
  turn: "X",
  quikeFinish: false,
  reactions: [],
  symbolsData: [{ text: "X" }, { text: "O" }],
  play: false,
  line: undefined,
  roomId: undefined,
  playAgain: 0,
  reactionTimer: 3,
  winner: undefined,
};

module.exports = {
  rooms: new Map(),
  newRoom,
};
