class LocalUsers {
  #users = new Map();

  getUsers() {
    return Array.from(this.#users, ([_, u]) => u);
  }

  getUserById(uid) {
    return this.#users.get(uid);
  }

  addUser(user) {
    const invitings = {
      from: null,
      to: null,
      isInvited: false,
      roomId: null,
    };

    if (this.#users.has(user.uid)) {
      const oldInvitings = this.#users.get(user.uid).invitings;
      this.#users.set(user.uid, { ...user, invitings: oldInvitings });
      return;
    }

    this.#users.set(user.uid, { ...user, invitings });
  }

  removeUser(uid) {
    this.#users.delete(uid);
  }

  updateUsers() {
    const users = this.getUsers();

    for (let i = 0, value; i < users.length; i++) {
      for (let j = 0; j < users.length - i - 1; j++) {
        if (users[j].raiting < users[j + 1]?.raiting) {
          value = users[j];
          users[j] = users[j + 1];
          users[j + 1] = value;
        }
      }
    }

    this.#users = new Map(users.map((u, idx) => [u.uid, { ...u, topRaiting: idx + 1 }]));
  }
}

module.exports = new LocalUsers();
