import { rest } from "msw";
import { baseURL } from "./constants";
import {
  tokenOne,
  tokenTwo,
  uidOne,
  uidTwo,
  userCredentialsTwo,
} from "../data/constants";
import * as usersDB from "../data/users";
import * as profilesDB from "../data/profiles";
import * as onlineGame from "../data/onlineGame";
import { uidToSocket } from "./socket-handlers";

function getUidFromHeaders(headers) {
  const token = headers.get("Authorization");
  const uid = token === tokenOne ? uidOne : uidTwo;
  return uid;
}

const handlers = [
  rest.patch(`${baseURL}api/profile/local-game`, async (req, res, ctx) => {
    const { history } = await req.json();
    return res(ctx.status(200), ctx.json({ history }));
  }),
  rest.post(`${baseURL}api/auth/login`, async (req, res, ctx) => {
    const { email, password } = await req.json();

    switch (true) {
      case userCredentialsTwo.email === email &&
        userCredentialsTwo.password === password:
        return res(
          ctx.status(200),
          ctx.json({
            jwt_token: tokenTwo,
            uid: uidTwo,
          })
        );
      default:
        return res(
          ctx.status(200),
          ctx.json({
            jwt_token: tokenOne,
            uid: uidOne,
          })
        );
    }
  }),
  rest.post(`${baseURL}api/auth/register`, async (req, res, ctx) => {
    const { email, password, name } = await req.json();

    if (email?.length && password?.length && name?.length) {
      return res(ctx.status(200));
    }

    return res(ctx.status(400));
  }),
  rest.get(`${baseURL}api/user`, (req, res, ctx) => {
    const uid = getUidFromHeaders(req.headers);

    return res(
      ctx.delay(2000),
      ctx.status(200),
      ctx.json({
        user: usersDB.getUser(uid),
        profile: profilesDB.getProfile(uid).onlineGame,
      })
    );
  }),
  rest.patch(`${baseURL}api/user/nickname`, async (req, res, ctx) => {
    const { name } = await req.json();
    const uid = getUidFromHeaders(req.headers);

    usersDB.updateUser(uid, { name });

    return res(ctx.status(200));
  }),
  rest.patch(`${baseURL}api/user/email`, async (req, res, ctx) => {
    const { email } = await req.json();
    const uid = getUidFromHeaders(req.headers);

    usersDB.updateUser(uid, { email });

    return res(ctx.status(200));
  }),
  rest.patch(`${baseURL}api/user/password`, async (req, res, ctx) => {
    const { newPassword } = await req.json();
    const uid = getUidFromHeaders(req.headers);

    usersDB.updateUser(uid, { password: newPassword });

    return res(ctx.status(200));
  }),
  rest.get(`${baseURL}api/user/leaders`, (req, res, ctx) => {
    return res(
      ctx.delay(200),
      ctx.status(200),
      ctx.json({
        users: [...uidToSocket.keys()].map(onlineGame.getUserForOnlineGame),
      })
    );
  }),
];

export default handlers;
