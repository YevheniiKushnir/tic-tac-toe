import handlers, {
  openRooms,
  serverRoomsMap,
  serverSocketMap,
  uidToSocket,
} from "./socket-handlers";

const serverSocket = (receivedClientEvents) => ({
  on: (key, cb = null) => {
    serverSocketMap.set(key, cb);
  },
  emit: (key, ...args) => {
    const clinetEvent = receivedClientEvents.get(key);

    if (!clinetEvent) {
      console.error(
        `[Mock Socket.io call] Unexpected client socket event: ${key}`
      );
      return;
    }

    if (Array.isArray(clinetEvent)) {
      clinetEvent.forEach((cb) => cb(...args));
    } else {
      clinetEvent(...args);
    }
  },
});

const mockSocketIO = (url, options) => {
  const clientSocket = new Map();

  return {
    on: (key, cb = null) => {
      clientSocket.set(key, cb);

      if (key === "connect") {
        serverSocket(clientSocket).emit("connect");
      }
    },
    emit: (key, ...args) => {
      const serverEvent = serverSocketMap.get(key);

      if (!serverEvent) {
        console.error(
          `[Mock Socket.io call] Unexpected server socket event: ${key}`
        );
        return;
      }

      serverEvent(clientSocket, ...args);
    },
    disconnect: () => {
      clientSocket.delete();
    },
  };
};

const socketServer = {
  io: mockSocketIO,
  listen: () => {
    handlers.forEach(({ key, handler }) => {
      serverSocketMap.set(key, handler);
    });
  },
  close: () => {
    serverSocketMap.delete();
    uidToSocket.delete();
  },
  reset: () => {
    uidToSocket.clear();
    serverSocketMap.clear();
    serverRoomsMap.clear();
    openRooms.clear();
    socketServer.listen();
  },
  debug: () => {
    console.log("[Mock server Sockent.io]", serverSocketMap);
    console.log("[Mock server rooms Sockent.io]", serverRoomsMap);
    console.log("[Mock server uid to socket Sockent.io]", uidToSocket);
  },
  getSockets: () => ({
    server: serverSocketMap,
  }),
};

export { socketServer, serverSocket };
