import { serverSocket } from "./socket-server";
import * as onlineGameDB from "../data/onlineGame";
import { calculateWinner } from "../../helpers/localGameCalculation";

export const serverSocketMap = new Map();
export const serverRoomsMap = new Map();
export const uidToSocket = new Map();
export const openRooms = new Set();

function addUidToSocket(uid, socket) {
  const uidToSocketValue = uidToSocket.get(uid);

  if (uidToSocketValue) {
    const mergedMap = new Map();

    const addMergeMapValue = (cb, key) => {
      const mergedMapValue = mergedMap.get(key);

      if (mergedMapValue) {
        const value = Array.isArray(mergedMapValue)
          ? mergedMapValue
          : [mergedMapValue];
        mergedMap.set(key, [...value, cb]);
      } else {
        mergedMap.set(key, cb);
      }
    };

    uidToSocketValue.forEach(addMergeMapValue);
    socket.forEach(addMergeMapValue);

    uidToSocket.set(uid, mergedMap);
  } else {
    uidToSocket.set(uid, socket);
  }
}

function emitToClientsInRoom(roomId) {
  if (!serverRoomsMap.has(roomId)) return;
  const room = serverRoomsMap.get(roomId);
  const socketOne = uidToSocket.get(room.users[0]?.uid);
  const socketTwo = uidToSocket.get(room.users[1]?.uid);

  if (socketOne)
    serverSocket(socketOne).emit("update-room-info", { data: room });
  if (socketTwo)
    serverSocket(socketTwo).emit("update-room-info", { data: room });
}

function quickFinishGame({ uid, roomId }) {
  if (!serverRoomsMap.has(roomId)) return;
  const room = serverRoomsMap.get(roomId);

  const userWon = room.users.find((u) => u.uid !== uid);

  if (!userWon) return;

  room.users = onlineGameDB.calculateUsersStatus(userWon.symbol, room.users);
  room.play = false;
  room.winner = userWon.symbol;
  room.quikeFinish = true;

  serverRoomsMap.set(roomId, room);

  emitToClientsInRoom(roomId);
}

const invitingHandlers = [
  {
    key: "inviting-init",
    handler: (clientSocket, { uid }) => {
      serverRoomsMap.set(uid, [uid]);
      addUidToSocket(uid, clientSocket);
    },
  },
  {
    key: "inviting-open",
    handler: (clientSocket, { users: [from, to] }) => {
      serverSocket(clientSocket).emit("inviting-sender", {
        data: {
          users: [from, to],
          user: onlineGameDB.getUserForOnlineGame(to),
          existTime: 2,
        },
      });

      const clientSocketTo = uidToSocket.get(to);
      if (clientSocketTo) {
        serverSocket(clientSocketTo).emit("inviting-recipient", {
          data: {
            users: [from, to],
            user: onlineGameDB.getUserForOnlineGame(from),
            existTime: 2,
          },
        });
      }
    },
  },
  {
    key: "inviting-submit",
    handler: (_, { users: [from, to] }, callback) => {
      const roomId = crypto.randomUUID();

      serverRoomsMap.set(roomId, {
        ...onlineGameDB.baseRoom,
        roomId,
        users: [
          {
            ...onlineGameDB.baseUser,
            ...onlineGameDB.getUserForOnlineGame(from),
            symbol: "X",
          },
        ],
      });

      const clientSocketFrom = uidToSocket.get(from);
      if (clientSocketFrom) {
        serverSocket(clientSocketFrom).emit("inviting-submited-sender", {
          data: { roomId },
        });
      }

      callback({ roomId });
    },
  },
  {
    key: "inviting-cancel",
    handler: (clientSocket, { users: [from, to] }) => {
      serverSocket(clientSocket).emit("inviting-canceled");
      const clientSocketTo = uidToSocket.get(to);
      if (clientSocketTo) {
        serverSocket(clientSocketTo).emit("inviting-canceled");
      }
    },
  },
];

const onlineGameHandler = [
  {
    key: "join-room",
    handler: (clientSocket, { uid, byInvinting, roomId: receivedRoomId }) => {
      let updatedRoom;

      addUidToSocket(uid, clientSocket);

      if (byInvinting) {
        const room = serverRoomsMap.get(receivedRoomId);
        const roomUsers = room.users.find((u) => u.uid === uid)
          ? room.users
          : [
              ...room.users,
              {
                ...onlineGameDB.baseUser,
                ...onlineGameDB.getUserForOnlineGame(uid),
                symbol: "O",
              },
            ];
        updatedRoom = {
          ...room,
          users: roomUsers,
          play: roomUsers.length === 2,
        };
      } else if (openRooms.size === 0) {
        const roomId = crypto.randomUUID();
        openRooms.add(roomId);

        updatedRoom = {
          ...onlineGameDB.baseRoom,
          roomId,
          users: [
            {
              ...onlineGameDB.baseUser,
              ...onlineGameDB.getUserForOnlineGame(uid),
              symbol: "X",
            },
          ],
        };
      } else {
        const [roomId] = openRooms;
        const room = serverRoomsMap.get(roomId);

        updatedRoom = {
          ...room,
          users: [
            ...room.users,
            {
              ...onlineGameDB.baseUser,
              ...onlineGameDB.getUserForOnlineGame(uid),
              symbol: "O",
            },
          ],
          play: true,
        };

        openRooms.delete(roomId);
      }

      serverRoomsMap.set(updatedRoom.roomId, updatedRoom);

      emitToClientsInRoom(updatedRoom.roomId);
    },
  },
  {
    key: "user-leave-room",
    handler: (_, { uid, roomId }) => {
      if (!serverRoomsMap.has(roomId)) return;
      const room = serverRoomsMap.get(roomId);

      if (room.play) {
        quickFinishGame({ roomId });
      }
      room.users = room.users.filter((u) => u.uid !== uid);

      if (room.users.length === 0) {
        serverRoomsMap.delete(roomId);
      } else {
        serverRoomsMap.set(roomId, room);
        emitToClientsInRoom(roomId);
      }
    },
  },
  {
    key: "user-faile-game",
    handler: (_, { uid, roomId }) => {
      quickFinishGame({ uid, roomId });
    },
  },
  {
    key: "user-select-reaction",
    handler: (_, { img, roomId }) => {
      if (!serverRoomsMap.has(roomId)) return;
      const room = serverRoomsMap.get(roomId);
      const id = crypto.randomUUID();

      room.reactions = [...room.reactions, { path: img, id }];
      serverRoomsMap.set(roomId, room);

      setTimeout(
        () => {
          room.reactions = room.reactions.filter((r) => r.id !== id);
          serverRoomsMap.set(roomId, room);
          emitToClientsInRoom(roomId);
        },
        room.reactions.length
          ? room.reactionTimer * 1000 * room.reactions.length
          : room.reactionTimer * 1000
      );

      emitToClientsInRoom(roomId);
    },
  },
  {
    key: "user-select-play-again",
    handler: (_, { roomId, uid }) => {
      if (!serverRoomsMap.has(roomId)) return;
      const room = serverRoomsMap.get(roomId);
      const user = room.users.find((u) => u.uid === uid);

      if (user.again > 0) return;

      user.again = 1;

      room.playAgain = room.playAgain + user.again;

      if (room.playAgain === 2) {
        serverRoomsMap.set(roomId, {
          ...onlineGameDB.baseRoom,
          users: room.users.map((u) => ({
            ...u,
            again: 0,
            symbol: u.symbol === "X" ? "O" : "X",
            status: undefined,
          })),
          play: true,
          roomId: room.roomId,
          turn: room.users[0].symbol === "X" ? "O" : "X",
        });
      }

      emitToClientsInRoom(roomId);
    },
  },
  {
    key: "user-select-square",
    handler: (_, { roomId, square }) => {
      if (!serverRoomsMap.has(roomId)) return;
      const room = serverRoomsMap.get(roomId);

      if (room.squares[square] || room.winner) return;

      const squaresCopy = [...room.squares];
      squaresCopy[square] = room.turn;
      room.squares = squaresCopy;
      const { winner, line } = calculateWinner(room.squares);

      if (winner) {
        room.users = onlineGameDB.calculateUsersStatus(winner, room.users);
        room.line = line;
        room.play = false;
        room.winner = winner;
      }

      room.turn = room.turn === "X" ? "O" : "X";

      serverRoomsMap.set(roomId, room);

      emitToClientsInRoom(roomId);
    },
  },
];

export default [...invitingHandlers, ...onlineGameHandler];
