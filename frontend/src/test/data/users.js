import { faker } from "@faker-js/faker";

export const uidToUser = new Map();

export const getUser = (uid) => uidToUser.get(uid);

export const updateUser = (uid, data = {}) => {
  const prevUserData = uidToUser.get(uid);

  uidToUser.set(uid, { ...prevUserData, ...data });
};

export const createUser = (options = {}) => ({
  name: faker.internet.userName(),
  email: faker.internet.email(),
  uid: faker.string.uuid(),
  date: faker.date.past(),
  password: "FakePass",
  ...options,
});
