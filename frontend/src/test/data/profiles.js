import { faker } from "@faker-js/faker";

export const uidToUserProfile = new Map();

export const getProfile = (uid) => uidToUserProfile.get(uid);

export const updateProfile = (uid, data = {}) => {
  const prevProfile = uidToUserProfile.get(uid);

  uidToUserProfile.set(uid, { ...prevProfile, ...data });
};

export const createProfile = (options = {}) => ({
  created_by: faker.string.uuid(),
  onlineGame: {
    history: {
      draw: 0,
      userLose: 0,
      userWon: 0,
    },
    raiting: 0,
    topRaiting: 0,
  },
  localGame: {
    history: {
      draw: 0,
      userLose: 0,
      userWon: 0,
    },
  },
  ...options,
});
