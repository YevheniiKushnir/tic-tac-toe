import { getProfile, uidToUserProfile, updateProfile } from "./profiles";
import { getUser } from "./users";

const userGameResults = {
  win: (uid) => {
    const prevProfile = uidToUserProfile.get(uid);
    const updatedData = {
      ...prevProfile,
      onlineGame: {
        ...prevProfile.onlineGame,
        history: {
          ...prevProfile.onlineGame.history,
          userWon: prevProfile.onlineGame.history.userWon + 1,
        },
        raiting: prevProfile.onlineGame.raiting + 10,
      },
    };

    updateProfile(uid, updatedData);

    return { ...updatedData.onlineGame };
  },
  lost: (uid) => {
    const prevProfile = uidToUserProfile.get(uid);
    const updatedData = {
      ...prevProfile,
      onlineGame: {
        ...prevProfile.onlineGame,
        history: {
          ...prevProfile.onlineGame.history,
          userLose: prevProfile.onlineGame.history.userLose + 1,
        },
        raiting:
          prevProfile.onlineGame.raiting === 2 ||
          prevProfile.onlineGame.raiting === 0
            ? 0
            : prevProfile.onlineGame.raiting - 2,
      },
    };

    updateProfile(uid, updatedData);

    return { ...updatedData.onlineGame };
  },
  draw: (uid) => {
    const prevProfile = uidToUserProfile.get(uid);
    const updatedData = {
      ...prevProfile,
      onlineGame: {
        ...prevProfile.onlineGame,
        history: {
          ...prevProfile.onlineGame.history,
          draw: prevProfile.onlineGame.history.draw + 1,
        },
      },
    };

    updateProfile(uid, updatedData);

    return { ...updatedData.onlineGame };
  },
};

export const calculateUsersStatus = (winner, users) => {
  const updatedUsers = users.map((u) => {
    switch (true) {
      case winner === u.symbol:
        return {
          ...u,
          status: "You win!",
          ...userGameResults.win(u.uid),
        };
      case winner !== "draw" && u.symbol !== winner:
        return {
          ...u,
          status: "You lost",
          ...userGameResults.lost(u.uid),
        };
      case winner === "draw":
        return {
          ...u,
          status: "Draw",
          ...userGameResults.draw(u.uid),
        };
      default:
        throw new Error(`Unexpected winner: ${winner}`);
    }
  });

  return updatedUsers;
};

export const baseRoom = {
  users: [],
  squares: Array(9).fill(null),
  timer: 10,
  turn: "X",
  quikeFinish: false,
  reactions: [],
  symbolsData: [{ text: "X" }, { text: "O" }],
  play: false,
  line: undefined,
  roomId: undefined,
  playAgain: 0,
  reactionTimer: 1,
  winner: undefined,
};

export const baseUser = {
  status: undefined,
  again: 0,
};

export const getUserForOnlineGame = (uid) => {
  const user = getUser(uid);
  const profile = getProfile(uid);

  return {
    name: user.name,
    uid: user.uid,
    ...profile.onlineGame,
  };
};
