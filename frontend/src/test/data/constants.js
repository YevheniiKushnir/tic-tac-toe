export const uidOne = "mock_uid_one";
export const uidTwo = "mock_uid_two";

export const tokenOne = "mock_token_one";
export const tokenTwo = "mock_token_two";

export const symbolX = "X";
export const symbolO = "O";

export const userCredentialsOne = {
  email: "testOne@somemail.com",
  password: "asdfgOne",
};

export const userCredentialsTwo = {
  email: "testTwo@somemail.com",
  password: "asdfgTwo",
};
