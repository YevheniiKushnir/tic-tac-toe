import { uidOne, uidTwo } from "./constants";
import { createUser, uidToUser } from "./users";
import { createProfile, uidToUserProfile } from "./profiles";

const users = () => [createUser({ uid: uidOne }), createUser({ uid: uidTwo })];

const userProfiles = () => [
  createProfile({ created_by: uidOne }),
  createProfile({ created_by: uidTwo }),
];

export function seedData() {
  users().forEach((u) => {
    uidToUser.set(u.uid, u);
  });
  userProfiles().forEach((p) => {
    uidToUserProfile.set(p.created_by, p);
  });
}

export function clearSeed() {
  uidToUserProfile.clear();
  uidToUser.clear();
}
