import "@testing-library/jest-dom";
import { cleanup } from "@testing-library/react";
import { vi } from "vitest";
import { seedData, clearSeed } from "./data/seeds";
import { server, socketServer } from "./server";

Object.defineProperty(window, "scrollTo", {
  writable: true,
  configurable: true,
  value: vi.fn(),
});

vi.mock("socket.io-client", () => ({
  io: socketServer.io,
}));

seedData();

beforeAll(() => {
  server.listen();
  socketServer.listen();
});
afterAll(() => {
  server.close();
  socketServer.close();
  vi.clearAllMocks();
  clearSeed();
});
afterEach(() => {
  cleanup();
  server.resetHandlers();
  localStorage.clear();
  socketServer.reset();
  seedData();
});
