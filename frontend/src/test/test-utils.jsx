import { vi } from "vitest";
import {
  act,
  render as rednderRTL,
  renderHook,
  waitForElementToBeRemoved,
  screen,
} from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { BrowserRouter, createMemoryRouter } from "react-router-dom";
import { Provider as StoreProvider } from "react-redux";

import AppProviders from "../AppProviders";
import { setupStore } from "../redux/store";
import router from "../routes/root";
import { socketServer } from "./server";

const waitForLoadingToFinish = () =>
  waitForElementToBeRemoved(
    () => [
      ...screen.queryAllByLabelText(/loading/i),
      ...screen.queryAllByText(/loading/i),
    ],
    { timeout: 4000 }
  );

function TestAppProviders({ children, store }) {
  return (
    <BrowserRouter>
      <StoreProvider store={store ?? setupStore()}>{children}</StoreProvider>
    </BrowserRouter>
  );
}

function render(
  ui,
  {
    preloadedState = {},
    CustomWrapper = ({ children }) => children,
    ...options
  } = {
    preloadedState: {},
    CustomWrapper: ({ children }) => children,
  }
) {
  return rednderRTL(ui, {
    wrapper: ({ children }) => (
      <TestAppProviders store={setupStore(preloadedState)}>
        <CustomWrapper>{children}</CustomWrapper>
      </TestAppProviders>
    ),
    ...options,
  });
}

function renderWithRoute(
  route = "/",
  { preloadedState, ...options } = { preloadedState: {} }
) {
  return rednderRTL(
    <AppProviders
      router={createMemoryRouter(router, {
        initialEntries: [route],
      })}
      store={setupStore(preloadedState)}
    />,
    options
  );
}

function renderWithHook(
  hookCb,
  preloadedState = {},
  { CustomWrapper } = {
    CustomWrapper: ({ children }) => children,
  }
) {
  return renderHook(hookCb, {
    wrapper: ({ children }) => (
      <TestAppProviders store={setupStore(preloadedState)}>
        <CustomWrapper>{children}</CustomWrapper>
      </TestAppProviders>
    ),
  });
}

function createOnlineGameEnemy(uid) {
  const clientSocket = socketServer.io();
  const state = {};
  let roomId = undefined;

  clientSocket.on("update-room-info", ({ data }) => {
    const { roomId: newRoomID, ...newState } = data;
    Object.keys(newState).forEach((key) => {
      state[key] = newState[key];
    });
    roomId = newRoomID;
  });

  clientSocket.on("connect", () => {
    clientSocket.emit("join-room", { uid });
  });

  return {
    clientSocket,
    state,
    roomId,
  };
}

async function autoPlayGame(actionOne, actionTwo, steps = []) {
  for (const [type, value] of steps) {
    await act(async () => {
      if (type === 1) {
        await actionOne(value);
      } else if (type === 2) {
        await actionTwo(value);
      }
    });
  }
}

async function delay(time = 0) {
  return new Promise((res) => {
    setTimeout(res, time);
  });
}
export * from "@testing-library/react";
export {
  render,
  renderWithRoute,
  renderWithHook,
  screen,
  userEvent,
  waitForLoadingToFinish,
  createOnlineGameEnemy,
  autoPlayGame,
  delay,
};
