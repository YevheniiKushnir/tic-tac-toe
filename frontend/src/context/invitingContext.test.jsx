import { afterEach, expect, test, vi } from "vitest";
import { RouterProvider, createMemoryRouter } from "react-router-dom";
import FullPageErrorFallback from "../components/UI/error/FullPageErrorFallback";
import { render, screen, renderWithHook, act } from "../test/test-utils";
import InvitingProvider, { useInvitatigContext } from "./invitingContext";

const consoleErrorSpy = vi.spyOn(console, "error").mockImplementation(() => {});

afterEach(() => {
  consoleErrorSpy.mockClear();
});

const mockToken = "mock_token";
const mockUid = "mock_uid";
const mockUidTwo = "mock_uid_two";
const mockUserOne = {
  uid: mockUid,
  isAuth: true,
  token: mockToken,
};

function TestComponent() {
  const context = useInvitatigContext();
  return context;
}

function renderHook() {
  return renderWithHook(
    () => useInvitatigContext(),
    { user: mockUserOne },
    { CustomWrapper: InvitingProvider }
  );
}

test("throw error on using invitatig context without Invitatig provider", () => {
  const routes = [
    {
      path: "/",
      element: <TestComponent />,
      errorElement: <FullPageErrorFallback />,
    },
  ];

  const router = createMemoryRouter(routes, {
    initialEntries: ["/"],
    initialIndex: 0,
  });

  render(<RouterProvider router={router} />, {
    wrapper: ({ children }) => children,
  });

  expect(consoleErrorSpy).toHaveBeenCalled();
  expect(screen.getByRole("alert")).toBeDefined();
  expect(screen.getByRole("alert").textContent).toMatchInlineSnapshot(`
  "Uh oh... There's a problem. Try refreshing the app.You missed InvitingProvider"
  `);
});

test("toggle find friend state", () => {
  const { result } = renderHook();

  expect(result.current).toEqual({
    uid: mockUid,
    user: undefined,
    isFindFriend: false,
    openInviting: expect.any(Function),
    submitInviting: expect.any(Function),
    cancelInviting: expect.any(Function),
    closeFindFriend: expect.any(Function),
    openFindFriend: expect.any(Function),
  });

  act(() => {
    result.current.openFindFriend();
  });

  expect(result.current.isFindFriend).toBeTruthy();

  act(() => {
    result.current.closeFindFriend();
  });

  expect(result.current.isFindFriend).toBeFalsy();
});
