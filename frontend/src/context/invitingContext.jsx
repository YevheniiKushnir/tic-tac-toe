import {
  createContext,
  useContext,
  useState,
  useCallback,
  useMemo,
} from "react";

import useInvitation from "../hooks/useInvitation";

const InvitingContext = createContext(null);
InvitingContext.displayName = "InvitingContext";

export function useInvitatigContext() {
  const context = useContext(InvitingContext);

  if (!context) {
    throw new Error("You missed InvitingProvider");
  }

  return context;
}

export default function InvitingProvider({ children }) {
  const [isFindFriend, setIsFindFriend] = useState(false);
  const inviting = useInvitation();

  const closeFindFriend = useCallback(() => {
    setIsFindFriend(false);
  }, []);

  const openFindFriend = useCallback(() => {
    setIsFindFriend(true);
  }, []);

  const value = useMemo(
    () => ({ ...inviting, isFindFriend, closeFindFriend, openFindFriend }),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [inviting, isFindFriend]
  );

  return (
    <InvitingContext.Provider value={value}>
      {children}
    </InvitingContext.Provider>
  );
}
