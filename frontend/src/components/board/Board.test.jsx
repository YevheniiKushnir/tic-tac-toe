import { test, expect, vi, afterEach } from "vitest";
import { render, screen, userEvent } from "../../test/test-utils";
import Board from "./Board";

const mockBtn = vi.fn().mockImplementation(() => {});
const restartBtn = vi.fn().mockImplementation(() => {});
const mockStatus = "mock_status";
const mockAgain = 1;
const mockLine = "mock_line";

afterEach(() => {
  mockBtn.mockReset();
  restartBtn.mockReset();
});

test("board render disabled buttons", async () => {
  const { container } = render(<Board click={mockBtn} isDisabled />);

  expect(screen.getAllByLabelText(/board item/i)).toHaveLength(9);

  await userEvent.click(screen.getAllByLabelText(/board item/i)[0]);

  expect(mockBtn).not.toHaveBeenCalled();
  expect(container).toMatchSnapshot();
});

test("user can restart game", async () => {
  const { rerender, container } = render(
    <Board restart={restartBtn} line={mockLine} status={mockStatus} />
  );

  expect(screen.getByLabelText(/status/i).textContent).toBe(mockStatus);
  expect(screen.getByLabelText(/board line/i)).toBeVisible();
  expect(screen.getByLabelText(/play again/i).textContent).toBe("Play again");

  await userEvent.click(screen.getByLabelText(/play again/i));

  expect(restartBtn).toHaveBeenCalled();

  rerender(
    <Board
      restart={restartBtn}
      line={mockLine}
      again={mockAgain}
      status={mockStatus}
    />
  );

  expect(screen.getByLabelText(/play again/i).textContent).toBe(
    `Play again (${mockAgain})`
  );
  expect(container).toMatchSnapshot();
});

test("render board with values", () => {
  const { container } = render(<Board boards={["X", "X"]} />);

  expect(screen.getAllByLabelText(/board item/i)).toHaveLength(2);
  expect(container).toMatchSnapshot();
});
