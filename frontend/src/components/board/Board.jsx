import { useId, memo } from "react";
import "./Board.scss";

import XIcon from "../../assets/images/Vector.png";
import OIcon from "../../assets/images/Ellipse 1.png";
import lineIcon from "../../assets/images/board-line.png";
import Button from "../UI/button/Button";

const BoardItem = ({ value, click }) => {
  return (
    <button
      aria-label="board item"
      className="board__item"
      onClick={click}
      disabled={Boolean(value)}
    >
      {value && (
        <img
          src={value === "X" ? XIcon : OIcon}
          alt={`${value === "X" ? "cross" : "zero"} icon`}
        />
      )}
    </button>
  );
};

const Board = ({
  click,
  restart,
  line,
  boards = Array(9).fill(null),
  status = null,
  again = null,
  isDisabled = true,
}) => {
  const id = useId();

  return (
    <div className="board" aria-label="board">
      {boards.map((b, i) => (
        <BoardItem
          key={id + i}
          value={b}
          click={() => {
            if (isDisabled) return;
            click(i);
          }}
        />
      ))}
      {!status ? null : (
        <>
          <div className="board__status">
            <p aria-label="Status">{status}</p>
            <Button
              click={restart}
              content={
                Boolean(again)
                  ? `Play again ${again === 0 ? "0" : "(" + again + ")"}`
                  : "Play again"
              }
              cName="white"
              aria-label="Play again"
            />
          </div>
          {line && (
            <img
              className="board__line"
              src={lineIcon}
              style={{ transform: line }}
              alt="line board icon"
              aria-label="Board line"
            />
          )}
        </>
      )}
    </div>
  );
};

export default memo(Board);
