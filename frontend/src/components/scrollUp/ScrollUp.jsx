import { useCallback, useEffect, useState, memo } from "react";
import "./ScrollUp.scss";

import arrowUp from "../../assets/icons/ArrowUp-w.png";

const ScrollUp = ({ children }) => {
  const [viewBtn, setViewBtn] = useState(false);

  const hanldeClick = useCallback(() => {
    window.scrollTo({ top: 0, behavior: "smooth" });
  }, []);

  const handleScroll = useCallback(() => {
    if (window.scrollY < window.screen.height / 2) {
      setViewBtn(false);
    } else setViewBtn(true);
  }, []);

  useEffect(() => {
    window.addEventListener("scroll", handleScroll);

    return () => window.removeEventListener("scroll", handleScroll);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div className="scroll-up">
      {viewBtn && (
        <button
          onClick={hanldeClick}
          className="scroll-up__btn"
          aria-label="Scroll up"
        >
          <img src={arrowUp} alt="arrow up icon" />
        </button>
      )}
      {children}
    </div>
  );
};

export default memo(ScrollUp);
