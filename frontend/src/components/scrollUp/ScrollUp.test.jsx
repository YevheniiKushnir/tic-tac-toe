import { test, expect, vi, afterEach } from "vitest";
import { render, screen, userEvent, fireEvent } from "../../test/test-utils";
import ScrollUp from "./ScrollUp";

const mockScrollY = vi.spyOn(window, "scrollY", "get");
const mockScreenHeight = vi.spyOn(window.screen, "height", "get");

afterEach(() => {
  mockScrollY.mockReset();
  mockScreenHeight.mockReset();
});

function renderComponent() {
  const rendered = render(<ScrollUp />);
  fireEvent.scroll(window);
  return rendered;
}

test("should render the scroll-up button when viewBtn is true", () => {
  mockScrollY.mockReturnValue(300);
  mockScreenHeight.mockReturnValue(100);

  const { container } = renderComponent();

  expect(screen.getByLabelText(/scroll up/i)).toBeInTheDocument();
  expect(container).toMatchSnapshot();
});

test("should not render the scroll-up button when viewBtn is false", () => {
  mockScrollY.mockReturnValue(0);
  mockScreenHeight.mockReturnValue(100);

  const { container } = renderComponent();

  expect(screen.queryByLabelText(/scroll up/i)).not.toBeInTheDocument();
  expect(container).toMatchSnapshot();
});

test("should scroll to the top when the scroll-up button is clicked", async () => {
  mockScrollY.mockReturnValue(300);
  mockScreenHeight.mockReturnValue(100);

  const { container } = renderComponent();

  expect(screen.getByLabelText(/scroll up/i)).toBeInTheDocument();

  await userEvent.click(screen.getByLabelText(/scroll up/i));

  expect(window.scrollTo).toHaveBeenCalledWith({ top: 0, behavior: "smooth" });
  expect(container).toMatchSnapshot();
});

test("should show the scroll-up button when scrolling down and hide it when scrolling up", () => {
  mockScreenHeight.mockReturnValue(100);
  mockScrollY.mockReturnValue(0);

  const { container } = renderComponent();

  expect(screen.queryByLabelText(/scroll up/i)).not.toBeInTheDocument();
  expect(container).toMatchSnapshot();

  mockScrollY.mockReturnValue(300);
  fireEvent.scroll(window);

  expect(screen.getByLabelText(/scroll up/i)).toBeInTheDocument();
  expect(container).toMatchSnapshot();

  mockScrollY.mockReturnValue(0);
  fireEvent.scroll(window);

  expect(screen.queryByLabelText(/scroll up/i)).not.toBeInTheDocument();
  expect(container).toMatchSnapshot();
});
