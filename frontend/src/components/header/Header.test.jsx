import { test, expect } from "vitest";
import { render, screen } from "../../test/test-utils";
import Header from "./Header";

test("render component with unauthenticated user", () => {
  const { container } = render(<Header />);

  expect(screen.queryByAltText(/user profile icon/i)).not.toBeInTheDocument();
  expect(container).toMatchSnapshot();
});

test("render component with authenticated user", () => {
  const { container } = render(<Header />, {
    preloadedState: { user: { isAuth: true } },
  });

  expect(screen.queryByAltText(/user profile icon/i)).toBeInTheDocument();
  expect(container).toMatchSnapshot();
});
