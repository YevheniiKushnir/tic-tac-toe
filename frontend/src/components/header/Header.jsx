import { memo } from "react";
import { Link, NavLink, useLocation } from "react-router-dom";
import { useSelector } from "react-redux";

import "./header.scss";
import logo from "../../assets/images/Logo.png";
import playIcon from "../../assets/icons/Play.png";
import medalIcon from "../../assets/icons/Medal.png";
import userProfile from "../../assets/images/GearSix.png";

const GAME_URL = "/game";

const Header = () => {
  const isAuth = useSelector((state) => state.user.isAuth);
  const location = useLocation();
  const isGamePage = location.pathname.startsWith(GAME_URL);

  return (
    <header className="header">
      <Link to="/">
        <img src={logo} alt="Logo" />
      </Link>
      <nav className="header__nav">
        <NavLink
          to="/"
          className={({ isActive }) =>
            `header__nav_play ${isActive || isGamePage ? "active" : ""}`
          }
        >
          <img src={playIcon} alt="play icon" /> Game room
        </NavLink>
        <NavLink to="/leader-board" className="header__nav_medal">
          <img src={medalIcon} alt="medal icon" /> Leader board
        </NavLink>
      </nav>
      <div className="header_auth">
        {isAuth ? (
          <Link to="/user-profile">
            <img src={userProfile} alt="user profile icon" />
          </Link>
        ) : (
          <>
            <Link to="/sing-in">Sing in</Link>
            <span>/</span>
            <Link to={"/sing-up"}>Sing up</Link>
          </>
        )}
      </div>
    </header>
  );
};

export default memo(Header);
