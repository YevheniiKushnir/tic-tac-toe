import { expect, test } from "vitest";
import { render } from "../../../test/test-utils";
import GameLayout from "./GameLayout";

test("component should render correctly", () => {
  const { container } = render(<GameLayout />);

  expect(container).toMatchSnapshot();
});
