import "./GameLayout.scss";
import flag from "../../../assets/images/flag.png";

import Button from "../../UI/button/Button";

const GameLayout = ({ children }) => {
  return (
    <div className="home">
      <div className="home__content">{children}</div>
      <hr />
      <div className="home__qa">
        <div className="home__qa__block">
          <h4>Like game?</h4>
          <div className="home__qa__block__text">
            <img src={flag} alt="ukraine flag" />
            <p>Please support Ukraine!</p>
          </div>
          <Button
            type={"link"}
            url={"https://war.ukraine.ua/donate/"}
            content={"Donate"}
            blank
          />
        </div>
        <div className="home__qa__block">
          <h4>Find error?</h4>
          <p>Please let us know</p>
          <Button
            type={"link"}
            url="https://keqing-site.onrender.com/autorsReview"
            content="Report an error"
            cName="white"
            blank
          />
        </div>
      </div>
      <hr />
      <div className="home__rules">
        <h4>How to play</h4>
        <p>
          You probably already know how to play Tic-Tac-Toe. It's a really
          simple game, right? That's what most people think. But if you really
          wrap your brain around it, you'll discover that Tic-Tac-Toe isn't
          quite as simple as you think!
          <br />
          Tic-Tac -Toe (along with a lot of other games) involves looking ahead
          and trying to figure out what the person playing against you might do
          next.
          <br />
          RULES FOR TIC-TAC-TOE
        </p>
        <ol>
          <li>The game is played on a grid that's 3 squares by 3 squares.</li>
          <li>
            You are X, your friend (or the computer in this case) is O. Players
            take turns putting their marks in empty squares.
          </li>
          <li>
            The first player to get 3 of her marks in a row (up, down, across,
            or diagonally) is the winner.
          </li>
          <li>
            When all 9 squares are full, the game is over. If no player has 3
            marks in a row, the game ends in a tie.
          </li>
        </ol>
      </div>
    </div>
  );
};

export default GameLayout;
