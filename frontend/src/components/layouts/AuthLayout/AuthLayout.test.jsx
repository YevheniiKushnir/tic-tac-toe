import { expect, test } from "vitest";
import { render } from "../../../test/test-utils";
import AuthLayout from "./AuthLayout";

test("component should render correctly", () => {
  const { container } = render(<AuthLayout />);

  expect(container).toMatchSnapshot();
});
