import "./AuthLayout.scss";

import { NavLink, Outlet } from "react-router-dom";

const AuthLayout = () => {
  return (
    <div className="auth_form">
      <div className="auth_form__links">
        <NavLink to={"/sing-in"}>Sign In</NavLink>
        <NavLink to={"/sing-up"}>Sign Up</NavLink>
      </div>
      <Outlet />
    </div>
  );
};

export default AuthLayout;
