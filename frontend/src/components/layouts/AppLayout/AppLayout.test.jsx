import { expect, test } from "vitest";
import { render } from "../../../test/test-utils";
import InvitingProvider from "../../../context/invitingContext";
import AppLayout from "./AppLayout";

test("component should render correctly", () => {
  const { container } = render(<AppLayout />, {
    CustomWrapper: InvitingProvider,
  });

  expect(container).toMatchSnapshot();
});
