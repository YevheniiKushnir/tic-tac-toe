import { Outlet } from "react-router-dom";

import Footer from "../../footer/Footer";
import Header from "../../header/Header";

import Invitation from "../../../pages/invitation/Invitation";

const AppLayout = () => {
  return (
    <>
      <div className="layout">
        <Header />
        <Outlet />
        <Invitation />
      </div>
      <Footer />
    </>
  );
};

export default AppLayout;
