import { memo } from "react";
import { Link } from "react-router-dom";
import "./Button.scss";

const Button = ({
  click,
  url = "",
  content = "",
  bType = "button",
  cName = "dark",
  type = null,
  style = null,
  disabled = false,
  blank = false,
  ...props
}) => {
  return type !== "link" ? (
    <button
      type={bType}
      disabled={disabled}
      onClick={click}
      className={"custome-btn " + cName}
      style={style}
      {...props}
    >
      {content}
    </button>
  ) : blank ? (
    <a
      href={url}
      target="_blank"
      className={"custome-btn link " + cName}
      style={style}
      rel="noreferrer"
      {...props}
    >
      {content}
    </a>
  ) : (
    <Link
      to={url}
      className={"custome-btn link " + cName}
      style={style}
      {...props}
    >
      {content}
    </Link>
  );
};

export default memo(Button);
