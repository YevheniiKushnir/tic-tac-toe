import { expect, test, vi } from "vitest";
import { render, screen, userEvent } from "../../../test/test-utils";
import Button from "./Button";

const mockLabel = "mock_label";
const mockCustomeProps = { "aria-label": mockLabel };
const mockType = "button";
const mockClick = vi.fn();
const mockClass = "mock_class";
const mockContent = "mock_content";
const mockLinkType = "link";
const mockUrl = "mock_url";

test("render component as button with correct props", async () => {
  const { rerender, container } = render(
    <Button
      type={mockType}
      click={mockClick}
      cName={mockClass}
      content={mockContent}
      disabled
      {...mockCustomeProps}
    />
  );
  const buttonElement = screen.getByRole("button");

  await userEvent.click(buttonElement);

  expect(screen.getByText(mockContent)).toBeInTheDocument();
  expect(buttonElement.className.split(" ").includes(mockClass)).toBeTruthy();
  expect(buttonElement).toHaveAttribute("disabled");
  expect(buttonElement).toHaveAttribute("aria-label", mockLabel);
  expect(mockClick).not.toHaveBeenCalled();
  expect(container).toMatchSnapshot();

  rerender(
    <Button
      type={mockType}
      click={mockClick}
      cName={mockClass}
      content={mockContent}
      {...mockCustomeProps}
    />
  );

  await userEvent.click(buttonElement);

  expect(mockClick).toHaveBeenCalled();
  expect(container).toMatchSnapshot();
});

test("render component as blank link with correct props", () => {
  const { container } = render(
    <Button
      type={mockLinkType}
      url={mockUrl}
      cName={mockClass}
      content={mockContent}
      blank
      {...mockCustomeProps}
    />
  );
  const linkElement = screen.getByRole("link");

  expect(linkElement).toHaveAttribute("href", mockUrl);
  expect(linkElement.className.split(" ").includes(mockClass)).toBeTruthy();
  expect(linkElement).toHaveAttribute("aria-label", mockLabel);
  expect(linkElement).toHaveAttribute("target", "_blank");
  expect(screen.getByText(mockContent)).toBeInTheDocument();
  expect(container).toMatchSnapshot();
});

test("render component as link with correct props", () => {
  const { container } = render(
    <Button
      type={mockLinkType}
      url={mockUrl}
      cName={mockClass}
      content={mockContent}
      {...mockCustomeProps}
    />
  );
  const linkElement = screen.getByRole("link");

  expect(linkElement).toHaveAttribute("href", `/${mockUrl}`);
  expect(linkElement.className.split(" ").includes(mockClass)).toBeTruthy();
  expect(linkElement).toHaveAttribute("aria-label", mockLabel);
  expect(screen.getByText(mockContent)).toBeInTheDocument();
  expect(container).toMatchSnapshot();
});
