import { test, expect, vi, afterEach } from "vitest";
import { render, screen } from "../../../test/test-utils";
import FullPageErrorFallback from "./FullPageErrorFallback";

const mockContent = "mock_content";
const mocks = vi.hoisted(() => ({
  mockError: vi.fn().mockImplementation(() => ({ message: mockContent })),
}));

vi.mock("react-router-dom", async (importOriginal) => ({
  ...(await importOriginal()),
  useRouteError: mocks.mockError,
}));

afterEach(() => {
  mocks.mockError.mockRestore();
});

test("component should render with error message", () => {
  const { container } = render(<FullPageErrorFallback />);

  expect(screen.getByRole("alert").textContent).toMatchInlineSnapshot(
    `"Uh oh... There's a problem. Try refreshing the app.${mockContent}"`
  );
  expect(container).toMatchSnapshot();
});

test("component should render without error message", () => {
  const { container } = render(<FullPageErrorFallback />);

  expect(screen.getByRole("alert").textContent).toMatchInlineSnapshot(
    '"Uh oh... There\'s a problem. Try refreshing the app."'
  );
  expect(container).toMatchSnapshot();
});
