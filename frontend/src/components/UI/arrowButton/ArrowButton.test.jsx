import { expect, test } from "vitest";
import { render, screen } from "../../../test/test-utils";
import ArrowButton from "./ArrowButton";

test("arrow button should render correctly", () => {
  const mockUrl = "mock_url";
  const mockText = "mock_text";
  const { container } = render(<ArrowButton url={mockUrl} text={mockText} />);

  const linkElement = screen.getByText(mockText);

  expect(linkElement).toBeInTheDocument();
  expect(linkElement).toHaveAttribute("href", `/${mockUrl}`);
  expect(container).toMatchSnapshot();
});
