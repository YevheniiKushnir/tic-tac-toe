import { memo } from "react";
import { Link } from "react-router-dom";

import arrowLeft from "../../../assets/icons/ArrowLeft-w.png";
import "./ArrowButton.scss";

const ArrowButton = ({ url = "/", text = "", ...props }) => {
  return (
    <Link to={url} className="arrow-button-back" {...props}>
      <img src={arrowLeft} alt="arrow to left" />
      {text}
    </Link>
  );
};

export default memo(ArrowButton);
