import { useState, memo, useId } from "react";
import "./Input.scss";

import watchIcon from "../../../assets/icons/Eye_view.png";
import hiddeWatchIcon from "../../../assets/icons/Eye_slash.png";

const Input = ({
  value,
  dType,
  isErr,
  dispatch,
  label = "",
  placeholder = "",
  type = "text",
  validate = null,
  ...props
}) => {
  const id = useId();
  const [watchBtn, setWatchBtn] = useState(false);

  const handleChange = (e) => {
    dispatch({ type: dType, [dType]: e.target.value });
  };

  const hanldeChangeType = () => setWatchBtn(!watchBtn);

  return (
    <div className="form_input">
      {label && (
        <label
          className={isErr ? "form_input-err" : ""}
          htmlFor={id}
          data-testid={id}
        >
          {label}
        </label>
      )}
      <input
        className={isErr ? "form_input-err" : ""}
        onChange={handleChange}
        value={value}
        id={id}
        type={watchBtn ? "text" : type}
        placeholder={placeholder}
        aria-label={label || "text box"}
        {...props}
      />
      {!validate ? null : (
        <ul className="validate__list">
          {validate.map((el, i) => (
            <li
              key={i + id}
              aria-label="Validate error"
              className={
                !el.isErr
                  ? el.isOk && isErr
                    ? "validate__list__item-ok"
                    : ""
                  : "validate__list__item-err"
              }
            >
              {el.value}
            </li>
          ))}
        </ul>
      )}
      {type !== "password" ? null : (
        <button
          type="button"
          onClick={hanldeChangeType}
          className="form_input__eyeBtn"
        >
          <img
            src={watchBtn ? hiddeWatchIcon : watchIcon}
            alt="view eye icon"
          />
        </button>
      )}
    </div>
  );
};

export default memo(Input);
