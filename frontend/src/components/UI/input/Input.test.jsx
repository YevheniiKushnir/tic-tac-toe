import { expect, test, vi } from "vitest";
import { fireEvent, render, screen } from "../../../test/test-utils";
import Input from "./Input";

test("renders input with correct type", () => {
  const { container } = render(<Input />);

  const inputElement = screen.getByRole("textbox");
  expect(inputElement).toBeInTheDocument();
  expect(inputElement).toHaveAttribute("type", "text");
  expect(container).toMatchSnapshot();
});

test("renders input with correct placeholder", () => {
  const { container } = render(<Input placeholder="Enter text" />);

  expect(screen.getByPlaceholderText("Enter text")).toBeInTheDocument();
  expect(container).toMatchSnapshot();
});

test("renders input with correct label", () => {
  const { container } = render(<Input label="Test Label" />);

  expect(screen.getByText("Test Label")).toBeInTheDocument();
  expect(container).toMatchSnapshot();
});

test("handles input change event", () => {
  const mockDispatch = vi.fn();
  const { container } = render(<Input dType="test" dispatch={mockDispatch} />);

  fireEvent.change(screen.getByRole("textbox"), {
    target: { value: "test value" },
  });

  expect(mockDispatch).toHaveBeenCalledWith({
    type: "test",
    test: "test value",
  });
  expect(container).toMatchSnapshot();
});

test("handles password input type and toggle visibility", () => {
  const { container } = render(<Input type="password" />);
  const inputElement = screen.getByLabelText(/text box/i);

  expect(inputElement).toHaveAttribute("type", "password");

  const toggleButton = screen.getByRole("button");
  fireEvent.click(toggleButton);

  expect(inputElement).toHaveAttribute("type", "text");
  expect(container).toMatchSnapshot();
});

test("renders validation list", () => {
  const validate = [
    { isErr: false, isOk: true, value: "Validation 1" },
    { isErr: true, isOk: false, value: "Validation 2" },
  ];
  const { container } = render(<Input validate={validate} />);

  expect(screen.getByRole("list")).toBeInTheDocument();
  expect(screen.getAllByRole("listitem")).toHaveLength(validate.length);
  expect(container).toMatchSnapshot();
});
