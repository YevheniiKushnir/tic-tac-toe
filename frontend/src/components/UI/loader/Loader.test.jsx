import { test, expect } from "vitest";
import { render, screen } from "../../../test/test-utils";
import Loader from "./Loader";

test("component should render correctly", () => {
  const mockContent = "mock_content";
  const { container } = render(<Loader content={mockContent} />);

  expect(screen.getByTestId(/loader content/i).textContent).toBe(mockContent);
  expect(container).toMatchSnapshot();
});
