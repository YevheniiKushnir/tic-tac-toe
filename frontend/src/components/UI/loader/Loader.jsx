import "./Loader.scss";

const Loader = ({ content }) => {
  return (
    <div className="wrapper-loader" aria-label="loading">
      <span className="wrapper-loader__loader"></span>
      <p className="wrapper-loader__text" data-testid="loader content">
        {content}
      </p>
    </div>
  );
};

export default Loader;
