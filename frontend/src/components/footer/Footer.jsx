import React from "react";
import "./Footer.scss";

import { Link } from "react-router-dom";

import logo from "../../assets/images/Logo_white.png";
import fIcon from "../../assets/icons/FacebookLogo.png";
import instIcon from "../../assets/icons/InstagramLogo.png";

const Footer = () => {
  return (
    <footer className="footer">
      <div className="footer__info">
        <img src={logo} alt="logo" />
        <p>
          © 2022 EPAM Systems, Inc. <br /> All Rights Reserved.
        </p>
      </div>
      <div className="footer__media">
        <div className="footer__media_icons">
          <img src={fIcon} alt="F icon" />
          <img src={instIcon} alt="Inst icon" />
        </div>
        <div className="footer__media_links">
          <Link to={"/#"}>Contact Us</Link>
          <br />
          <Link to={"/terms&conditions"}>Terms & Conditions</Link>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
