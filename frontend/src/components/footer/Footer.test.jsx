import { test, expect } from "vitest";
import { render } from "../../test/test-utils";
import Footer from "./Footer";

test("component should render correctly", () => {
  const { container } = render(<Footer />);

  expect(container).toMatchSnapshot();
});
