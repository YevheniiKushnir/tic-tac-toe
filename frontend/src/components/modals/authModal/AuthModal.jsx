import Button from "../../UI/button/Button";
import Modal from "../../modal/Modal";

const AuthModal = ({ closeModal }) => {
  return (
    <Modal closeClick={closeModal}>
      <div className="home-welcome__modal">
        <h4 data-testid="auth_modal_heading">
          For this featur you must be authorizated, <br />
          please Sign In or Sign Up
        </h4>
        <div className="home-welcome__modal__links">
          <Button
            type="link"
            url="/sing-up"
            content="Sing Up"
            style={{ flex: 1 }}
          />
          <Button
            type="link"
            url="/sing-in"
            content="Sing In"
            style={{ flex: 1 }}
          />
        </div>
      </div>
    </Modal>
  );
};

export default AuthModal;
