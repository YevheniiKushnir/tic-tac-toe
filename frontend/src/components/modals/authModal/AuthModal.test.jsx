import { test, expect, vi } from "vitest";
import { render, screen, userEvent } from "../../../test/test-utils";
import AuthModal from "./AuthModal";

test("component should render correctly", async () => {
  const mockClick = vi.fn().mockImplementation(() => {});

  const { container } = render(<AuthModal closeModal={mockClick} />);

  await userEvent.click(screen.getByLabelText(/close modal/i));

  expect(mockClick).toHaveBeenCalled();
  expect(container).toMatchSnapshot();
});
