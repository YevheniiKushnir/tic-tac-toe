import { Link } from "react-router-dom";
import errIcon from "../../../assets/icons/err-mess.png";
import Modal from "../../modal/Modal";

const AuthFormError = ({ closeClick, errMessage }) => {
  return (
    <Modal closeClick={closeClick}>
      <div className="auth_form__modal">
        <img src={errIcon} alt="error icon" />
        <h4 data-testid="error_message">
          {errMessage ? (
            errMessage
          ) : (
            <>
              Sorry, something <br />
              went wrong.
            </>
          )}
        </h4>
        <p>
          Please try again or <Link to={"/#test"}>Contact Us</Link>
        </p>
      </div>
    </Modal>
  );
};

export default AuthFormError;
