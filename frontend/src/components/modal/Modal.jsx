import "./Modal.scss";
import closeIcon from "../../assets/icons/white-cross.svg";

const Modal = ({ children, closeClick }) => {
  return (
    <div className="modal" role="alert">
      <div className="modal__content">
        {children}
        {closeClick && (
          <button
            className="modal__closeBtn"
            onClick={closeClick}
            aria-label="close modal"
          >
            <img src={closeIcon} alt="close icon" />
          </button>
        )}
      </div>
    </div>
  );
};

export default Modal;
