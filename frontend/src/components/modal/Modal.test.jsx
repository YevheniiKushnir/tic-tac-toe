import { test, expect } from "vitest";
import { render, screen, userEvent } from "../../test/test-utils";
import Modal from "./Modal";

test("render component without user close click method", () => {
  const { container } = render(
    <Modal>
      <p>test</p>
    </Modal>
  );

  expect(screen.getByText(/test/i)).toBeInTheDocument();
  expect(container).toMatchSnapshot();
});

test("render component without user close click method", async () => {
  const mockClick = vi.fn().mockImplementation(() => {});
  const { container } = render(<Modal closeClick={mockClick} />);

  await userEvent.click(screen.getByLabelText(/close modal/i));

  expect(mockClick).toHaveBeenCalled();
  expect(container).toMatchSnapshot();
});
