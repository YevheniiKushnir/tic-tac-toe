import { test, expect } from "vitest";
import {
  calculateNextValue,
  calculateStatus,
  calculateWinner,
  simulateBot,
} from "./localGameCalculation";

const SymbolX = "X";
const SymbolO = "O";
const createTestWinSquar = (cells = [], symbol = SymbolX, size = 9) => {
  const squar = new Array(size);

  cells.forEach((cell) => squar.splice(cell, 1, symbol));

  return squar;
};

test("calculate user game status", () => {
  const userWin = calculateStatus(SymbolX, SymbolX);
  const userLost = calculateStatus(SymbolX, SymbolO);
  const userDraw = calculateStatus(null, SymbolX);

  expect(userWin).toBe("You win!");
  expect(userLost).toBe("You lost");
  expect(userDraw).toBe("Draw");
});

test("calculate next user turn", () => {
  const userXSquares = createTestWinSquar();
  const userOSquares = createTestWinSquar([2]);

  const turnX = calculateNextValue(userXSquares, SymbolX);
  const turnO = calculateNextValue(userOSquares, SymbolX);
  const turnX2 = calculateNextValue(userOSquares, SymbolO);

  expect(turnX, SymbolX);
  expect(turnX2, SymbolX);
  expect(turnO, SymbolO);
});

test("simulate bot turn", () => {
  const squares = new Array(10);

  const botTurn = simulateBot(squares, SymbolO);

  expect(botTurn.filter(Boolean)).toEqual([SymbolX]);
});

test.each([
  [
    calculateWinner(createTestWinSquar([0, 1, 2])),
    SymbolX,
    "rotateZ(0deg) scale(1.1) translateY(-112px)",
  ],
  [
    calculateWinner(createTestWinSquar([3, 4, 5])),
    SymbolX,
    "rotateZ(0deg) scale(1.1)",
  ],
  [
    calculateWinner(createTestWinSquar([6, 7, 8])),
    SymbolX,
    "rotateZ(0deg) scale(1.1) translateY(107px)",
  ],
  [
    calculateWinner(createTestWinSquar([0, 3, 6], SymbolO)),
    SymbolO,
    "rotateZ(90deg) scale(1.1) translateY(113px)",
  ],
  [
    calculateWinner(createTestWinSquar([1, 4, 7])),
    SymbolX,
    "rotateZ(90deg) scale(1.1) translateY(0)",
  ],
  [
    calculateWinner(createTestWinSquar([2, 5, 8])),
    SymbolX,
    "rotateZ(90deg) scale(1.1) translateY(-113px)",
  ],
  [
    calculateWinner(createTestWinSquar([0, 4, 8])),
    SymbolX,
    "rotateZ(45deg) scale(1.1)",
  ],
  [
    calculateWinner(createTestWinSquar([2, 4, 6], SymbolO)),
    SymbolO,
    "rotateZ(-45deg) scale(1.1)",
  ],
])("calculate game winner: #%#", (result, winner, line) => {
  expect(result).toEqual({
    winner,
    line,
  });
});
