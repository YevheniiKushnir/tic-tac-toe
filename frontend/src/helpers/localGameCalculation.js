export function calculateNextValue(squares, userSymbol) {
  return squares.filter(Boolean).length % 2 === 0
    ? userSymbol
    : userSymbol !== "O"
    ? "O"
    : "X";
}

export function calculateWinner(squares) {
  const lines = [
    {
      variants: [0, 1, 2],
      line: "rotateZ(0deg) scale(1.1) translateY(-112px)",
    },
    { variants: [3, 4, 5], line: "rotateZ(0deg) scale(1.1)" },
    { variants: [6, 7, 8], line: "rotateZ(0deg) scale(1.1) translateY(107px)" },
    {
      variants: [0, 3, 6],
      line: "rotateZ(90deg) scale(1.1) translateY(113px)",
    },
    { variants: [1, 4, 7], line: "rotateZ(90deg) scale(1.1) translateY(0)" },
    {
      variants: [2, 5, 8],
      line: "rotateZ(90deg) scale(1.1) translateY(-113px)",
    },
    { variants: [0, 4, 8], line: "rotateZ(45deg) scale(1.1)" },
    { variants: [2, 4, 6], line: "rotateZ(-45deg) scale(1.1)" },
  ];
  for (let i = 0; i < lines.length; i++) {
    const [a, b, c] = lines[i].variants;
    const line = lines[i].line;
    if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
      return {
        winner: squares[a],
        line,
      };
    }
  }
  return {
    winner: null,
    line: null,
  };
}

export function calculateStatus(winner, userSymbol) {
  return winner === userSymbol
    ? "You win!"
    : winner !== null
    ? "You lost"
    : "Draw";
}

export function simulateBot(squares, userSymbol) {
  const squaresCopy = [...squares];
  const botSymbol = userSymbol === "X" ? "O" : "X";
  const randomIndex = Math.floor(Math.random() * squaresCopy.length);

  if (!squaresCopy[randomIndex]) {
    squaresCopy[randomIndex] = botSymbol;
    return squaresCopy;
  }

  return simulateBot(squares, userSymbol);
}
