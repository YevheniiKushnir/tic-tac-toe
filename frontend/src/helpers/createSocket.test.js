import { expect, test, vi, afterEach } from "vitest";
import createSocket, { socketPath } from "./createSocket";
import { baseURL } from "../redux/constants";

const mocks = vi.hoisted(() => ({
  mockIo: vi.fn().mockReturnValue("mockIoResult"),
}));

vi.mock("socket.io-client", () => ({
  io: mocks.mockIo,
}));

afterEach(() => {
  mocks.mockIo.mockClear();
});

const mockPath = "mock_path";
const mockToken = "mock_token";

test("create io instance with path", () => {
  const socket = createSocket({ token: mockToken, path: mockPath });

  expect(socket).toBe("mockIoResult");
  expect(mocks.mockIo).toHaveBeenCalledWith(`${baseURL}${mockPath}`, {
    path: socketPath,
    auth: {
      token: mockToken,
    },
  });
  expect(mocks.mockIo).toHaveBeenCalledOnce();
});

test("create io instance without path", () => {
  const socket = createSocket({ token: mockToken });

  expect(socket).toBe("mockIoResult");
  expect(mocks.mockIo).toHaveBeenCalledWith(baseURL, {
    path: socketPath,
    auth: {
      token: mockToken,
    },
  });
  expect(mocks.mockIo).toHaveBeenCalledOnce();
});
