export const setLSValue = (key, value) => {
  localStorage.setItem(key, JSON.stringify(value));
};

export const getLSValue = (key) => {
  const value = localStorage.getItem(key);
  if (!value) return null;

  return JSON.parse(value);
};
