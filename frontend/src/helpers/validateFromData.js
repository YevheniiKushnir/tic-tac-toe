const validateFormData = (data = {}, exclude = 1) => {
  const isValidArr = [];

  Object.keys(data).forEach((key) => {
    const option = data[key];

    if (!option.required || !option.touched) {
      isValidArr.push(false);
      return;
    }

    if (new RegExp(option.pattern).test(option.value)) {
      option.isErr = false;
    } else if (option.pattern) {
      option.isErr = true;
    }

    if (Array.isArray(option.validate)) {
      option.validate.forEach((el) => {
        if (new RegExp(el.pattern).test(option.value)) {
          el.isOk = true;
          el.isErr = false;
        } else {
          el.isErr = true;
          option.isErr = true;
        }
      });

      if (option.isErr) return isValidArr.push(false);
    }
  });

  return { ...data, isValid: isValidArr.length <= exclude ? true : false };
};

export default validateFormData;
