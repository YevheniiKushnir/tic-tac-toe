import { io } from "socket.io-client";

export const socketPath = "/live-game";

function createSocket({ token, path = "" }) {
  const socket = io(window.location.origin + "/" + path, {
    path: socketPath,
    auth: {
      token,
    },
  });

  return socket;
}

export default createSocket;
