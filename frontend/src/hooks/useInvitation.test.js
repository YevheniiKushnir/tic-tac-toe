import { test, expect, vi, afterEach } from "vitest";
import { act, renderWithHook, waitFor } from "../test/test-utils";
import { tokenOne, uidOne, uidTwo } from "../test/data/constants";
import useInvitation, { pathToGameOnlinePage } from "./useInvitation";

const mockUserOne = {
  uid: uidOne,
  isAuth: true,
  token: tokenOne,
};

const mockUseNavigate = vi.fn();

vi.mock("react-router-dom", async (importActual) => ({
  ...(await importActual()),
  useNavigate: () => mockUseNavigate,
}));

afterEach(() => {
  mockUseNavigate.mockReset();
  vi.clearAllMocks();
});

test("render invitation hook without authenticated user", () => {
  const { result } = renderWithHook(() => useInvitation());

  expect(result.current).toEqual({
    uid: undefined,
    user: undefined,
    openInviting: expect.any(Function),
    submitInviting: expect.any(Function),
    cancelInviting: expect.any(Function),
  });
});

test("render invitation hook with authenticated user", () => {
  const { result } = renderWithHook(() => useInvitation(), {
    user: mockUserOne,
  });

  expect(result.current).toEqual({
    uid: uidOne,
    user: undefined,
    openInviting: expect.any(Function),
    submitInviting: expect.any(Function),
    cancelInviting: expect.any(Function),
  });
});

test("user send inviting", () => {
  const { result } = renderWithHook(() => useInvitation(), {
    user: mockUserOne,
  });

  act(() => {
    result.current.openInviting(uidTwo);
  });

  expect(result.current).toEqual({
    uid: uidOne,
    user: {
      existTime: 2,
      users: [uidOne, uidTwo],
      user: {
        history: {
          draw: 0,
          userLose: 0,
          userWon: 0,
        },
        name: expect.any(String),
        raiting: 0,
        topRaiting: 0,
        uid: uidTwo,
      },
      isSender: true,
      isRecipient: false,
    },
    openInviting: expect.any(Function),
    submitInviting: expect.any(Function),
    cancelInviting: expect.any(Function),
  });
});

test("user is recipient", () => {
  const userOne = renderWithHook(() => useInvitation(), {
    user: mockUserOne,
  });
  const userTwo = renderWithHook(() => useInvitation(), {
    user: { ...mockUserOne, uid: uidTwo },
  });

  act(() => {
    userOne.result.current.openInviting(uidTwo);
  });

  expect(userOne.result.current).toEqual({
    uid: uidOne,
    user: {
      existTime: 2,
      users: [uidOne, uidTwo],
      user: {
        history: {
          draw: 0,
          userLose: 0,
          userWon: 0,
        },
        name: expect.any(String),
        raiting: 0,
        topRaiting: 0,
        uid: uidTwo,
      },
      isSender: true,
      isRecipient: false,
    },
    openInviting: expect.any(Function),
    submitInviting: expect.any(Function),
    cancelInviting: expect.any(Function),
  });
  expect(userTwo.result.current).toEqual({
    uid: uidTwo,
    user: {
      existTime: 2,
      user: {
        history: {
          draw: 0,
          userLose: 0,
          userWon: 0,
        },
        name: expect.any(String),
        raiting: 0,
        topRaiting: 0,
        uid: uidOne,
      },
      users: [uidOne, uidTwo],
      isSender: false,
      isRecipient: true,
    },
    openInviting: expect.any(Function),
    submitInviting: expect.any(Function),
    cancelInviting: expect.any(Function),
  });
});

test("user submit inviting", () => {
  const userOne = renderWithHook(() => useInvitation(), {
    user: mockUserOne,
  });
  const userTwo = renderWithHook(() => useInvitation(), {
    user: { ...mockUserOne, uid: uidTwo },
  });

  act(() => {
    userOne.result.current.openInviting(uidTwo);
  });

  act(() => {
    userTwo.result.current.submitInviting();
  });

  expect(userOne.result.current).toEqual({
    uid: uidOne,
    user: {
      isRecipient: false,
      isSender: false,
    },
    openInviting: expect.any(Function),
    submitInviting: expect.any(Function),
    cancelInviting: expect.any(Function),
  });
  expect(userTwo.result.current).toEqual({
    uid: uidTwo,
    user: {
      isSender: false,
      isRecipient: false,
    },
    openInviting: expect.any(Function),
    submitInviting: expect.any(Function),
    cancelInviting: expect.any(Function),
  });
  expect(mockUseNavigate).toHaveBeenCalled(2);
  expect(mockUseNavigate).toHaveBeenCalledWith(pathToGameOnlinePage, {
    state: { roomId: expect.any(String), byInvinting: true },
  });
});

test("user cancel inviting", () => {
  const userOne = renderWithHook(() => useInvitation(), {
    user: mockUserOne,
  });
  const userTwo = renderWithHook(() => useInvitation(), {
    user: { ...mockUserOne, uid: uidTwo },
  });

  act(() => {
    userOne.result.current.openInviting(uidTwo);
  });

  act(() => {
    userTwo.result.current.cancelInviting();
  });

  expect(userOne.result.current).toEqual({
    uid: uidOne,
    user: {
      isRecipient: false,
      isSender: false,
    },
    openInviting: expect.any(Function),
    submitInviting: expect.any(Function),
    cancelInviting: expect.any(Function),
  });
  expect(userTwo.result.current).toEqual({
    uid: uidTwo,
    user: {
      isSender: false,
      isRecipient: false,
    },
    openInviting: expect.any(Function),
    submitInviting: expect.any(Function),
    cancelInviting: expect.any(Function),
  });
  expect(mockUseNavigate).not.toHaveBeenCalled();
});
