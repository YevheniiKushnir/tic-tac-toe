import { useCallback, useLayoutEffect, useRef, useState } from "react";
import { useSelector } from "react-redux";
import { useLocation } from "react-router-dom";

import createSocket from "../helpers/createSocket";

const useOnlineGame = () => {
  const location = useLocation();
  const { token, uid } = useSelector((state) => state.user);
  const [state, setState] = useState(null);
  const socketRef = useRef(undefined);
  const roomIdRef = useRef(undefined);

  const selectSquare = useCallback((square) => {
    socketRef.current.emit("user-select-square", {
      square,
      roomId: roomIdRef.current,
    });
  }, []);

  const selectReaction = useCallback((img) => {
    socketRef.current.emit("user-select-reaction", {
      img,
      roomId: roomIdRef.current,
    });
  }, []);

  const selectPlayAgain = useCallback(() => {
    socketRef.current.emit("user-select-play-again", {
      roomId: roomIdRef.current,
      uid,
    });
  }, []);

  const userFaileGame = useCallback(() => {
    socketRef.current.emit("user-faile-game", {
      uid,
      roomId: roomIdRef.current,
    });
  }, []);

  useLayoutEffect(() => {
    socketRef.current = createSocket({ token });

    socketRef.current.on("update-room-info", ({ data }) => {
      const { roomId: newRoomID, ...state } = data;
      setState((prev) => ({ ...prev, ...state }));
      roomIdRef.current = newRoomID;
    });

    socketRef.current.on("connect", () => {
      socketRef.current.emit("join-room", { ...location.state, uid });
    });

    return () => {
      socketRef.current.emit("user-leave-room", {
        uid,
        roomId: roomIdRef.current,
      });
      socketRef.current.disconnect();
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return {
    state,
    uid,
    selectSquare,
    selectPlayAgain,
    selectReaction,
    userFaileGame,
  };
};

export default useOnlineGame;
