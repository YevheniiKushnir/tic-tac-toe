import { useCallback, useLayoutEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import {
  calculateNextValue,
  calculateStatus,
  calculateWinner,
} from "../helpers/localGameCalculation";
import {
  changeComplexity,
  changeSquares,
  changeUserSymbol,
  drawWin,
  fetchSaveProgress,
  restartLocalGame,
  robotWin,
  simulateRobot,
  updateStatus,
  userWin,
} from "../redux/features/localGame";

const useLocalGame = () => {
  const dispatch = useDispatch();
  const navigation = useNavigate();

  const {
    squares,
    userSymbol,
    userHistory,
    complexity,
    status,
    complexityData,
    symbolsData,
    isPlay,
    saveSessionEnable,
  } = useSelector((state) => state.localGame);
  const isAuth = useSelector((state) => state.user.isAuth);

  const nextValue = calculateNextValue(squares, userSymbol);
  const { winner, line } = calculateWinner(squares);

  const selectSquare = (square) => {
    if (winner || squares[square]) {
      return;
    }
    const squaresCopy = [...squares];
    squaresCopy[square] = nextValue;
    dispatch(changeSquares(squaresCopy));
  };

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const restart = useCallback(() => dispatch(restartLocalGame()), []);

  const setComplexity = useCallback(
    (value) => dispatch(changeComplexity(value)),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  );
  const setSymbol = useCallback(
    (value) => dispatch(changeUserSymbol(value)),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  );

  useLayoutEffect(() => {
    return () => {
      if (isAuth) {
        dispatch(fetchSaveProgress({ reWrite: true }));
      }
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isAuth]);

  useLayoutEffect(() => {
    if ((winner || squares.every(Boolean)) && isPlay) {
      const status = calculateStatus(winner, userSymbol);
      dispatch(updateStatus(status));

      if (winner === null) {
        dispatch(drawWin());
      } else if (winner === userSymbol) {
        dispatch(userWin());
      } else if (winner !== userSymbol) {
        dispatch(robotWin());
      }
    } else if (nextValue !== userSymbol && !squares.every(Boolean) && isPlay) {
      dispatch(simulateRobot());
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [squares, nextValue, userSymbol, isPlay, winner]);

  useLayoutEffect(() => {
    return () => {
      if (isAuth) return;

      if (
        (userHistory.userWon > 0 ||
          userHistory.userLose > 0 ||
          userHistory.draw > 0) &&
        !saveSessionEnable
      ) {
        navigation("/game/play-with-robot", { state: { activeModal: true } });
      }
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [userHistory, isAuth, saveSessionEnable]);

  return {
    userHistory,
    userSymbol,
    complexity,
    status,
    squares,
    complexityData,
    symbolsData,
    setSymbol,
    setComplexity,
    restart,
    selectSquare,
    line,
  };
};

export default useLocalGame;
