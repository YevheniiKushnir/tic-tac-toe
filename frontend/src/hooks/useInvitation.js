import { useCallback, useLayoutEffect, useRef, useState } from "react";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";

import createSocket from "../helpers/createSocket";

export const pathToGameOnlinePage = "game/play-online";

function useInvitation() {
  const navigate = useNavigate();
  const { token, uid, isAuth } = useSelector((state) => state.user);
  const [user, setUser] = useState(undefined);
  const socketRef = useRef(undefined);

  const redirecToGame = (roomId) => {
    navigate(pathToGameOnlinePage, { state: { roomId, byInvinting: true } });
  };

  const openInviting = (to) => {
    socketRef.current.emit("inviting-open", { users: [uid, to] });
  };

  const submitInviting = () => {
    socketRef.current.emit(
      "inviting-submit",
      { users: user.users },
      ({ roomId }) => {
        setUser({ isSender: false, isRecipient: false });
        redirecToGame(roomId);
      }
    );
  };

  const cancelInviting = () => {
    socketRef.current.emit("inviting-cancel", {
      users: [uid, user.users.find((u) => u !== uid)],
    });
  };

  useLayoutEffect(() => {
    if (!isAuth) return;

    socketRef.current = createSocket({ token, path: "inviting" });

    socketRef.current.on("connect", () => {
      socketRef.current.emit("inviting-init", { uid });
    });

    socketRef.current.on("inviting-sender", ({ data }) => {
      setUser({ ...data, isSender: true, isRecipient: false });
    });
    socketRef.current.on("inviting-recipient", ({ data }) => {
      setUser({ ...data, isSender: false, isRecipient: true });
    });
    socketRef.current.on("inviting-submited-sender", ({ data }) => {
      setUser({ isSender: false, isRecipient: false });
      redirecToGame(data.roomId);
    });
    socketRef.current.on("inviting-canceled", () => {
      setUser({ isSender: false, isRecipient: false });
    });

    return () => {
      socketRef.current?.disconnect();
      socketRef.current = undefined;
    };
  }, [isAuth]);

  return { uid, user, openInviting, submitInviting, cancelInviting };
}

export default useInvitation;
