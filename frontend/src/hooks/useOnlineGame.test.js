import { expect, test } from "vitest";
import { act, renderWithHook, waitFor } from "../test/test-utils";
import {
  symbolX,
  symbolO,
  tokenOne,
  uidOne,
  uidTwo,
} from "../test/data/constants";
import useOnlineGame from "./useOnlineGame";

const mockUser = {
  uid: uidOne,
  token: tokenOne,
};

function renderHook(preloadedState = { user: mockUser }) {
  return renderWithHook(() => useOnlineGame(), preloadedState);
}

test("initial render should return correct result", () => {
  const { result } = renderHook();

  expect(result.current).toEqual({
    state: {
      users: [
        {
          again: 0,
          history: {
            draw: 0,
            userLose: 0,
            userWon: 0,
          },
          name: expect.any(String),
          raiting: 0,
          status: undefined,
          symbol: "X",
          topRaiting: 0,
          uid: "mock_uid_one",
        },
      ],
      squares: [null, null, null, null, null, null, null, null, null],
      timer: 10,
      turn: symbolX,
      quikeFinish: false,
      reactions: [],
      symbolsData: [{ text: symbolX }, { text: symbolO }],
      play: false,
      line: undefined,
      playAgain: 0,
      reactionTimer: 1,
      winner: undefined,
    },
    uid: uidOne,
    selectSquare: expect.any(Function),
    selectPlayAgain: expect.any(Function),
    selectReaction: expect.any(Function),
    userFaileGame: expect.any(Function),
  });
});

test("users can join into random room", () => {
  const userOne = renderHook();
  const userTwo = renderHook({ user: { ...mockUser, uid: uidTwo } });

  expect(userTwo.result.current).toEqual({
    state: {
      users: [
        {
          again: 0,
          history: {
            draw: 0,
            userLose: 0,
            userWon: 0,
          },
          name: expect.any(String),
          raiting: 0,
          status: undefined,
          symbol: symbolX,
          topRaiting: 0,
          uid: uidOne,
        },
        {
          again: 0,
          history: {
            draw: 0,
            userLose: 0,
            userWon: 0,
          },
          name: expect.any(String),
          raiting: 0,
          status: undefined,
          symbol: symbolO,
          topRaiting: 0,
          uid: uidTwo,
        },
      ],
      squares: [null, null, null, null, null, null, null, null, null],
      timer: 10,
      turn: symbolX,
      quikeFinish: false,
      reactions: [],
      symbolsData: [{ text: symbolX }, { text: symbolO }],
      play: true,
      line: undefined,
      playAgain: 0,
      reactionTimer: 1,
      winner: undefined,
    },
    uid: uidTwo,
    selectSquare: expect.any(Function),
    selectPlayAgain: expect.any(Function),
    selectReaction: expect.any(Function),
    userFaileGame: expect.any(Function),
  });
});

test("users can play and start new game", () => {
  const userOne = renderHook();
  const userTwo = renderHook({ user: { ...mockUser, uid: uidTwo } });

  act(() => {
    userOne.result.current.selectSquare(0);
  });
  act(() => {
    userTwo.result.current.selectSquare(3);
  });

  expect(userOne.result.current.state).toEqual(userTwo.result.current.state);
  expect(userOne.result.current.state.squares).toEqual([
    "X",
    null,
    null,
    "O",
    null,
    null,
    null,
    null,
    null,
  ]);
  expect(userOne.result.current.state.turn).toBe(symbolX);

  act(() => {
    userOne.result.current.selectSquare(1);
  });
  act(() => {
    userOne.result.current.selectSquare(3);
  });
  act(() => {
    userTwo.result.current.selectSquare(4);
  });
  act(() => {
    userOne.result.current.selectSquare(2);
  });
  act(() => {
    userTwo.result.current.selectSquare(5);
  });

  expect(userOne.result.current.state).toEqual(userTwo.result.current.state);
  expect(userOne.result.current.state.winner).toBe(symbolX);
  expect(userOne.result.current.state.line).toBe(
    "rotateZ(0deg) scale(1.1) translateY(-112px)"
  );
  expect(userOne.result.current.state.play).toBeFalsy();
  expect(userOne.result.current.state.squares).toEqual([
    "X",
    "X",
    "X",
    "O",
    "O",
    null,
    null,
    null,
    null,
  ]);
  expect(userOne.result.current.state.users[0]).toEqual({
    status: "You win!",
    again: 0,
    uid: uidOne,
    symbol: symbolX,
    history: { draw: 0, userLose: 0, userWon: 1 },
    raiting: 10,
    topRaiting: 0,
    name: expect.any(String),
  });
  expect(userTwo.result.current.state.users[1]).toEqual({
    status: "You lost",
    again: 0,
    uid: uidTwo,
    symbol: symbolO,
    history: { draw: 0, userLose: 1, userWon: 0 },
    raiting: 0,
    topRaiting: 0,
    name: expect.any(String),
  });

  act(() => {
    userOne.result.current.selectPlayAgain();
  });
  act(() => {
    userOne.result.current.selectPlayAgain();
  });

  expect(userTwo.result.current.state.users[0].again).toBe(1);
  expect(userTwo.result.current.state.playAgain).toBe(1);

  act(() => {
    userTwo.result.current.selectPlayAgain();
  });

  expect(userOne.result.current.state.play).toBeTruthy();
  expect(userOne.result.current.state.squares).toEqual([
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
  ]);
  expect(userOne.result.current.state.turn).toBe(symbolO);
  expect(userTwo.result.current.state.playAgain).toBe(0);
});

test("users can send reactions", async () => {
  const mockReactionImgOne = "mock_img_one";
  const mockReactionImgTwo = "mock_img_two";
  const userOne = renderHook();

  act(() => {
    userOne.result.current.selectReaction(mockReactionImgOne);
  });

  expect(userOne.result.current.state.reactions).toEqual([
    {
      id: expect.any(String),
      path: mockReactionImgOne,
    },
  ]);

  await waitFor(
    () => expect(userOne.result.current.state.reactions).toHaveLength(0),
    { timeout: 4000 }
  );

  const userTwo = renderHook({ user: { ...mockUser, uid: uidTwo } });

  act(() => {
    userTwo.result.current.selectReaction(mockReactionImgTwo);
  });
  act(() => {
    userTwo.result.current.selectReaction(mockReactionImgOne);
  });

  expect(userOne.result.current.state).toEqual(userTwo.result.current.state);
  expect(userOne.result.current.state.reactions).toEqual([
    {
      id: expect.any(String),
      path: mockReactionImgTwo,
    },
    {
      id: expect.any(String),
      path: mockReactionImgOne,
    },
  ]);

  await waitFor(
    () => expect(userOne.result.current.state.reactions).toHaveLength(0),
    { timeout: 4000 }
  );

  expect(userOne.result.current.state.reactions).toHaveLength(0);
});

test("user loses game due to lack of time for his turn", () => {
  const userOne = renderHook();
  const userTwo = renderHook({ user: { ...mockUser, uid: uidTwo } });

  act(() => {
    userOne.result.current.userFaileGame();
  });

  expect(userTwo.result.current.state.users[1]).toEqual({
    status: "You win!",
    again: 0,
    uid: uidTwo,
    symbol: symbolO,
    history: { draw: 0, userLose: 0, userWon: 1 },
    raiting: 10,
    topRaiting: 0,
    name: expect.any(String),
  });
  expect(userOne.result.current.state.users[0]).toEqual({
    status: "You lost",
    again: 0,
    uid: uidOne,
    symbol: symbolX,
    history: { draw: 0, userLose: 1, userWon: 0 },
    raiting: 0,
    topRaiting: 0,
    name: expect.any(String),
  });
  expect(userOne.result.current.state.quikeFinish).toBeTruthy();
});

test("user wins game because another user leaves the game", () => {
  const userOne = renderHook();
  const userTwo = renderHook({ user: { ...mockUser, uid: uidTwo } });

  userTwo.unmount();

  expect(userOne.result.current.state.users[0]).toEqual({
    status: "You win!",
    again: 0,
    uid: uidOne,
    symbol: symbolX,
    history: { draw: 0, userLose: 0, userWon: 1 },
    raiting: 10,
    topRaiting: 0,
    name: expect.any(String),
  });
  expect(userOne.result.current.state.users).toHaveLength(1);
});
