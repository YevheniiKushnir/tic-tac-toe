import { test, expect, vi, beforeEach, afterAll } from "vitest";
import { act, renderWithHook } from "../test/test-utils";
import useLocalGame from "./useLocalGame";

let botSteps = [3, 4, 6, 5];
const mocks = vi.hoisted(() => {
  return {
    mockSimulateBot: vi.fn().mockImplementation((squares, userSymbol) => {
      const squaresCopy = [...squares];
      const botSymbol = userSymbol === "X" ? "O" : "X";
      const botStep = botSteps.shift();

      if (botStep) squaresCopy[botStep] = botSymbol;

      return squaresCopy;
    }),
  };
});

vi.mock("../helpers/localGameCalculation.js", async (importOriginal) => ({
  ...(await importOriginal()),
  simulateBot: mocks.mockSimulateBot,
}));

beforeEach(() => {
  botSteps = [3, 4, 6];
});
afterAll(() => {
  vi.resetAllMocks();
});

function renderHook() {
  return renderWithHook(() => useLocalGame(), { user: { isAuth: true } });
}

test("initial render should return correct result", () => {
  const { result } = renderHook();

  expect(result.current).toEqual({
    userHistory: { userWon: 0, draw: 0, userLose: 0 },
    userSymbol: "X",
    complexity: "Easy",
    status: null,
    line: null,
    squares: [null, null, null, null, null, null, null, null, null],
    complexityData: [{ text: "Easy" }, { text: "Medium" }, { text: "Hard" }],
    symbolsData: [{ text: "X" }, { text: "O" }],
    setSymbol: expect.any(Function),
    setComplexity: expect.any(Function),
    restart: expect.any(Function),
    selectSquare: expect.any(Function),
  });
});

test("user win game", () => {
  const { result } = renderHook();

  act(() => {
    result.current.selectSquare(0);
  });

  act(() => {
    result.current.selectSquare(1);
  });

  act(() => {
    result.current.selectSquare(2);
  });

  expect(result.current).toEqual({
    userHistory: { userWon: 1, draw: 0, userLose: 0 },
    userSymbol: "X",
    complexity: "Easy",
    status: "You win!",
    line: "rotateZ(0deg) scale(1.1) translateY(-112px)",
    squares: ["X", "X", "X", "O", "O", null, null, null, null],
    complexityData: [{ text: "Easy" }, { text: "Medium" }, { text: "Hard" }],
    symbolsData: [{ text: "X" }, { text: "O" }],
    setSymbol: expect.any(Function),
    setComplexity: expect.any(Function),
    restart: expect.any(Function),
    selectSquare: expect.any(Function),
  });
});

test("user lost game", () => {
  botSteps = [3, 4, 6, 5];
  const { result } = renderHook();

  act(() => {
    result.current.selectSquare(0);
  });
  act(() => {
    result.current.selectSquare(1);
  });
  act(() => {
    result.current.selectSquare(8);
  });
  act(() => {
    result.current.selectSquare(7);
  });

  expect(result.current).toEqual({
    userHistory: { userWon: 0, draw: 0, userLose: 1 },
    userSymbol: "X",
    complexity: "Easy",
    status: "You lost",
    line: "rotateZ(0deg) scale(1.1)",
    squares: ["X", "X", null, "O", "O", "O", "O", "X", "X"],
    complexityData: [{ text: "Easy" }, { text: "Medium" }, { text: "Hard" }],
    symbolsData: [{ text: "X" }, { text: "O" }],
    setSymbol: expect.any(Function),
    setComplexity: expect.any(Function),
    restart: expect.any(Function),
    selectSquare: expect.any(Function),
  });
});

test("draw game", () => {
  botSteps = [4, 5, 6, 1];
  const { result } = renderHook();

  act(() => {
    result.current.selectSquare(0);
  });
  act(() => {
    result.current.selectSquare(2);
  });
  act(() => {
    result.current.selectSquare(8);
  });
  act(() => {
    result.current.selectSquare(7);
  });
  act(() => {
    result.current.selectSquare(3);
  });

  expect(result.current).toEqual({
    userHistory: { userWon: 0, draw: 1, userLose: 0 },
    userSymbol: "X",
    complexity: "Easy",
    status: "Draw",
    line: null,
    squares: ["X", "O", "X", "X", "O", "O", "O", "X", "X"],
    complexityData: [{ text: "Easy" }, { text: "Medium" }, { text: "Hard" }],
    symbolsData: [{ text: "X" }, { text: "O" }],
    setSymbol: expect.any(Function),
    setComplexity: expect.any(Function),
    restart: expect.any(Function),
    selectSquare: expect.any(Function),
  });
});

test("user can't change symbol and complexity only before game", () => {
  const symbolO = "O";
  const symbolX = "X";
  const complexityMedium = "Medium";
  const complexityEasy = "Easy";

  const { result } = renderHook();

  act(() => {
    result.current.setSymbol(symbolO);
  });

  expect(result.current.userSymbol).toBe(symbolO);

  act(() => {
    result.current.setComplexity(complexityMedium);
  });

  expect(result.current.complexity).toBe(complexityMedium);

  act(() => {
    result.current.selectSquare(0);
  });

  expect(result.current.squares.filter(Boolean)).toEqual([symbolO, symbolX]);

  act(() => {
    result.current.setComplexity(complexityEasy);
  });

  act(() => {
    result.current.setSymbol(symbolX);
  });

  expect(result.current.complexity).toBe(complexityMedium);
  expect(result.current.userSymbol).toBe(symbolO);
});

test("user can restart game", () => {
  const { result } = renderHook();

  act(() => {
    result.current.selectSquare(0);
  });

  act(() => {
    result.current.selectSquare(1);
  });

  act(() => {
    result.current.selectSquare(2);
  });

  expect(result.current).toEqual({
    userHistory: { userWon: 1, draw: 0, userLose: 0 },
    userSymbol: "X",
    complexity: "Easy",
    status: "You win!",
    line: "rotateZ(0deg) scale(1.1) translateY(-112px)",
    squares: ["X", "X", "X", "O", "O", null, null, null, null],
    complexityData: [{ text: "Easy" }, { text: "Medium" }, { text: "Hard" }],
    symbolsData: [{ text: "X" }, { text: "O" }],
    setSymbol: expect.any(Function),
    setComplexity: expect.any(Function),
    restart: expect.any(Function),
    selectSquare: expect.any(Function),
  });

  act(() => {
    result.current.restart();
  });

  expect(result.current).toEqual({
    userHistory: { userWon: 1, draw: 0, userLose: 0 },
    userSymbol: "X",
    complexity: "Easy",
    status: null,
    line: null,
    squares: [null, null, null, null, null, null, null, null, null],
    complexityData: [{ text: "Easy" }, { text: "Medium" }, { text: "Hard" }],
    symbolsData: [{ text: "X" }, { text: "O" }],
    setSymbol: expect.any(Function),
    setComplexity: expect.any(Function),
    restart: expect.any(Function),
    selectSquare: expect.any(Function),
  });
});
