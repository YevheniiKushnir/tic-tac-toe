import { configureStore } from "@reduxjs/toolkit";

import localGameSlice from "./features/localGame";
import userSlice, { getUserFromLS } from "./features/userSlice";
import { leaderBoardApi } from "./services/leaderBoard";
import { userProfileApi } from "./services/userProfile";

export const setupStore = (preloadedState = {}) =>
  configureStore({
    reducer: {
      user: userSlice,
      localGame: localGameSlice,
      [userProfileApi.reducerPath]: userProfileApi.reducer,
      [leaderBoardApi.reducerPath]: leaderBoardApi.reducer,
    },
    middleware: (getDefaultMiddleware) =>
      getDefaultMiddleware().concat(
        userProfileApi.middleware,
        leaderBoardApi.middleware
      ),
    preloadedState,
  });

const store = setupStore();

store.dispatch(getUserFromLS());

export default store;
