import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { baseURL } from "../constants";

export const leaderBoardApi = createApi({
  reducerPath: "leaderBoard",
  baseQuery: fetchBaseQuery({
    baseUrl: `${baseURL}api/user/leaders`,
    prepareHeaders: (headers, { getState }) => {
      const token = getState().user.token;
      if (token) {
        headers.set("authorization", token);
      }
      return headers;
    },
  }),
  tagTypes: ["Leaders"],
  endpoints: (builder) => ({
    getLeaders: builder.query({
      query: () => ({
        method: "GET",
      }),
      providesTags: ["Leaders"],
    }),
  }),
});

export const { useGetLeadersQuery } = leaderBoardApi;
