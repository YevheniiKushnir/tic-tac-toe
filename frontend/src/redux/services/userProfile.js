import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { baseURL } from "../constants";

export const userProfileApi = createApi({
  reducerPath: "userProfile",
  baseQuery: fetchBaseQuery({
    baseUrl: `${baseURL}api/user`,
    prepareHeaders: (headers, { getState }) => {
      const token = getState().user.token;
      if (token) {
        headers.set("authorization", token);
      }
      return headers;
    },
  }),
  tagTypes: ["UserProfile"],
  endpoints: (builder) => ({
    getUserProfile: builder.query({
      query: () => ({
        method: "GET",
      }),
      providesTags: ["UserProfile"],
    }),
    updateNickname: builder.mutation({
      query: (name) => ({
        url: "nickname",
        method: "PATCH",
        body: { name },
      }),
      invalidatesTags: ["UserProfile"],
    }),
    updateEmail: builder.mutation({
      query: (email) => ({
        url: "email",
        method: "PATCH",
        body: { email },
      }),
      invalidatesTags: ["UserProfile"],
    }),
    updatePassword: builder.mutation({
      query: (body) => ({
        url: "password",
        method: "PATCH",
        body,
      }),
      invalidatesTags: ["UserProfile"],
    }),
  }),
});

export const {
  useGetUserProfileQuery,
  useUpdateEmailMutation,
  useUpdateNicknameMutation,
  useUpdatePasswordMutation,
} = userProfileApi;
