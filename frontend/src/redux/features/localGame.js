import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axios from "axios";

import { simulateBot } from "../../helpers/localGameCalculation";
import { fetchLogin, getUserFromLS } from "./userSlice";

export const fetchSaveProgress = createAsyncThunk(
  "localGame/saveProgress",
  async (data, { rejectWithValue, getState }) => {
    try {
      const res = await axios.patch("/api/profile/local-game", {
        history: getState().localGame.userHistory,
        ...data,
      });
      return res.data;
    } catch (error) {
      return rejectWithValue(error.response.data);
    }
  }
);

const initialState = {
  isActiveSession: false,
  isPlay: true,
  saveSessionEnable: false,
  userSymbol: "X",
  squares: Array(9).fill(null),
  userHistory: {
    userWon: 0,
    draw: 0,
    userLose: 0,
  },
  complexity: "Easy",
  status: null,
  complexityData: [{ text: "Easy" }, { text: "Medium" }, { text: "Hard" }],
  symbolsData: [{ text: "X" }, { text: "O" }],
};

export const localGameSlice = createSlice({
  name: "localGame",
  initialState,
  reducers: {
    changeUserSymbol(state, { payload }) {
      if (state.squares.some(Boolean)) return state;
      state.userSymbol = payload;
    },
    changeSquares(state, { payload }) {
      state.isActiveSession = true;
      state.squares = payload;
    },
    changeComplexity(state, { payload }) {
      if (state.squares.some(Boolean)) return state;
      state.complexity = payload;
    },
    leaveLocalGame() {
      return initialState;
    },
    restartLocalGame(state) {
      return {
        ...initialState,
        userHistory: state.userHistory,
        userSymbol: state.userSymbol,
        complexity: state.complexity,
        isActiveSession: state.isActiveSession,
      };
    },
    updateStatus(state, { payload }) {
      state.status = payload;
      state.isPlay = false;
    },
    simulateRobot(state) {
      const squares = simulateBot(state.squares, state.userSymbol);
      state.squares = squares;
    },
    userWin(state) {
      state.userHistory.userWon += 1;
    },
    robotWin(state) {
      state.userHistory.userLose += 1;
    },
    drawWin(state) {
      state.userHistory.draw += 1;
    },
    toggleSaveSession(state) {
      state.saveSessionEnable = !state.saveSessionEnable;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(fetchLogin.fulfilled, (state) => {
      state.saveSessionEnable = true;
    });
    builder.addCase(getUserFromLS.fulfilled, (state, { payload }) => {
      state.saveSessionEnable = Boolean(payload.token);
    });
    builder.addCase(fetchSaveProgress.fulfilled, (state, { payload }) => {
      return {
        ...initialState,
        userHistory: payload.history,
        saveSessionEnable: state.saveSessionEnable,
      };
    });
  },
});

export const {
  changeComplexity,
  changeSquares,
  changeUserSymbol,
  drawWin,
  restartLocalGame,
  robotWin,
  simulateRobot,
  updateStatus,
  userWin,
  leaveLocalGame,
  toggleSaveSession,
} = localGameSlice.actions;

export default localGameSlice.reducer;
