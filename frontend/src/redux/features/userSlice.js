import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axios from "axios";

export const TIC_TAC_TOE_USER_LS_KEY = "TIC_TAC_TOE_USER_LS_KEY";

export const fetchRegistration = createAsyncThunk(
  "user/registration",
  async (data, { rejectWithValue }) => {
    try {
      const res = await axios.post("/api/auth/register", data);
      return res.data;
    } catch (error) {
      return rejectWithValue(error.response.data);
    }
  }
);
export const fetchLogin = createAsyncThunk(
  "user/login",
  async (data, { rejectWithValue }) => {
    try {
      const res = await axios.post("/api/auth/login", data);
      return res.data;
    } catch (error) {
      return rejectWithValue(error.response.data);
    }
  }
);
export const getUserFromLS = createAsyncThunk(
  "user/creds-from-ls",
  async (_, { rejectWithValue }) => {
    try {
      const creds = localStorage.getItem(TIC_TAC_TOE_USER_LS_KEY);
      if (creds === "null" || !creds) {
        return {
          token: undefined,
          uid: undefined,
        };
      }

      return await JSON.parse(creds);
    } catch (error) {
      return rejectWithValue(error);
    }
  }
);
export const userLogout = createAsyncThunk("user/logout", async () => {
  localStorage.setItem(TIC_TAC_TOE_USER_LS_KEY, null);
});

const initialState = {
  isAuth: false,
  token: undefined,
  uid: undefined,
  email: undefined,
  isErr: false,
  isOk: false,
  errMessage: undefined,
};

export const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
    setErrModal(state) {
      state.isErr = !state.isErr;
    },
    setOkModal(state) {
      state.isOk = !state.isOk;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(fetchRegistration.fulfilled, (state, { payload }) => {
      state.password = payload.password;
      state.email = payload.email;
      state.isErr = false;
      state.errMessage = undefined;
      state.isOk = true;
    });
    builder.addCase(fetchRegistration.rejected, (state, { payload }) => {
      state.isErr = true;
      state.errMessage = payload.message;
      state.isOk = false;
    });
    builder.addCase(fetchLogin.fulfilled, (state, { payload }) => {
      state.token = payload.jwt_token;
      state.uid = payload.uid;
      state.isAuth = true;
      state.errMessage = undefined;

      axios.defaults.headers.common["Authorization"] = payload.jwt_token;
      localStorage.setItem(
        TIC_TAC_TOE_USER_LS_KEY,
        JSON.stringify({
          token: payload.jwt_token,
          uid: payload.uid,
        })
      );
    });
    builder.addCase(fetchLogin.rejected, (state, { payload }) => {
      state.isErr = true;
      state.errMessage = payload.message;
    });
    builder.addCase(getUserFromLS.fulfilled, (state, { payload }) => {
      state.token = payload.token;
      state.uid = payload.uid;
      state.isAuth = Boolean(payload.token);

      axios.defaults.headers.common["Authorization"] = payload.token;
    });
    builder.addCase(userLogout.fulfilled, () => {
      return { ...initialState };
    });
  },
});

export const { setErrModal, setOkModal } = userSlice.actions;

export default userSlice.reducer;
