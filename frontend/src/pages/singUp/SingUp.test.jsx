import { test, expect, vi, afterEach } from "vitest";
import { act, render, screen, userEvent } from "../../test/test-utils";
import { userCredentialsOne } from "../../test/data/constants";
import { baseURL, rest, server } from "../../test/server";
import * as userSlice from "../../redux/features/userSlice";
import SingUp from "./SingUp";

const spyFetchRegistration = vi.spyOn(userSlice, "fetchRegistration");

afterEach(() => {
  spyFetchRegistration.mockClear();
});

const mockEmail = userCredentialsOne.email;
const mockPassword = userCredentialsOne.password;
const mockName = "mock_name";

function renderComponent(preloadedState = {}) {
  const result = render(<SingUp />, { preloadedState });

  const emailInput = screen.getByTestId(/input_email/i);
  const passwordInput = screen.getByTestId(/input_password/i);
  const nameInput = screen.getByTestId(/input_name/i);
  const checkboxInput = screen.getByTestId(/input_checkbox/i);
  const submitButton = screen.getByRole("button", { name: /sign up/i });

  return {
    result,
    emailInput,
    passwordInput,
    nameInput,
    checkboxInput,
    submitButton,
  };
}

test("user can fiel inputs, see input errors and submit form", async () => {
  const { emailInput, passwordInput, nameInput, submitButton, checkboxInput } =
    renderComponent();

  await act(async () => {
    await userEvent.type(nameInput, "a");
  });
  await act(async () => {
    await userEvent.type(emailInput, "t");
  });
  await act(async () => {
    await userEvent.type(passwordInput, "2");
  });

  expect(nameInput).toHaveClass("form_input-err");
  expect(emailInput).toHaveClass("form_input-err");
  expect(passwordInput).toHaveClass("form_input-err");
  expect(screen.getByTestId(emailInput.id)).toHaveClass("form_input-err");
  expect(screen.getByTestId(passwordInput.id)).toHaveClass("form_input-err");
  expect(screen.getByTestId(nameInput.id)).toHaveClass("form_input-err");
  expect(submitButton).toBeDisabled();

  await act(async () => {
    await userEvent.clear(nameInput);
    await userEvent.type(nameInput, mockName);
  });
  await act(async () => {
    await userEvent.clear(emailInput);
    await userEvent.type(emailInput, mockEmail);
  });
  await act(async () => {
    await userEvent.clear(passwordInput);
    await userEvent.type(passwordInput, mockPassword);
  });
  await act(async () => {
    await userEvent.click(checkboxInput);
  });

  expect(nameInput).not.toHaveClass("form_input-err");
  expect(emailInput).not.toHaveClass("form_input-err");
  expect(passwordInput).not.toHaveClass("form_input-err");
  expect(screen.getByTestId(emailInput.id)).not.toHaveClass("form_input-err");
  expect(screen.getByTestId(passwordInput.id)).not.toHaveClass(
    "form_input-err"
  );
  expect(screen.getByTestId(nameInput.id)).not.toHaveClass("form_input-err");
  expect(checkboxInput).toBeChecked();

  await act(async () => {
    await userEvent.click(submitButton);
  });

  expect(spyFetchRegistration).toHaveBeenCalledWith({
    email: mockEmail,
    password: mockPassword,
    name: mockName,
  });
  expect(screen.getByRole("alert")).toBeInTheDocument();
  expect(screen.getByTestId(/user_registered/i).textContent).toBe(
    "We successfully received your data."
  );
  expect(screen.getByAltText(/approve icon/i)).toBeInTheDocument();
});

test("show error modal", async () => {
  const mockError = "mock_error";
  server.use(
    rest.post(`${baseURL}api/auth/register`, (req, res, ctx) => {
      return res(ctx.status(400), ctx.json({ message: mockError }));
    })
  );

  const { nameInput, emailInput, passwordInput, checkboxInput, submitButton } =
    renderComponent();

  await act(async () => {
    await userEvent.type(nameInput, mockName);
  });
  await act(async () => {
    await userEvent.type(emailInput, mockEmail);
  });
  await act(async () => {
    await userEvent.type(passwordInput, mockPassword);
  });
  await act(async () => {
    await userEvent.click(checkboxInput);
  });
  await act(async () => {
    await userEvent.click(submitButton);
  });

  expect(screen.getByRole("alert")).toBeInTheDocument();
  expect(screen.getByTestId(/error_message/i).textContent).toBe(mockError);
  expect(screen.getByAltText(/error icon/i)).toBeInTheDocument();
});
