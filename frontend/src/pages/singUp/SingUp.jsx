import { useEffect, useReducer } from "react";
import "./SingUp.scss";

import { Link } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";

import {
  fetchRegistration,
  setErrModal,
  setOkModal,
} from "../../redux/features/userSlice";

import Input from "../../components/UI/input/Input";
import Button from "../../components/UI/button/Button";
import Modal from "../../components/modal/Modal";
import AuthFormError from "../../components/modals/authFormError/AuthFormError";

import okIcon from "../../assets/icons/ok-mess.png";

import { initialState, reduser } from "./helpers";

const SingUp = () => {
  const stateDispatch = useDispatch();
  const [state, dispatch] = useReducer(reduser, initialState);
  const isErr = useSelector((state) => state.user.isErr);
  const errMessage = useSelector((state) => state.user.errMessage);
  const isOk = useSelector((state) => state.user.isOk);

  useEffect(
    () => dispatch({ type: "validate" }),
    [state.name.value, state.email.value, state.password.value]
  );

  const habldeSubmit = (e) => {
    e.preventDefault();

    if (state.isValid) {
      stateDispatch(
        fetchRegistration({
          name: state.name.value,
          email: state.email.value,
          password: state.password.value,
        })
      );
    }
  };

  return (
    <div className="sing-up">
      {isErr && (
        <AuthFormError
          closeClick={() => stateDispatch(setErrModal())}
          errMessage={errMessage}
        />
      )}
      {isOk && (
        <Modal closeClick={() => stateDispatch(setOkModal())}>
          <div className="auth_form__modal">
            <img src={okIcon} alt="approve icon" />
            <h4 data-testid="user_registered">
              We successfully received <br />
              your data.
            </h4>
            <p>
              Please continue to{" "}
              <Link
                to={"/sing-in"}
                state={{
                  email: state.email.value,
                  password: state.password.value,
                }}
              >
                Sing In
              </Link>
            </p>
          </div>
        </Modal>
      )}
      <form onSubmit={habldeSubmit}>
        <Input
          label="Nickname (will shown for other users)"
          placeholder="Enter your nickname"
          type="text"
          value={state.name.value}
          dispatch={dispatch}
          dType="name"
          isErr={state.name.isErr}
          data-testid="input_name"
        />
        <Input
          label="Email Address"
          placeholder="Enter your email"
          type="email"
          value={state.email.value}
          dispatch={dispatch}
          dType="email"
          isErr={state.email.isErr}
          data-testid="input_email"
        />
        <Input
          label="Create a password"
          placeholder="Enter password"
          type="password"
          value={state.password.value}
          dispatch={dispatch}
          dType="password"
          validate={state.password.validate}
          isErr={state.password.isErr}
          data-testid="input_password"
        />
        <div className="sing-up__checkbox">
          <input
            required
            type="checkbox"
            id="checkbox"
            data-testid="input_checkbox"
          />
          <label htmlFor="checkbox">I accept the</label>
          <Link to="/terms&conditions"> Terms & Conditions</Link>
        </div>
        <Button content="Sign Up" disabled={!state.isValid} bType="submit" />
      </form>
      <p className="link">
        Already have an account? <Link to="/sing-in">Sign In</Link>
      </p>
    </div>
  );
};

export default SingUp;
