import { test, expect, vi, beforeEach } from "vitest";
import {
  screen,
  renderWithRoute,
  userEvent,
  act,
} from "../../../test/test-utils";

let botSteps;
const mocks = vi.hoisted(() => {
  return {
    mockSimulateBot: vi.fn().mockImplementation((squares, userSymbol) => {
      const squaresCopy = [...squares];
      const botSymbol = userSymbol === "X" ? "O" : "X";
      const botStep = botSteps.shift();

      if (botStep) squaresCopy[botStep] = botSymbol;

      return squaresCopy;
    }),
  };
});

vi.mock("../../../helpers/localGameCalculation.js", async (importOriginal) => ({
  ...(await importOriginal()),
  simulateBot: mocks.mockSimulateBot,
}));

beforeEach(() => {
  botSteps = [3, 4, 6];
});

function renderComponent() {
  return renderWithRoute("/game/play-with-robot");
}

test("user can select board item", async () => {
  const { container } = renderComponent();

  const boarditems = screen.getAllByLabelText(/board item/i);

  await act(async () => {
    await userEvent.click(boarditems[0]);
  });

  expect(boarditems[0]).toBeDisabled();
  expect(screen.getByAltText(/cross icon/i)).toBeInTheDocument();
  expect(screen.getByAltText(/zero icon/i)).toBeInTheDocument();
  expect(container).toMatchSnapshot();
});

test("user can change complexity", async () => {
  const { container } = renderComponent();
  const mediumButton = screen.getByRole("button", { name: /medium/i });
  const easyButton = screen.getByRole("button", { name: /easy/i });
  const hardButton = screen.getByRole("button", { name: /hard/i });

  await act(async () => {
    await userEvent.click(mediumButton);
  });
  expect(mediumButton).toHaveClass("active");

  await act(async () => {
    await userEvent.click(easyButton);
  });
  expect(easyButton).toHaveClass("active");

  await act(async () => {
    await userEvent.click(hardButton);
  });
  expect(hardButton).toHaveClass("active");
  expect(container).toMatchSnapshot();
});

test("user can change symbol and can't while playing game", async () => {
  const { container } = renderComponent();
  const crossButton = screen.getByRole("button", { name: "X" });
  const zeroButton = screen.getByRole("button", { name: "O" });
  const boarditems = screen.getAllByLabelText(/board item/i);

  await act(async () => {
    await userEvent.click(zeroButton);
  });
  expect(zeroButton).toHaveClass("active");

  await act(async () => {
    await userEvent.click(boarditems[0]);
  });
  expect(boarditems[0]).toBeDisabled();
  expect(screen.getByAltText(/zero icon/i)).toBeInTheDocument();

  await act(async () => {
    await userEvent.click(crossButton);
  });
  expect(zeroButton).toHaveClass("active");
  expect(container).toMatchSnapshot();
});

test("user can win game and restart", async () => {
  const { container } = renderComponent();
  const boarditems = screen.getAllByLabelText(/board item/i);

  await act(async () => {
    await userEvent.click(boarditems[0]);
  });
  expect(boarditems[0]).toBeDisabled();
  expect(boarditems[3]).toBeDisabled();

  await act(async () => {
    await userEvent.click(boarditems[1]);
  });

  expect(boarditems[1]).toBeDisabled();
  expect(boarditems[4]).toBeDisabled();

  await act(async () => {
    await userEvent.click(boarditems[2]);
  });

  expect(boarditems[2]).toBeDisabled();
  expect(screen.getByLabelText(/board line/i)).toBeInTheDocument();
  expect(screen.getByLabelText(/status/i).textContent).toBe("You win!");
  expect(screen.getByLabelText(/user win/i).textContent).toBe("You won: 1");
  expect(container).toMatchSnapshot();

  await act(async () => {
    await userEvent.click(screen.getByRole("button", { name: /play again/i }));
  });

  expect(screen.queryByAltText(/cross icon/i)).not.toBeInTheDocument();
  expect(screen.queryByAltText(/zero icon/i)).not.toBeInTheDocument();
  expect(screen.queryByLabelText(/board line/i)).not.toBeInTheDocument();
  expect(screen.queryByLabelText(/status/i)).not.toBeInTheDocument();
  expect(container).toMatchSnapshot();
});

test("anauthenticated user can't leave local game page after starting game", async () => {
  const { container } = renderComponent();
  const boarditems = screen.getAllByLabelText(/board item/i);

  await act(async () => {
    await userEvent.click(boarditems[0]);
  });
  expect(boarditems[0]).toBeDisabled();
  expect(boarditems[3]).toBeDisabled();

  await act(async () => {
    await userEvent.click(boarditems[1]);
  });

  expect(boarditems[1]).toBeDisabled();
  expect(boarditems[4]).toBeDisabled();

  await act(async () => {
    await userEvent.click(boarditems[2]);
  });
  expect(boarditems[2]).toBeDisabled();
  expect(container).toMatchSnapshot();

  await act(async () => {
    await userEvent.click(screen.getByRole("link", { name: /back to home/i }));
  });

  expect(screen.getByRole("alert")).toBeInTheDocument();
  expect(
    screen.getByRole("heading", { level: 4, name: /save your score/i })
  ).toBeInTheDocument();
  expect(container).toMatchSnapshot();
});
