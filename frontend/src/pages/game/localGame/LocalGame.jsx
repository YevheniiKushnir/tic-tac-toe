import { useCallback, useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";

import "./LocalGame.scss";

import ArrowButton from "../../../components/UI/arrowButton/ArrowButton";
import Button from "../../../components/UI/button/Button";
import Board from "../../../components/board/Board";
import GameLayout from "../components/gameLayout/GameLayout";
import PlayHistory from "../components/playHistory/PlayHistory";
import OptionsList from "../components/optionsList/OptionsList";
import Modal from "../../../components/modal/Modal";

import useLocalGame from "../../../hooks/useLocalGame";
import {
  leaveLocalGame,
  toggleSaveSession,
} from "../../../redux/features/localGame";
import { useDispatch } from "react-redux";

const LocalGame = () => {
  const location = useLocation();
  const navigation = useNavigate();
  const dispatch = useDispatch();
  const [openModal, setOpenModal] = useState(() =>
    Boolean(location?.state?.activeModal)
  );

  const {
    userHistory,
    userSymbol,
    complexity,
    status,
    squares,
    complexityData,
    symbolsData,
    setSymbol,
    setComplexity,
    restart,
    selectSquare,
    line,
  } = useLocalGame();

  const handleUnmountPage = useCallback(async () => {
    await dispatch(leaveLocalGame());
    navigation("/");
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleScheduleSaveSession = useCallback(
    (url) => async () => {
      console.log("hir unmount");
      await dispatch(toggleSaveSession());
      navigation(url);
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  );

  return (
    <div className="local-game">
      {openModal ? (
        <Modal closeClick={() => setOpenModal(false)}>
          <div className="local-game__modal">
            <h4>Save your score</h4>
            <p>You can save your results and participate in the ranking</p>
            <div className="local-game__modal__links">
              <Button
                content="Leave anyway"
                cName="white"
                click={handleUnmountPage}
              />
              <Button
                content="Sign Up"
                style={{ padding: "8px 30px" }}
                click={handleScheduleSaveSession("/sing-up")}
              />
              <Button
                content="Sign In"
                style={{ padding: "8px 30px" }}
                click={handleScheduleSaveSession("/sing-in")}
              />
            </div>
          </div>
        </Modal>
      ) : null}
      <ArrowButton url="/" text="Back to Home" aria-label="Back to home" />
      <GameLayout>
        <>
          <PlayHistory
            draw={userHistory.draw}
            userLose={userHistory.userLose}
            userWon={userHistory.userWon}
          />
          <OptionsList
            name={"Complexity:"}
            data={complexityData}
            activeValue={complexity}
            dispatch={setComplexity}
            dType={"changeComplexity"}
          />
          <OptionsList
            name={"Preferred symbol:"}
            data={symbolsData}
            activeValue={userSymbol}
            dispatch={setSymbol}
            dType="changeUserSymbol"
          />
        </>
        <>
          <Board
            boards={squares}
            click={selectSquare}
            status={status}
            line={line}
            restart={restart}
            isDisabled={false}
          />
        </>
      </GameLayout>
    </div>
  );
};

export default LocalGame;
