import "./OnlineGame.scss";

import GameLayout from "../components/gameLayout/GameLayout";
import ArrowButton from "../../../components/UI/arrowButton/ArrowButton";
import Loader from "../../../components/UI/loader/Loader";
import Board from "../../../components/board/Board";
import PlayHistory from "../components/playHistory/PlayHistory";
import OptionsList from "../components/optionsList/OptionsList";
import UserInfo from "../components/userInfo/UserInfo";
import ReactionIcons, {
  ReactionPreview,
} from "../components/reactionIcons/ReactionIcons";
import Timer from "../components/timer/Timer";

import useOnlineGame from "../../../hooks/useOnlineGame";

const OnlineGame = () => {
  const {
    state,
    uid,
    selectSquare,
    selectPlayAgain,
    selectReaction,
    userFaileGame,
  } = useOnlineGame();

  if (!state) return <Loader content="Connecting..." />;

  const {
    squares,
    line,
    play,
    timer,
    turn,
    playAgain,
    reactionTimer,
    reactions,
  } = state;

  const user = state.users.find((u) => u.uid === uid);
  const enemy = state.users.find((u) => u.uid !== uid);
  const isDisabledBoard = !play || user.symbol !== turn;

  return (
    <div className="online-game">
      <ArrowButton url="/" text="Back to Home" />
      <Timer
        faileGame={userFaileGame}
        isPlay={play}
        step={turn}
        time={timer}
        userSymbol={user.symbol}
      />
      <GameLayout>
        <>
          <UserInfo name={user.name} raiting={user.raiting} isUser />
          <PlayHistory
            draw={user.history.draw}
            userLose={user.history.userLose}
            userWon={user.history.userWon}
            winText="You won"
            drawText="Draw"
            loseText="You lose"
          />
          <OptionsList
            name={"Preferred symbol:"}
            data={state.symbolsData}
            activeValue={user.symbol}
            isOnline={true}
          />
          <ReactionIcons timer={reactionTimer} click={selectReaction} />
        </>
        <>
          <Board
            isDisabled={isDisabledBoard}
            boards={squares}
            click={selectSquare}
            status={user.status}
            line={line}
            restart={selectPlayAgain}
            again={playAgain}
          />
          {reactions.map((r, i) => (
            <ReactionPreview
              key={r.id}
              timer={r}
              startTime={i * reactionTimer * 500}
            />
          ))}
        </>
        <>
          {enemy ? (
            <>
              <UserInfo name={enemy.name} raiting={enemy.raiting} />
              <PlayHistory
                draw={enemy.history.draw}
                userLose={enemy.history.userLose}
                userWon={enemy.history.userWon}
                winText="Rival won"
                drawText="Draw"
                loseText="Rival lose"
              />
            </>
          ) : (
            <Loader content="Searching..." />
          )}
        </>
      </GameLayout>
    </div>
  );
};

export default OnlineGame;
