import { test, expect } from "vitest";
import {
  screen,
  renderWithRoute,
  userEvent,
  act,
  createOnlineGameEnemy,
  autoPlayGame,
  waitFor,
} from "../../../test/test-utils";
import {
  symbolX,
  symbolO,
  uidOne,
  uidTwo,
  tokenOne,
} from "../../../test/data/constants";
import * as usersDB from "../../../test/data/users";
import * as onlineGameDB from "../../../test/data/onlineGame";

const userOne = {
  ...usersDB.getUser(uidOne),
  ...onlineGameDB.getUserForOnlineGame(uidOne),
  isAuth: true,
  token: tokenOne,
};

function renderComponent(user = userOne, options = {}) {
  return renderWithRoute("/game/play-online", {
    preloadedState: { user, ...options },
  });
}

test("user can't play game without enemy", () => {
  renderComponent();
  const boardItems = screen.getAllByLabelText(/board item/i);

  act(() => {
    userEvent.click(boardItems[0]);
  });

  expect(boardItems[0]).not.toBeDisabled();
  expect(screen.getByLabelText(/loading/i)).toBeInTheDocument();
  expect(screen.getByTestId("user_info_name").textContent).toBe(userOne.name);
  expect(screen.getByTestId("user_info_raiting").textContent).toBe(
    String(userOne.raiting)
  );
  expect(screen.getByTestId("user_history_won").textContent).toBe(
    `You won: ${userOne.history.userWon}`
  );
  expect(screen.getByTestId("user_history_draw").textContent).toBe(
    `Draw: ${userOne.history.draw}`
  );
  expect(screen.getByTestId("user_history_lose").textContent).toBe(
    `You lose: ${userOne.history.userLose}`
  );
  expect(screen.getByTestId("game_turn").textContent).toBe("Your turn");
  expect(screen.getAllByLabelText(/reaction icon/i)).toHaveLength(5);
});

test("users can play game", async () => {
  let userEnemy = undefined;
  renderComponent();
  const boardItems = screen.getAllByLabelText(/board item/i);

  act(() => {
    userEnemy = createOnlineGameEnemy(uidTwo);
  });

  await act(async () => {
    await userEvent.click(boardItems[0]);
  });

  expect(boardItems[0]).toBeDisabled();
  expect(screen.getByTestId("game_turn").textContent).toBe("You waite");

  await act(async () => {
    await userEnemy.clientSocket.emit("user-select-square", {
      square: 1,
      roomId: userEnemy.roomId,
    });
  });

  expect(boardItems[1]).toBeDisabled();
  expect(screen.getByTestId("game_turn").textContent).toBe("Your turn");
});

test("result of game should be show correct and users can restart game", async () => {
  let userEnemy = undefined;
  renderComponent();
  const boardItems = screen.getAllByLabelText(/board item/i);

  act(() => {
    userEnemy = createOnlineGameEnemy(uidTwo);
  });

  await autoPlayGame(
    (idx) => userEvent.click(boardItems[idx]),
    (idx) => {
      userEnemy.clientSocket.emit("user-select-square", {
        square: idx,
        roomId: userEnemy.roomId,
      });
    },
    [
      [1, 0],
      [2, 3],
      [1, 1],
      [2, 4],
      [1, 2],
    ]
  );

  expect(screen.getByLabelText(/board line/i)).toBeInTheDocument();
  expect(screen.getByLabelText(/status/i).textContent).toBe("You win!");
  expect(screen.getAllByTestId(/user_history_won/i)[0].textContent).toBe(
    "You won: 1"
  );

  await act(async () => {
    await userEvent.click(screen.getByRole("button", { name: /play again/i }));
  });

  act(() => {
    userEnemy.clientSocket.emit("user-select-play-again", {
      uid: uidTwo,
      roomId: userEnemy.roomId,
    });
  });

  expect(screen.queryByLabelText(/board line/i)).not.toBeInTheDocument();
  expect(userEnemy.state.users[1].symbol).toBe(symbolX);
  expect(userEnemy.state.turn).toBe(symbolO);
});

test("user win when enemy leave game", () => {
  let userEnemy = undefined;
  renderComponent();

  act(() => {
    userEnemy = createOnlineGameEnemy(uidTwo);
  });

  act(() => {
    userEnemy.clientSocket.emit("user-leave-room", {
      uid: uidTwo,
      roomId: userEnemy.roomId,
    });
  });

  expect(screen.queryByLabelText(/board line/i)).not.toBeInTheDocument();
  expect(screen.getByLabelText(/status/i).textContent).toBe("You win!");
  expect(screen.getAllByTestId(/user_history_won/i)[0].textContent).toBe(
    "You won: 1"
  );
});

test("user win when enemy faile game", () => {
  let userEnemy = undefined;

  act(() => {
    userEnemy = createOnlineGameEnemy(uidTwo);
  });

  renderComponent();

  act(() => {
    userEnemy.clientSocket.emit("user-faile-game", {
      uid: uidTwo,
      roomId: userEnemy.roomId,
    });
  });

  expect(screen.queryByLabelText(/board line/i)).not.toBeInTheDocument();
  expect(screen.getByLabelText(/status/i).textContent).toBe("You win!");
  expect(screen.getAllByTestId(/user_history_won/i)[0].textContent).toBe(
    "You won: 1"
  );
});

test("users can send reactions", async () => {
  let userEnemy = undefined;
  const mockImg = "mock_img";

  renderComponent();
  const reactionIcons = screen.getAllByLabelText(/reaction icon/i);

  act(() => {
    userEnemy = createOnlineGameEnemy(uidTwo);
  });

  await act(async () => {
    await userEvent.click(reactionIcons[0]);
  });

  expect(screen.getByAltText(/reaction preview/i)).toBeInTheDocument();

  await waitFor(
    () =>
      expect(
        screen.queryByAltText(/reaction preview/i)
      ).not.toBeInTheDocument(),
    { timeout: 4000 }
  );

  act(() => {
    userEnemy.clientSocket.emit("user-select-reaction", {
      img: mockImg,
      roomId: userEnemy.roomId,
    });
  });

  expect(screen.getByAltText(/reaction preview/i)).toBeInTheDocument();
  expect(screen.getByAltText(/reaction preview/i)).toHaveAttribute(
    "src",
    mockImg
  );
});
