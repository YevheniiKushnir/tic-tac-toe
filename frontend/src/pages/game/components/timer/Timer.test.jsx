import { test, expect, vi, afterEach, beforeEach, afterAll } from "vitest";
import { act, render, screen, waitFor } from "../../../../test/test-utils";
import { symbolO, symbolX } from "../../../../test/data/constants";
import Timer from "./Timer";

const mockFaileCb = vi.fn();

beforeEach(() => {
  vi.useFakeTimers();
});
afterEach(() => {
  mockFaileCb.mockReset();
});
afterAll(() => {
  vi.useRealTimers();
});

test("component should render conrretly", async () => {
  const { container } = render(
    <Timer
      faileGame={mockFaileCb}
      isPlay
      step={symbolO}
      time={2}
      userSymbol={symbolO}
    />
  );

  expect(screen.getByTestId(/game_turn/i).textContent).toBe("Your turn");
  expect(screen.getByTestId(/game_timer/i).textContent).toBe("2s");
  expect(container).toMatchSnapshot();

  act(() => {
    vi.advanceTimersByTime(2000);
  });

  await waitFor(
    () => expect(screen.getByTestId(/game_timer/i).textContent).toBe("0s"),
    { timeout: 4000 }
  );

  expect(mockFaileCb).toHaveBeenCalledOnce();
  expect(container).toMatchSnapshot();
});

test("component shouldn't call faileGame callback", async () => {
  const { container } = render(
    <Timer
      faileGame={mockFaileCb}
      isPlay
      step={symbolX}
      time={2}
      userSymbol={symbolO}
    />
  );

  expect(screen.getByTestId(/game_turn/i).textContent).toBe("You waite");
  expect(screen.getByTestId(/game_timer/i).textContent).toBe("2s");
  expect(container).toMatchSnapshot();

  act(() => {
    vi.advanceTimersByTime(2000);
  });

  await waitFor(
    () => expect(screen.getByTestId(/game_timer/i).textContent).toBe("0s"),
    { timeout: 5000 }
  );

  expect(mockFaileCb).not.toHaveBeenCalledOnce();
  expect(container).toMatchSnapshot();
});
