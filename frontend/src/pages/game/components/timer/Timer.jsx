import { useCallback, useEffect, useState } from "react";
import "./Timer.scss";

const Timer = ({ time, step, userSymbol, isPlay, faileGame }) => {
  const [timer, setTimer] = useState(time);

  const intervalFun = useCallback(() => {
    if (isPlay) {
      setTimer((prev) => (prev === 0 ? prev : prev - 1));
    }
  }, [setTimer, isPlay]);

  useEffect(() => {
    setTimer(time);
    const myInterval = setInterval(intervalFun, 1000);

    return () => clearInterval(myInterval);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [step, isPlay]);

  useEffect(() => {
    if (timer === 0 && userSymbol === step) {
      faileGame();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [timer]);

  return (
    <div className="game-timer">
      <div className="game-timer__info">
        <p data-testid="game_turn">
          {userSymbol === step ? "Your turn" : "You waite"}
        </p>
        <span data-testid="game_timer">{timer}s</span>
      </div>
      <div className="game-timer__line">
        <span style={{ width: (time - timer) * 10 + "%" }}></span>
      </div>
    </div>
  );
};

export default Timer;
