import "./GameLayout.scss";

const GameLayout = ({ children }) => {
  return (
    <div className="game-layout">
      <div className="game-layout__leftArea">{children[0]}</div>
      <div className="game-layout__centralArea">{children[1]}</div>
      <div className="game-layout__rightArea">{children[2]}</div>
    </div>
  );
};

export default GameLayout;
