import { test, expect } from "vitest";
import { render, screen } from "../../../../test/test-utils";
import GameLayout from "./GameLayout";

test("component should render correctly", () => {
  const { container } = render(
    <GameLayout>
      <p data-testid="test_one">one</p>
      <p data-testid="test_two">two</p>
      <p data-testid="test_three">three</p>
    </GameLayout>
  );

  expect(screen.getByTestId(/test_one/i)).toBeInTheDocument();
  expect(screen.getByTestId(/test_two/i)).toBeInTheDocument();
  expect(screen.getByTestId(/test_three/i)).toBeInTheDocument();
  expect(container).toMatchSnapshot();
});
