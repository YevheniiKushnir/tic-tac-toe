import "./PlayHistory.scss";

import trophyIcon from "../../../../assets/icons/Trophy.png";
import dIcon from "../../../../assets/icons/Handshake.png";

const PlayHistory = ({
  userWon,
  draw,
  userLose,
  winText = "You won",
  drawText = "Draw",
  loseText = "Robot won",
}) => (
  <div className="play-history">
    <div className="play-history__item" aria-label="user win">
      <img src={trophyIcon} alt="trophy icon" />
      <p data-testid="user_history_won">
        {winText}: {userWon}
      </p>
    </div>
    <div className="play-history__item" aria-label="user draw">
      <img src={dIcon} alt="draw icon" />
      <p data-testid="user_history_draw">
        {drawText}: {draw}
      </p>
    </div>
    <div className="play-history__item" aria-label="user lost">
      <img src={trophyIcon} alt="trophy icon" />
      <p data-testid="user_history_lose">
        {loseText}: {userLose}
      </p>
    </div>
  </div>
);

export default PlayHistory;
