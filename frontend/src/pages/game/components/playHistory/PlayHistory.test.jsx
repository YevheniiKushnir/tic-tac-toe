import { test, expect } from "vitest";
import { render, screen } from "../../../../test/test-utils";
import PlayHistory from "./PlayHistory";

test("component should render correctly", () => {
  const { container } = render(
    <PlayHistory userWon={1} draw={2} userLose={3} />
  );

  expect(screen.getByTestId(/user_history_won/i).textContent).toBe(
    `You won: ${1}`
  );
  expect(screen.getByTestId(/user_history_draw/i).textContent).toBe(
    `Draw: ${2}`
  );
  expect(screen.getByTestId(/user_history_lose/i).textContent).toBe(
    `Robot won: ${3}`
  );
  expect(container).toMatchSnapshot();
});

test("component should render correctly with provided text", () => {
  const { container } = render(
    <PlayHistory
      userWon={1}
      draw={2}
      userLose={3}
      winText="test won"
      drawText="test draw"
      loseText="test lose"
    />
  );

  expect(screen.getByTestId(/user_history_won/i).textContent).toBe(
    `test won: ${1}`
  );
  expect(screen.getByTestId(/user_history_draw/i).textContent).toBe(
    `test draw: ${2}`
  );
  expect(screen.getByTestId(/user_history_lose/i).textContent).toBe(
    `test lose: ${3}`
  );
  expect(container).toMatchSnapshot();
});
