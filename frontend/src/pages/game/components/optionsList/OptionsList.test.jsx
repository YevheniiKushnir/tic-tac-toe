import { test, expect, vi } from "vitest";
import { render, screen, userEvent } from "../../../../test/test-utils";
import OptionsList from "./OptionsList";

const mockActiveValue = "mock_active_value";
const mockActiveValueTwo = "mock_active_value_two";
const mockDispatch = vi.fn();
const mockOptionsName = "mock_name";
const mockOptions = [
  { text: mockActiveValue },
  { text: mockActiveValueTwo },
  { text: "mock_text_three" },
];

test("component should render correctly", () => {
  const { container } = render(
    <OptionsList
      name={mockOptionsName}
      activeValue={mockActiveValue}
      data={mockOptions}
    />
  );

  expect(screen.getByTestId(/options_name/i).textContent).toBe(mockOptionsName);
  mockOptions.forEach(({ text }) => {
    expect(screen.getByRole("button", { name: text })).toBeInTheDocument();
  });
  expect(screen.getByRole("button", { name: mockActiveValue })).toHaveClass(
    "active"
  );
  expect(container).toMatchSnapshot();
});

test("component should call dispatch when isOnline false and can't when true", async () => {
  const { container, rerender } = render(
    <OptionsList
      name={mockOptionsName}
      activeValue={mockActiveValue}
      data={mockOptions}
      dispatch={mockDispatch}
      isOnline
    />
  );

  await userEvent.click(
    screen.getByRole("button", { name: mockActiveValueTwo })
  );

  expect(mockDispatch).not.toHaveBeenCalled();
  expect(container).toMatchSnapshot();

  rerender(
    <OptionsList
      name={mockOptionsName}
      activeValue={mockActiveValue}
      data={mockOptions}
      dispatch={mockDispatch}
    />
  );

  await userEvent.click(
    screen.getByRole("button", { name: mockActiveValueTwo })
  );

  rerender(
    <OptionsList
      name={mockOptionsName}
      activeValue={mockActiveValueTwo}
      data={mockOptions}
      dispatch={mockDispatch}
    />
  );

  expect(mockDispatch).toHaveBeenCalledWith(mockActiveValueTwo);
  expect(screen.getByRole("button", { name: mockActiveValueTwo })).toHaveClass(
    "active"
  );
  expect(container).toMatchSnapshot();
});
