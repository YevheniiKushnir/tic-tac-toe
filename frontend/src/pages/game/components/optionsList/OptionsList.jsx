import { memo } from "react";
import "./OptionsList.scss";

const OptionsList = ({
  data,
  dispatch,
  name,
  activeValue,
  isOnline = false,
}) => {
  const handleClick = (text) => {
    if (isOnline) return;
    dispatch(text);
  };

  return (
    <div className="optionsList">
      <p data-testid="options_name">{name}</p>
      <div className="optionsList__list">
        {data.map((opt) => (
          <button
            key={opt.text}
            aria-label={opt.text}
            onClick={() => handleClick(opt.text)}
            className={`optionsList__list__item ${
              opt.text === activeValue ? "active" : ""
            }`}
          >
            <div className="optionsList__list__item_circle">
              <span></span>
            </div>
            <p>{opt.text}</p>
          </button>
        ))}
      </div>
    </div>
  );
};

export default memo(OptionsList);
