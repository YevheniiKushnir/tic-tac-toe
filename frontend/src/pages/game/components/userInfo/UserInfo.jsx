import "./UserInfo.scss";

import userAchieveIcon from "../../../../assets/icons/userAchieve.png";

const UserInfo = ({ name, topRaiting, raiting, isUser = false }) => {
  return (
    <div className="user-info">
      <div className="user-info__photo"></div>
      <div className="user-info__content">
        <div className="user-info__content__name">
          <h5 title={name} data-testid="user_info_name">
            {name}
          </h5>
          {isUser && <span>(you)</span>}
        </div>
        <div className="user-info__content__achieve">
          <img src={userAchieveIcon} alt="user achieve icon" />
          <p data-testid="user_info_raiting">{raiting}</p>
        </div>
      </div>
    </div>
  );
};

export default UserInfo;
