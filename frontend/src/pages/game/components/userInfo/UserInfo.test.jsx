import { test, expect } from "vitest";
import { render, screen } from "../../../../test/test-utils";
import UserInfo from "./UserInfo";

test("component should render corretly", () => {
  const mockName = "mock_name";
  const mockRaiting = 10;
  const { container, rerender } = render(
    <UserInfo name={mockName} raiting={mockRaiting} isUser />
  );

  expect(screen.getByTestId(/user_info_name/i).textContent).toBe(mockName);
  expect(screen.getByTestId(/user_info_raiting/i).textContent).toBe(
    String(mockRaiting)
  );
  expect(container).toMatchSnapshot();

  rerender(<UserInfo name={mockName} raiting={mockRaiting} />);

  expect(container).toMatchSnapshot();
});
