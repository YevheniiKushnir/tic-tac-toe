import React, { useEffect, useId, useRef, useState } from "react";
import "./ReactionIcons.scss";

import firstReaction from "../../../../assets/emojis/emoji-1.svg";
import secondReaction from "../../../../assets/emojis/emoji-2.svg";
import threeReaction from "../../../../assets/emojis/emoji-3.svg";
import fourReaction from "../../../../assets/emojis/emoji-4.svg";
import fiveReaction from "../../../../assets/emojis/emoji-5.svg";

const reactionIcons = [
  firstReaction,
  secondReaction,
  threeReaction,
  fourReaction,
  fiveReaction,
];

export const ReactionPreview = ({ timer, startTime }) => {
  const imgRef = useRef();

  useEffect(() => {
    if (!imgRef.current) return;
    setTimeout(() => imgRef.current?.classList.add("show"), startTime);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [timer]);

  return (
    <img
      ref={imgRef}
      className="game-reactions__preview"
      src={timer.path}
      alt="reaction preview"
    />
  );
};

const ReactionIcons = ({ click, timer }) => {
  const id = useId();
  const [disable, setDisable] = useState(false);

  const hendlerCkick = (img) => {
    click(img);
    setDisable(true);
    setTimeout(() => setDisable(false), timer * 1000);
  };

  return (
    <div className="game-reactions">
      <p>Send reactions:</p>
      <ul className="game-reactions__list">
        {reactionIcons.map((r, i) => (
          <li key={id + i}>
            <button
              className={disable ? "disable" : ""}
              disabled={disable}
              onClick={() => hendlerCkick(r)}
              aria-label="reaction icon"
            >
              <img src={r} alt="emoji icon" />
            </button>
          </li>
        ))}
      </ul>
    </div>
  );
};

export default ReactionIcons;
