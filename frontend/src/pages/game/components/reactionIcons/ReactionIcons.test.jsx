import { test, expect, vi } from "vitest";
import {
  act,
  render,
  screen,
  userEvent,
  waitFor,
} from "../../../../test/test-utils";
import ReactionIcons, { ReactionPreview } from "./ReactionIcons";

test("component ReactionIcons should render correctly", async () => {
  const mockClick = vi.fn();
  const { container } = render(<ReactionIcons timer={1} click={mockClick} />);
  const reactionIcons = screen.getAllByLabelText(/reaction icon/i);

  await act(async () => {
    await userEvent.click(reactionIcons[0]);
  });

  expect(mockClick).toBeCalledWith(expect.any(String));
  reactionIcons.forEach((reaction) => {
    expect(reaction).toBeDisabled();
  });
  expect(container).toMatchSnapshot();

  await waitFor(() => expect(reactionIcons[0]).not.toBeDisabled(), {
    timeout: 2000,
  });

  reactionIcons.forEach((reaction) => {
    expect(reaction).not.toBeDisabled();
  });
});

test("component ReactionPreview should render correctly", async () => {
  vi.useFakeTimers();
  const mockPath = "mock_path";
  const { container } = render(
    <ReactionPreview startTime={0} timer={{ path: mockPath }} />
  );
  vi.runAllTimers();
  const imageElement = screen.getByAltText(/reaction preview/i);

  expect(imageElement).toBeInTheDocument();
  expect(imageElement).toHaveClass("show");
  expect(imageElement).toHaveAttribute("src", mockPath);
  expect(container).toMatchSnapshot();
});
