import { test, expect, beforeEach } from "vitest";
import {
  waitForLoadingToFinish,
  renderWithRoute,
  screen,
  act,
  userEvent,
  renderWithHook,
  delay,
} from "../../test/test-utils";
import { tokenOne, tokenTwo, uidOne, uidTwo } from "../../test/data/constants";
import * as usersDB from "../../test/data/users";
import useInvitation from "../../hooks/useInvitation";

let userTwo = undefined;
let userOne = undefined;

beforeEach(() => {
  userTwo = usersDB.getUser(uidTwo);
  userOne = usersDB.getUser(uidOne);
});

function renderComponent(
  { route, preloadedState } = { route: "/leader-board", preloadedState: {} }
) {
  return renderWithRoute(route, {
    preloadedState: {
      ...preloadedState,
      user: {
        token: tokenOne,
        uid: uidOne,
        isAuth: true,
      },
    },
  });
}

function createInvitingUser() {
  return renderWithHook(() => useInvitation(), {
    user: {
      token: tokenTwo,
      uid: uidTwo,
      isAuth: true,
    },
  });
}

test("user can send and accept invitation", async () => {
  const invitingUserTwo = createInvitingUser();
  renderComponent();

  await waitForLoadingToFinish();

  expect(screen.getAllByTestId(/leader_board_item/i)).toHaveLength(3);

  await act(async () => {
    await userEvent.click(screen.getByTestId(/leader_invite/i));
  });

  expect(screen.getByRole("alert")).toBeInTheDocument();
  expect(screen.getByTestId(/invitation_sender_title/i).textContent).toBe(
    `We have sent an invitation to ${userTwo.name}`
  );
  expect(screen.getByTestId(/timer_title/i).textContent).toBe(
    `Waiting for a response${invitingUserTwo.result.current.user.existTime}s`
  );
  expect(invitingUserTwo.result.current.user.user.name).toBe(userOne.name);

  act(() => {
    invitingUserTwo.result.current.submitInviting();
  });

  expect(screen.getByTestId(/user_info_name/i).textContent).toBe(userOne.name);
});

test("user can send and reject invitation", async () => {
  const invitingUserTwo = createInvitingUser();
  renderComponent();

  await waitForLoadingToFinish();

  expect(screen.getAllByTestId(/leader_board_item/i)).toHaveLength(3);

  await act(async () => {
    await userEvent.click(screen.getByTestId(/leader_invite/i));
  });

  expect(screen.getByRole("alert")).toBeInTheDocument();
  expect(screen.getByTestId(/invitation_sender_title/i).textContent).toBe(
    `We have sent an invitation to ${userTwo.name}`
  );
  expect(screen.getByTestId(/timer_title/i).textContent).toBe(
    `Waiting for a response${invitingUserTwo.result.current.user.existTime}s`
  );
  expect(invitingUserTwo.result.current.user.user.name).toBe(userOne.name);

  act(() => {
    invitingUserTwo.result.current.cancelInviting();
  });

  expect(screen.queryByRole("alert")).not.toBeInTheDocument();
  expect(screen.getAllByTestId(/leader_board_item/i)).toHaveLength(3);
});

test("auto reject initation after time over", async () => {
  const invitingUserTwo = createInvitingUser();
  renderComponent();

  await waitForLoadingToFinish();

  expect(screen.getAllByTestId(/leader_board_item/i)).toHaveLength(3);

  await act(async () => {
    await userEvent.click(screen.getByTestId(/leader_invite/i));
  });

  expect(screen.getByRole("alert")).toBeInTheDocument();
  expect(screen.getByTestId(/invitation_sender_title/i).textContent).toBe(
    `We have sent an invitation to ${userTwo.name}`
  );
  expect(screen.getByTestId(/timer_title/i).textContent).toBe(
    `Waiting for a response${invitingUserTwo.result.current.user.existTime}s`
  );
  expect(invitingUserTwo.result.current.user.user.name).toBe(userOne.name);

  await act(async () => {
    await delay(invitingUserTwo.result.current.user.existTime * 1000);
  });

  expect(screen.queryByRole("alert")).not.toBeInTheDocument();
  expect(screen.getAllByTestId(/leader_board_item/i)).toHaveLength(3);
});

test("user can find users by nickname", async () => {
  createInvitingUser();
  renderComponent({ route: "/" });

  await act(async () => {
    await userEvent.click(
      screen.getByRole("button", { name: /play with friend/i })
    );
  });

  await waitForLoadingToFinish();

  expect(screen.getByRole("alert")).toBeInTheDocument();
  expect(screen.getAllByTestId(/leader_board_item/i)).toHaveLength(2);
  expect(screen.getByTestId(/invitation_findfriend_title/i).textContent).toBe(
    "Find your friend or choose someone else"
  );

  const nicknameInput = screen.getByTestId(/input_nickname/i);
  const searchButton = screen.getByRole("button", { name: /search/i });

  await act(async () => {
    await userEvent.type(nicknameInput, "rsaddassdf");
  });
  await act(async () => {
    await userEvent.click(searchButton);
  });

  expect(screen.queryAllByTestId(/leader_board_item/i)).toHaveLength(0);
  expect(screen.getByTestId(/no_user_found/i)).toBeInTheDocument();

  await act(async () => {
    await userEvent.clear(nicknameInput);
    await userEvent.type(nicknameInput, userOne.name);
  });
  await act(async () => {
    await userEvent.click(searchButton);
  });

  expect(screen.getAllByTestId(/leader_board_item/i)).toHaveLength(1);
});
