import { test, expect, vi } from "vitest";
import { render, screen, userEvent } from "../../../../test/test-utils";
import Sender from "./Sender";

const mocks = vi.hoisted(() => ({
  user: {
    user: {
      name: "mokc_name",
      topRaiting: 10,
      raiting: 2,
      uid: "mock_uid",
      history: {
        userWon: 10,
        draw: 5,
        userLose: 0,
      },
    },
    existTime: 2,
  },
  cancelInviting: vi.fn(),
}));

vi.mock("../../../../context/invitingContext.jsx", async (importOriginal) => ({
  ...(await importOriginal()),
  useInvitatigContext: () => mocks,
}));

test("component should render correctly", async () => {
  const { container } = render(<Sender />);

  expect(screen.getByTestId(/invitation_sender_title/i).textContent).toBe(
    `We have sent an invitation to ${mocks.user.user.name}`
  );
  expect(screen.getByRole("alert")).toBeInTheDocument();

  await userEvent.click(screen.getByRole("button", { name: /discard/i }));

  expect(mocks.cancelInviting).toHaveBeenCalled();
  expect(container).toMatchSnapshot();
});
