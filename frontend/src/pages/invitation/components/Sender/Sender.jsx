import "./Sender.scss";

import Timer from "../Timer/Timer";
import LeaderItem from "../../../leader/components/leaderItem/LeaderItem";
import Button from "../../../../components/UI/button/Button";
import Modal from "../../../../components/modal/Modal";

import { useInvitatigContext } from "../../../../context/invitingContext";

const Sender = () => {
  const {
    user: { user, existTime },
    cancelInviting,
  } = useInvitatigContext();

  return (
    <Modal closeClick={cancelInviting}>
      <div className="invitation invitation-sender">
        <p
          className="invitation-sender__title"
          data-testid="invitation_sender_title"
        >
          We have sent an invitation to {user.name}
        </p>
        <Timer
          existTime={existTime}
          title="Waiting for a response"
          autoCancle={cancelInviting}
        />
        <LeaderItem isUser user={user} />
        <Button
          type="button"
          cName="dark invitation-sender__btn"
          content="Discard"
          click={cancelInviting}
        />
      </div>
    </Modal>
  );
};

export default Sender;
