import "./Recipient.scss";

import Timer from "../Timer/Timer";
import LeaderItem from "../../../leader/components/leaderItem/LeaderItem";
import Button from "../../../../components/UI/button/Button";
import Modal from "../../../../components/modal/Modal";

import { useInvitatigContext } from "../../../../context/invitingContext";

const Recipient = () => {
  const {
    user: { user, existTime },
    submitInviting,
    cancelInviting,
  } = useInvitatigContext();

  return (
    <Modal closeClick={cancelInviting}>
      <div className="invitation invitation-recipient">
        <p
          className="invitation-recipient__title"
          data-testid="invitation_recipient_title"
        >
          You have been invited to the game by {user.name}
        </p>
        <Timer
          existTime={existTime}
          title="Make the choose"
          autoCancle={cancelInviting}
        />
        <LeaderItem isUser user={user} />
        <div className="invitation-recipient__btns">
          <Button
            type="button"
            cName="white"
            content="Reject"
            click={cancelInviting}
            data-testid="reject"
          />
          <Button
            type="button"
            cName="white"
            content="Accept"
            click={submitInviting}
            data-testid="accept"
          />
        </div>
      </div>
    </Modal>
  );
};

export default Recipient;
