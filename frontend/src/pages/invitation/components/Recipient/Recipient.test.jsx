import { test, expect, vi } from "vitest";
import { render, screen, userEvent } from "../../../../test/test-utils";
import Recipient from "./Recipient";
import { uidOne } from "../../../../test/data/constants";

const mocks = vi.hoisted(() => ({
  user: {
    user: {
      name: "mokc_name",
      topRaiting: 10,
      raiting: 2,
      uid: "mock_uid",
      history: {
        userWon: 10,
        draw: 5,
        userLose: 0,
      },
    },
    existTime: 2,
  },
  cancelInviting: vi.fn(),
  submitInviting: vi.fn(),
}));

vi.mock("../../../../context/invitingContext.jsx", async (importOriginal) => ({
  ...(await importOriginal()),
  useInvitatigContext: () => mocks,
}));

test("component should render correctly", async () => {
  const { container } = render(<Recipient />);

  expect(screen.getByTestId(/invitation_recipient_title/i).textContent).toBe(
    `You have been invited to the game by ${mocks.user.user.name}`
  );
  expect(screen.getByRole("alert")).toBeInTheDocument();

  await userEvent.click(screen.getByRole("button", { name: /reject/i }));
  await userEvent.click(screen.getByRole("button", { name: /accept/i }));

  expect(mocks.cancelInviting).toHaveBeenCalled();
  expect(mocks.submitInviting).toHaveBeenCalled();
  expect(container).toMatchSnapshot();
});
