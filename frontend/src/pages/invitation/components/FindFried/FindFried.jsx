import { useCallback, useEffect, useRef, useState } from "react";
import { useSelector } from "react-redux";

import "./FindFried.scss";

import { useGetLeadersQuery } from "../../../../redux/services/leaderBoard";

import { useInvitatigContext } from "../../../../context/invitingContext";

import LeaderItem from "../../../leader/components/leaderItem/LeaderItem";
import Loader from "../../../../components/UI/loader/Loader";
import Modal from "../../../../components/modal/Modal";

const FindFried = () => {
  const uid = useSelector((state) => state.user.uid);
  const { data, refetch } = useGetLeadersQuery();
  const { openInviting, closeFindFriend } = useInvitatigContext();
  const [searchedQuery, setSearchedQuery] = useState(undefined);
  const inputRef = useRef(null);

  const searchedUsers = searchedQuery
    ? data?.users.filter(({ name }) => name.includes(searchedQuery))
    : data?.users;

  const handleSearch = () => {
    setSearchedQuery(inputRef?.current.value);
  };

  const handleInvite = useCallback((inviteUid) => {
    closeFindFriend();
    openInviting(inviteUid);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    refetch();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <Modal closeClick={closeFindFriend}>
      <div className="invitation invitation-find-friend">
        <h4
          className="invitation-find-friend__title"
          data-testid="invitation_findfriend_title"
        >
          Find your friend or choose someone else
        </h4>
        <div className="invitation-find-friend__search">
          <input
            type="text"
            placeholder="Type nickname"
            name="search"
            ref={inputRef}
            data-testid="input_nickname"
          />
          <button type="button" onClick={handleSearch} aria-label="Search">
            <svg
              width="25"
              height="24"
              viewBox="0 0 25 24"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M11.375 18.75C15.7242 18.75 19.25 15.2242 19.25 10.875C19.25 6.52576 15.7242 3 11.375 3C7.02576 3 3.5 6.52576 3.5 10.875C3.5 15.2242 7.02576 18.75 11.375 18.75Z"
                stroke="white"
                strokeWidth="2"
                strokeLinecap="round"
                strokeLinejoin="round"
              />
              <path
                d="M16.9434 16.4436L21.4996 20.9999"
                stroke="white"
                strokeWidth="2"
                strokeLinecap="round"
                strokeLinejoin="round"
              />
            </svg>
          </button>
        </div>
        <div className="invitation-find-friend__users">
          {data?.users?.length ? (
            searchedUsers.length ? (
              searchedUsers.map((u) => (
                <LeaderItem
                  key={`find-friend-${u.uid}`}
                  user={u}
                  isUser={u.uid === uid}
                  invite={handleInvite}
                />
              ))
            ) : (
              <p data-testid="no_user_found">No user found</p>
            )
          ) : (
            <Loader content="Loading..." />
          )}
        </div>
      </div>
    </Modal>
  );
};

export default FindFried;
