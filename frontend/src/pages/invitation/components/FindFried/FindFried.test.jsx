import { test, expect, vi } from "vitest";
import {
  act,
  render,
  renderWithHook,
  screen,
  userEvent,
  waitForLoadingToFinish,
} from "../../../../test/test-utils";
import {
  uidTwo,
  uidOne,
  tokenOne,
  tokenTwo,
} from "../../../../test/data/constants";
import * as usersDB from "../../../../test/data/users";
import useInvitation from "../../../../hooks/useInvitation";
import FindFried from "./FindFried";

const mocks = vi.hoisted(() => ({
  closeFindFriend: vi.fn(),
  openInviting: vi.fn(),
}));

function createInvitingUser(uid, token) {
  return renderWithHook(() => useInvitation(), {
    user: {
      isAuth: true,
      token,
      uid,
    },
  });
}

vi.mock("../../../../context/invitingContext.jsx", async (importOriginal) => ({
  ...(await importOriginal()),
  useInvitatigContext: () => mocks,
}));

test("component should render correctly", async () => {
  const userOne = usersDB.getUser(uidOne);
  createInvitingUser(uidOne, tokenOne);
  createInvitingUser(uidTwo, tokenTwo);
  render(<FindFried />, {
    preloadedState: { user: { uid: uidOne } },
  });
  const nicknameInput = screen.getByTestId(/input_nickname/i);
  const searchButton = screen.getByRole("button", { name: /search/i });

  await waitForLoadingToFinish();

  expect(screen.getByTestId(/invitation_findfriend_title/i).textContent).toBe(
    "Find your friend or choose someone else"
  );
  expect(screen.getByRole("alert")).toBeInTheDocument();
  expect(screen.queryAllByTestId(/leader_board_item/i)).toHaveLength(2);

  await userEvent.click(
    screen.getByRole("button", { name: /invite to a game/i })
  );

  expect(mocks.openInviting).toHaveBeenCalled();
  expect(mocks.openInviting).toHaveBeenCalledWith(uidTwo);

  await act(async () => {
    await userEvent.type(nicknameInput, userOne.name);
  });
  await act(async () => {
    await userEvent.click(searchButton);
  });

  expect(screen.queryAllByTestId(/leader_board_item/i)).toHaveLength(1);

  await act(async () => {
    await userEvent.type(nicknameInput, "dsfdf");
  });
  await act(async () => {
    await userEvent.click(searchButton);
  });

  expect(screen.queryAllByTestId(/leader_board_item/i)).toHaveLength(0);
  expect(screen.getByTestId(/no_user_found/i)).toBeInTheDocument();

  await act(async () => {
    await userEvent.click(screen.getByRole("button", { name: /close modal/i }));
  });

  expect(mocks.closeFindFriend).toHaveBeenCalled();
});
