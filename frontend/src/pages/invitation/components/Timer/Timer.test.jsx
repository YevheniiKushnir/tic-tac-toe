import { test, expect, vi, beforeEach, afterAll } from "vitest";
import { act, render, screen } from "../../../../test/test-utils";
import Timer from "./Timer";

beforeEach(() => {
  vi.useFakeTimers();
});
afterAll(() => {
  vi.useRealTimers();
});

test("component should render correctly", () => {
  const mockCancleCb = vi.fn();
  const mockTitle = "mock_title";
  const { container } = render(
    <Timer autoCancle={mockCancleCb} title={mockTitle} existTime={2} />
  );

  expect(screen.getByTestId(/timer_title/i).textContent).toMatchInlineSnapshot(`
    "${mockTitle}2s"
  `);
  expect(container).toMatchSnapshot();

  act(() => {
    vi.advanceTimersByTime(2000);
  });

  expect(screen.getByTestId(/timer_title/i).textContent).toMatchInlineSnapshot(`
    "${mockTitle}0s"
  `);
  expect(mockCancleCb).toHaveBeenCalledOnce();
  expect(container).toMatchSnapshot();
});
