import { useCallback, useEffect, useMemo, useState } from "react";
import "./Timer.scss";

const Timer = ({ existTime, autoCancle, title }) => {
  const [time, setTime] = useState(0);

  const rangeValue = useMemo(
    () => Math.floor((time * 100) / existTime),
    [time]
  );

  const intervalFun = useCallback(() => {
    setTime((prev) => (prev === existTime ? prev : prev + 1));
  }, [setTime, existTime]);

  useEffect(() => {
    const myInterval = setInterval(intervalFun, 1000);

    return () => clearInterval(myInterval);
  }, [time]);

  useEffect(() => {
    if (time === existTime) {
      autoCancle();
    }
  }, [time, autoCancle, existTime]);
  return (
    <div className="initation-timer">
      <p className="initation-timer__title" data-testid="timer_title">
        <span>{title}</span>
        <span>{existTime - time}s</span>
      </p>
      <div className="initation-timer__range">
        <span style={{ width: rangeValue + "%" }}></span>
      </div>
    </div>
  );
};

export default Timer;
