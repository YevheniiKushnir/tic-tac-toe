import "./Invitation.scss";

import { useInvitatigContext } from "../../context/invitingContext";

import Sender from "./components/Sender/Sender";
import Recipient from "./components/Recipient/Recipient";
import FindFried from "./components/FindFried/FindFried";

const Invitation = () => {
  const { user, isFindFriend } = useInvitatigContext();

  if (user?.isSender) {
    return <Sender />;
  }

  if (user?.isRecipient) {
    return <Recipient />;
  }

  if (isFindFriend) {
    return <FindFried />;
  }

  return null;
};

export default Invitation;
