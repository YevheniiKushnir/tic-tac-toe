import medalIcon from "../../../../assets/icons/Medal.png";
import trophyIcon from "../../../../assets/icons/Trophy.png";
import handshakeIcon from "../../../../assets/icons/Handshake.png";

import Button from "../../../../components/UI/button/Button";
import "./LeaderItem.scss";

const LeaderItem = ({ user, invite, isUser }) => (
  <div className="leader-board__item" data-testid="leader_board_item">
    <div className="leader-board__item__wraper">
      <div className="leader-board__item-left-side">
        <p
          className="leader-board__item__topRaiting"
          data-testid="leader_topRating"
        >
          {user.topRaiting}
        </p>
        <div className="leader-board__item__photo"></div>
        <div className="leader-board__item__raiting">
          <p data-testid="leader_name">{user.name}</p>
          <div className="leader-board__item__raiting__info">
            <img src={medalIcon} alt="medal icon" />
            <p data-testid="leader_raiting">{user.raiting}</p>
          </div>
        </div>
      </div>
      <div className="leader-board__item-right-side">
        <div className="leader-board__item__history">
          <div className="leader-board__item__history__item">
            <img src={trophyIcon} alt="trophy icon" />
            <p data-testid="leader_won">Won : {user.history.userWon}</p>
          </div>
          <div className="leader-board__item__history__item">
            <img src={handshakeIcon} alt="handshake icon" />
            <p data-testid="leader_draw">Draw : {user.history.draw}</p>
          </div>
          <div className="leader-board__item__history__item">
            <img src={trophyIcon} alt="trophy icon" />
            <p data-testid="leader_lose">Lost : {user.history.userLose}</p>
          </div>
        </div>
      </div>
    </div>
    {isUser ? null : (
      <Button
        content="Invite to a game"
        cName="white leader-board__item__btn"
        click={() => invite(user.uid)}
        data-testid="leader_invite"
      />
    )}
  </div>
);

export default LeaderItem;
