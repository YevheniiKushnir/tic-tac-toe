import { test, expect, vi } from "vitest";
import { render, screen, userEvent } from "../../../../test/test-utils";
import LeaderItem from "./LeaderItem";

test("component should render correctly", async () => {
  const mockInvite = vi.fn();
  const mockUser = {
    topRaiting: 100,
    name: "mock_name",
    raiting: 3,
    history: {
      userWon: 1,
      draw: 2,
      userLose: 3,
    },
    uid: "mock_uid",
  };
  const { container, rerender } = render(
    <LeaderItem user={mockUser} invite={mockInvite} isUser={false} />
  );

  await userEvent.click(screen.getByTestId(/leader_invite/i));

  expect(screen.getByTestId(/leader_topRating/i).textContent).toBe(
    String(mockUser.topRaiting)
  );
  expect(screen.getByTestId(/leader_raiting/i).textContent).toBe(
    String(mockUser.raiting)
  );
  expect(screen.getByTestId(/leader_name/i).textContent).toBe(
    String(mockUser.name)
  );
  expect(screen.getByTestId(/leader_draw/i).textContent).toBe(
    `Draw : ${mockUser.history.draw}`
  );
  expect(screen.getByTestId(/leader_lose/i).textContent).toBe(
    `Lost : ${mockUser.history.userLose}`
  );
  expect(screen.getByTestId(/leader_won/i).textContent).toBe(
    `Won : ${mockUser.history.userWon}`
  );
  expect(screen.getByTestId(/leader_invite/i)).toBeVisible();
  expect(container).toMatchSnapshot();

  rerender(<LeaderItem user={mockUser} invite={mockInvite} isUser />);

  expect(screen.queryByTestId(/leader_invite/i)).not.toBeInTheDocument();
  expect(container).toMatchSnapshot();
});
