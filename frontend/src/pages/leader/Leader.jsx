import { useSelector } from "react-redux";

import { useGetLeadersQuery } from "../../redux/services/leaderBoard";
import { useInvitatigContext } from "../../context/invitingContext";

import LeaderItem from "./components/leaderItem/LeaderItem";
import Loader from "../../components/UI/loader/Loader";
import "./Leader.scss";

const Leader = () => {
  const uid = useSelector((state) => state.user.uid);
  const { data, isLoading } = useGetLeadersQuery(undefined, {
    pollingInterval: 10_000,
  });
  const { openInviting } = useInvitatigContext();

  if (isLoading) return <Loader content="Loading..." />;

  const user = data?.users.find((user) => user.uid === uid);

  return (
    <div className="leader-board">
      {data?.users?.length && user ? (
        <>
          <div className="leader-board__user">
            <h3>Your reiting</h3>
            <LeaderItem user={user} isUser />
          </div>
          <hr />
          <div className="leader-board__users">
            <h4>Leader board</h4>
            <div className="leader-board__users__list">
              {data.users.map((u) => (
                <LeaderItem
                  key={u.uid}
                  user={u}
                  invite={openInviting}
                  isUser={u.uid == uid}
                />
              ))}
            </div>
          </div>
        </>
      ) : (
        <div className="leader-board__emptyList">
          <h3>Empty list</h3>
          <p>This statistics makes from online activites players</p>
        </div>
      )}
    </div>
  );
};

export default Leader;
