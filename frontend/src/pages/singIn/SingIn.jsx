import { useLayoutEffect, useReducer } from "react";
import "./SingIn.scss";

import { Link, useLocation } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";

import { fetchLogin, setErrModal } from "../../redux/features/userSlice";

import okIcon from "../../assets/icons/ok-mess.png";

import Modal from "../../components/modal/Modal";
import Input from "../../components/UI/input/Input";
import Button from "../../components/UI/button/Button";
import AuthFormError from "../../components/modals/authFormError/AuthFormError";

import { fetchSaveProgress } from "../../redux/features/localGame";

import { reduser, initialState } from "./helpers";

const SingIn = () => {
  const location = useLocation();
  const appDispatch = useDispatch();

  const [state, dispatch] = useReducer(reduser, initialState);
  const isAuth = useSelector((state) => state.user.isAuth);
  const isErr = useSelector((state) => state.user.isErr);
  const errMessage = useSelector((state) => state.user.errMessage);
  const saveSessionEnable = useSelector(
    (state) => state.localGame.saveSessionEnable
  );

  useLayoutEffect(
    () => {
      if (saveSessionEnable && isAuth) {
        appDispatch(fetchSaveProgress());
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [saveSessionEnable, isAuth]
  );

  useLayoutEffect(() => {
    if (location?.state?.email && location?.state?.password) {
      dispatch({
        type: "forceValidate",
        email: location.state.email,
        password: location.state.password,
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useLayoutEffect(
    () => dispatch({ type: "validate" }),
    [state.email.value, state.password.value]
  );

  const habldeSubmit = (e) => {
    e.preventDefault();

    if (state.isValid) {
      appDispatch(
        fetchLogin({
          email: state.email.value,
          password: state.password.value,
        })
      );
    }
  };

  return (
    <div>
      {isErr && (
        <AuthFormError
          closeClick={() => appDispatch(setErrModal())}
          errMessage={errMessage}
        />
      )}
      {isAuth && (
        <Modal>
          <div className="auth_form__modal">
            <img src={okIcon} alt="approve icon" />
            <h4 data-testid="user_logined">We successfully logined.</h4>
            <p>
              Please continue to <Link to="/">Game page</Link>
            </p>
          </div>
        </Modal>
      )}
      <form onSubmit={habldeSubmit}>
        <Input
          label="Email Address"
          placeholder="Enter your email"
          type="email"
          value={state.email.value}
          dispatch={dispatch}
          dType="email"
          isErr={state.email.isErr}
          name="email"
          data-testid="input_email"
        />
        <Input
          label="Password"
          placeholder="Enter password"
          type="password"
          value={state.password.value}
          dispatch={dispatch}
          dType="password"
          validate={state.password.validate}
          isErr={state.password.isErr}
          name="password"
          data-testid="input_password"
        />
        <Button content="Sign In" disabled={!state.isValid} bType="submit" />
      </form>
      <p className="link">
        Don't have an account? <Link to="/sing-up">Sign Up now</Link>
      </p>
    </div>
  );
};

export default SingIn;
