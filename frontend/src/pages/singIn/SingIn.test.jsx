import { test, expect, vi, afterEach } from "vitest";
import { act, render, screen, userEvent } from "../../test/test-utils";
import {
  tokenOne,
  uidOne,
  userCredentialsOne,
} from "../../test/data/constants";
import { baseURL, rest, server } from "../../test/server";
import * as userSlice from "../../redux/features/userSlice";
import * as localGame from "../../redux/features/localGame";
import SingIn from "./SingIn";

const mocks = vi.hoisted(() => ({
  mockUseLocation: vi.fn(),
}));

vi.mock("react-router-dom", async (importOriginal) => ({
  ...(await importOriginal()),
  useLocation: mocks.mockUseLocation,
}));

const spyFetchLogin = vi.spyOn(userSlice, "fetchLogin");
const spyFetchSaveProgress = vi.spyOn(localGame, "fetchSaveProgress");
const spyLS = vi.spyOn(Storage.prototype, "setItem");

afterEach(() => {
  spyFetchLogin.mockClear();
  spyFetchSaveProgress.mockClear();
  spyLS.mockClear();
  mocks.mockUseLocation.mockRestore();
});

const mockEmail = userCredentialsOne.email;
const mockPassword = userCredentialsOne.password;

function renderComponent(preloadedState = {}) {
  const result = render(<SingIn />, { preloadedState });

  const emailInput = screen.getByTestId(/input_email/i);
  const passwordInput = screen.getByTestId(/input_password/i);
  const submitButton = screen.getByRole("button", { name: /sign in/i });

  return {
    result,
    emailInput,
    passwordInput,
    submitButton,
  };
}

test("user can fiel inputs, see input errors and submit form", async () => {
  const { emailInput, passwordInput, submitButton } = renderComponent();

  await act(async () => {
    await userEvent.type(emailInput, "t");
  });
  await act(async () => {
    await userEvent.type(passwordInput, "2");
  });

  expect(emailInput).toHaveClass("form_input-err");
  expect(passwordInput).toHaveClass("form_input-err");
  expect(screen.getByTestId(emailInput.id)).toHaveClass("form_input-err");
  expect(screen.getByTestId(passwordInput.id)).toHaveClass("form_input-err");
  expect(submitButton).toBeDisabled();

  await act(async () => {
    await userEvent.clear(emailInput);
    await userEvent.type(emailInput, mockEmail);
  });
  await act(async () => {
    await userEvent.clear(passwordInput);
    await userEvent.type(passwordInput, mockPassword);
  });
  await act(async () => {
    await userEvent.click(submitButton);
  });

  expect(emailInput).not.toHaveClass("form_input-err");
  expect(passwordInput).not.toHaveClass("form_input-err");
  expect(screen.getByTestId(emailInput.id)).not.toHaveClass("form_input-err");
  expect(screen.getByTestId(passwordInput.id)).not.toHaveClass(
    "form_input-err"
  );
  expect(spyFetchLogin).toHaveBeenCalledWith({
    email: mockEmail,
    password: mockPassword,
  });
  expect(spyLS).toHaveBeenCalled();
  expect(spyLS).toHaveBeenCalledWith(
    userSlice.TIC_TAC_TOE_USER_LS_KEY,
    JSON.stringify({
      token: tokenOne,
      uid: uidOne,
    })
  );
  expect(screen.getByRole("alert")).toBeInTheDocument();
  expect(screen.getByTestId(/user_logined/i).textContent).toBe(
    "We successfully logined."
  );
  expect(screen.getByAltText(/approve icon/i)).toBeInTheDocument();
});

test("save local game progress after user login", async () => {
  const { emailInput, passwordInput, submitButton } = renderComponent({
    localGame: { saveSessionEnable: true },
  });

  await act(async () => {
    await userEvent.type(emailInput, mockEmail);
  });
  await act(async () => {
    await userEvent.type(passwordInput, mockPassword);
  });
  await act(async () => {
    await userEvent.click(submitButton);
  });

  expect(spyFetchLogin).toHaveBeenCalledWith({
    email: mockEmail,
    password: mockPassword,
  });
  expect(spyFetchSaveProgress).toHaveBeenCalled();
});

test("auto fiel inputs", () => {
  mocks.mockUseLocation.mockReturnValue({
    state: {
      email: userCredentialsOne.email,
      password: userCredentialsOne.password,
    },
  });

  const { emailInput, passwordInput } = renderComponent();

  expect(emailInput).toHaveAttribute("value", userCredentialsOne.email);
  expect(passwordInput).toHaveAttribute("value", userCredentialsOne.password);
});

test("show error modal", async () => {
  const mockError = "mock_error";
  server.use(
    rest.post(`${baseURL}api/auth/login`, (req, res, ctx) => {
      return res(ctx.status(400), ctx.json({ message: mockError }));
    })
  );

  const { emailInput, passwordInput, submitButton } = renderComponent();

  await act(async () => {
    await userEvent.type(emailInput, mockEmail);
  });
  await act(async () => {
    await userEvent.type(passwordInput, mockPassword);
  });
  await act(async () => {
    await userEvent.click(submitButton);
  });

  expect(screen.getByRole("alert")).toBeInTheDocument();
  expect(screen.getByTestId(/error_message/i).textContent).toBe(mockError);
  expect(screen.getByAltText(/error icon/i)).toBeInTheDocument();
});
