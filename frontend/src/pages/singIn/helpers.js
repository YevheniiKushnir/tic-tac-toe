import validateFormData from "../../helpers/validateFromData";

export const initialState = {
  email: {
    value: "",
    required: true,
    isErr: false,
    pattern: /^[\w-\\.]+@([\w-]+\.)+[\w-]{2,4}$/g,
    touched: false,
  },
  password: {
    value: "",
    required: true,
    isErr: false,
    validate: [
      {
        value: "One lowercase character",
        pattern: /[a-z]{1,}/g,
        isErr: false,
        isOk: false,
      },
      {
        value: "One uppercase character",
        pattern: /[A-Z]{1,}/g,
        isErr: false,
        isOk: false,
      },
      {
        value: "At least 1 symbol",
        pattern: /[A-Z]{1,}/g,
        isErr: false,
        isOk: false,
      },
      {
        value: "8 characters minimum & maximum",
        pattern: /^.{8,8}$/g,
        isErr: false,
        isOk: false,
      },
    ],
    touched: false,
  },
  isValid: false,
};

export function reduser(state, action) {
  switch (action.type) {
    case "email":
      return {
        ...state,
        email: { ...state.email, value: action.email, touched: true },
      };

    case "password":
      return {
        ...state,
        password: { ...state.password, value: action.password, touched: true },
      };

    case "validate":
      return validateFormData(state);

    case "forceValidate":
      const newState = {
        ...state,
        email: { ...state.email, value: action.email, touched: true },
        password: { ...state.password, value: action.password, touched: true },
      };
      return validateFormData(newState);

    default:
      return state;
  }
}
