import { expect, test } from "vitest";
import { renderWithRoute } from "../../test/test-utils";

test("Info page should render correctly", () => {
  const { container } = renderWithRoute("/terms&conditions");

  expect(window.scrollTo).toHaveBeenCalled();
  expect(container).toMatchSnapshot();
});
