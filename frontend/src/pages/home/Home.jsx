import { useState } from "react";
import { useLocation } from "react-router-dom";
import { useSelector } from "react-redux";

import "./Home.scss";

import Board from "../../components/board/Board";
import Button from "../../components/UI/button/Button";
import AuthModal from "../../components/modals/authModal/AuthModal";

import { useInvitatigContext } from "../../context/invitingContext";

const buttonStyles = { padding: "8px 12px" };

const Home = () => {
  const location = useLocation();
  const [showAuthModal, setShowAuthModal] = useState(
    Boolean(location?.state?.isUnauthenticated)
  );
  const isAuth = useSelector((state) => state.user.isAuth);
  const { openFindFriend } = useInvitatigContext();

  const handleShowInvitation = () => {
    if (isAuth) {
      openFindFriend();
    } else {
      setShowAuthModal((prev) => !prev);
    }
  };

  return (
    <div className="home-welcome">
      {showAuthModal ? (
        <AuthModal closeModal={() => setShowAuthModal(!showAuthModal)} />
      ) : null}
      <h1 className="home-welcome__title">Welcome to Tic Tac Toe</h1>
      <div className="home-welcome__board">
        <Board />
        <div className="home-welcome__playBtns">
          <Button
            type="link"
            url="/game/play-with-robot"
            content="Play with robot"
            cName="white"
            style={buttonStyles}
          />
          <Button
            type="link"
            url="/game/play-online"
            content="Play with random player"
            cName="white"
            style={buttonStyles}
          />
          <Button
            type="button"
            click={handleShowInvitation}
            content="Play with friend"
            cName="white"
            style={buttonStyles}
          />
        </div>
      </div>
    </div>
  );
};

export default Home;
