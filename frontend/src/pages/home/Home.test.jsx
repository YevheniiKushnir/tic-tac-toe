import { test, expect, vi, afterEach } from "vitest";
import { screen, renderWithRoute } from "../../test/test-utils";

const mocks = vi.hoisted(() => ({
  useLocation: vi.fn().mockImplementation(() => ({
    pathname: "/",
    state: { isUnauthenticated: false },
  })),
}));

vi.mock("react-router-dom", async (importOriginal) => ({
  ...(await importOriginal()),
  useLocation: mocks.useLocation,
}));

afterEach(() => {
  mocks.useLocation.mockReset();
});

test("render home component should work", () => {
  const { container } = renderWithRoute("/");

  expect(container).toMatchSnapshot();
});

test("render home component with authenticated modal", () => {
  mocks.useLocation.mockImplementation(() => ({
    pathname: "/",
    state: { isUnauthenticated: true },
  }));
  const { container } = renderWithRoute("/");

  expect(screen.getByRole("alert")).toBeInTheDocument();
  expect(
    screen.getByTestId("auth_modal_heading").textContent
  ).toMatchInlineSnapshot(
    `"For this featur you must be authorizated, please Sign In or Sign Up"`
  );
  expect(container).toMatchSnapshot();
});
