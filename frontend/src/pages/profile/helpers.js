import validateFormData from "../../helpers/validateFromData";

export function getPasswodDate(date) {
  return `Last updated ${
    new Date().getMonth() - new Date(date).getMonth()
  }month ago`;
}

export const initialState = {
  name: {
    value: "",
    required: true,
    isErr: false,
    pattern: /[A-z0-9]{3,}/g,
    touched: false,
  },
  email: {
    value: "",
    required: true,
    isErr: false,
    pattern: /^[\w-\\.]+@([\w-]+\.)+[\w-]{2,4}$/g,
    touched: false,
  },
  password: {
    value: "",
    required: true,
    isErr: false,
    validate: [
      {
        value: "One lowercase character",
        pattern: /[a-z]{1,}/g,
        isErr: false,
        isOk: false,
      },
      {
        value: "One uppercase character",
        pattern: /[A-Z]{1,}/g,
        isErr: false,
        isOk: false,
      },
      {
        value: "At least 1 symbol",
        pattern: /[A-Z]{1,}/g,
        isErr: false,
        isOk: false,
      },
      {
        value: "8 characters minimum & maximum",
        pattern: /^.{8,8}$/g,
        isErr: false,
        isOk: false,
      },
    ],
    touched: false,
  },
  oldPassword: {
    value: "",
    required: true,
    isErr: false,
    pattern: /^.{8,8}$/g,
    touched: false,
  },
  isValid: false,
};
export function reducer(state, action) {
  switch (action.type) {
    case "name":
      return {
        ...state,
        name: { ...state.name, value: action.name, touched: true },
      };

    case "email":
      return {
        ...state,
        email: { ...state.email, value: action.email, touched: true },
      };

    case "password":
      return {
        ...state,
        password: { ...state.password, value: action.password, touched: true },
      };

    case "oldPassword":
      return {
        ...state,
        oldPassword: {
          ...state.oldPassword,
          value: action.oldPassword,
          touched: true,
        },
      };

    case "validate":
      return validateFormData(state, 0);

    default:
      return state;
  }
}
