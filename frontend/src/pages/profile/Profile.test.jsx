import { test, expect, beforeEach, vi } from "vitest";
import {
  act,
  render,
  screen,
  userEvent,
  waitForLoadingToFinish,
} from "../../test/test-utils";
import { tokenOne, uidOne } from "../../test/data/constants";
import * as onlineGameDB from "../../test/data/onlineGame";
import * as usersDB from "../../test/data/users";
import * as userSlice from "../../redux/features/userSlice";
import * as localGame from "../../redux/features/localGame";
import { getPasswodDate } from "./helpers";
import Profile from "./Profile";

let user = undefined;
const mockUseNavigate = vi.fn();
const spyUserLogout = vi.spyOn(userSlice, "userLogout");
const spyLeaveLocalGame = vi.spyOn(localGame, "leaveLocalGame");

beforeEach(() => {
  user = {
    ...onlineGameDB.getUserForOnlineGame(uidOne),
    ...usersDB.getUser(uidOne),
  };
});

vi.mock("react-router-dom", async (importOriginal) => ({
  ...(await importOriginal()),
  useNavigate: () => mockUseNavigate,
}));

async function renderComponent(preloadedState = {}) {
  const rendered = render(<Profile />, {
    preloadedState: {
      user: {
        token: tokenOne,
      },
      ...preloadedState,
    },
  });

  await waitForLoadingToFinish();

  return rendered;
}

test("user raiting should render correctly", async () => {
  await renderComponent();

  expect(screen.getByTestId(/profile_topRaiting/i).textContent).toBe(
    String(user.topRaiting)
  );
  expect(screen.getByTestId(/profile_raiting/i).textContent).toBe(
    String(user.raiting)
  );
  expect(screen.getByTestId(/profile_won/i).textContent).toBe(
    `Won: ${user.history.userWon}`
  );
  expect(screen.getByTestId(/profile_draw/i).textContent).toBe(
    `Draw: ${user.history.draw}`
  );
  expect(screen.getByTestId(/profile_lose/i).textContent).toBe(
    `Lost: ${user.history.userLose}`
  );
});

test("user can change name", async () => {
  await renderComponent();
  const mockName = "mock_name";
  const editButton = screen.getAllByRole("button", { name: /edit/i })[1];
  const nameUser = screen.getAllByTestId(/profile_item_data/i)[0];

  expect(nameUser.textContent).toBe(user.name);

  await act(async () => {
    await userEvent.click(editButton);
  });

  const nameInput = screen.getByTestId(/input_name/i);
  const cancelButton = screen.getByRole("button", { name: /cancel/i });
  expect(cancelButton).toBeInTheDocument();
  expect(nameInput).toBeInTheDocument();

  await act(async () => {
    await userEvent.type(nameInput, "r");
  });

  expect(nameInput).toHaveClass("form_input-err");

  await act(async () => {
    await userEvent.clear(nameInput);
    await userEvent.type(nameInput, mockName);
  });

  expect(nameInput).not.toHaveClass("form_input-err");

  const saveButton = screen.getByRole("button", { name: /save/i });
  await act(async () => {
    await userEvent.click(saveButton);
  });

  await act(async () => {
    await userEvent.click(cancelButton);
  });

  expect(usersDB.getUser(uidOne).name).toBe(mockName);
});

test("user can change email", async () => {
  await renderComponent();
  const mockEmail = "TestTest@test.com";
  const editButton = screen.getAllByRole("button", { name: /edit/i })[2];
  const emailUser = screen.getAllByTestId(/profile_item_data/i)[1];

  expect(emailUser.textContent).toBe(user.email);

  await act(async () => {
    await userEvent.click(editButton);
  });

  const emailInput = screen.getByTestId(/input_email/i);
  const cancelButton = screen.getByRole("button", { name: /cancel/i });
  expect(cancelButton).toBeInTheDocument();
  expect(emailInput).toBeInTheDocument();

  await act(async () => {
    await userEvent.type(emailInput, "r");
  });

  expect(emailInput).toHaveClass("form_input-err");

  await act(async () => {
    await userEvent.clear(emailInput);
    await userEvent.type(emailInput, mockEmail);
  });

  expect(emailInput).not.toHaveClass("form_input-err");

  const saveButton = screen.getByRole("button", { name: /save/i });
  await act(async () => {
    await userEvent.click(saveButton);
  });

  await act(async () => {
    await userEvent.click(cancelButton);
  });

  expect(usersDB.getUser(uidOne).email).toBe(mockEmail);
});

test("user can change password", async () => {
  await renderComponent();
  const mockPassword = "Dddddddd";
  const editButton = screen.getAllByRole("button", { name: /edit/i })[3];
  const passwordUser = screen.getAllByTestId(/profile_item_data/i)[2];

  expect(passwordUser.textContent).toBe(getPasswodDate(user.date));

  await act(async () => {
    await userEvent.click(editButton);
  });

  const passwordOldInput = screen.getByTestId(/input_password_old/i);
  const passwordNewInput = screen.getByTestId(/input_password_new/i);
  const cancelButton = screen.getByRole("button", { name: /cancel/i });
  expect(cancelButton).toBeInTheDocument();
  expect(passwordOldInput).toBeInTheDocument();
  expect(passwordNewInput).toBeInTheDocument();

  await act(async () => {
    await userEvent.type(passwordOldInput, "r");
  });
  await act(async () => {
    await userEvent.type(passwordNewInput, "r");
  });

  expect(passwordOldInput).toHaveClass("form_input-err");
  expect(passwordNewInput).toHaveClass("form_input-err");

  await act(async () => {
    await userEvent.clear(passwordOldInput);
    await userEvent.type(passwordOldInput, user.password);
  });
  await act(async () => {
    await userEvent.clear(passwordNewInput);
    await userEvent.type(passwordNewInput, mockPassword);
  });

  expect(passwordOldInput).not.toHaveClass("form_input-err");
  expect(passwordNewInput).not.toHaveClass("form_input-err");

  const saveButton = screen.getByRole("button", { name: /save/i });
  await act(async () => {
    await userEvent.click(saveButton);
  });

  await act(async () => {
    await userEvent.click(cancelButton);
  });

  expect(usersDB.getUser(uidOne).password).toBe(mockPassword);
});

test("user can logout", async () => {
  await renderComponent();

  await act(async () => {
    await userEvent.click(screen.getByRole("button", { name: /logout/i }));
  });

  expect(mockUseNavigate).toHaveBeenCalledWith("/");
  expect(spyLeaveLocalGame).toHaveBeenCalledOnce();
  expect(spyUserLogout).toHaveBeenCalledOnce();
});
