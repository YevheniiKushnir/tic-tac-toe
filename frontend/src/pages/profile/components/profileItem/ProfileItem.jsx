import { useState } from "react";
import Button from "../../../../components/UI/button/Button";

function ProfileItem({ children, title, data, photo, disable, click }) {
  const [isEdit, setIsEdit] = useState(false);

  const handleSetEdit = () => setIsEdit(!isEdit);

  return (
    <div className="user-profile__content__item">
      <div className="user-profile__content__item__head">
        <p data-testid="profile_item_title">{title}</p>
        {isEdit ? (
          <button className="btn-cancel" onClick={handleSetEdit} type="button">
            Cancel
          </button>
        ) : (
          <button className="btn-edit" onClick={handleSetEdit} type="button">
            Edit
          </button>
        )}
      </div>
      <div className="user-profile__content__item__body">
        {isEdit ? (
          children
        ) : photo ? (
          <img
            src={photo}
            alt="profile icon"
            data-testid="profile_item_photo"
          />
        ) : (
          <p data-testid="profile_item_data">{data}</p>
        )}
      </div>
      {isEdit && (
        <>
          <hr />
          <div className="user-profile__content__item__footer">
            <Button
              disabled={disable}
              click={click}
              content="Save"
              bType="button"
              data-testid="profile_item_save"
            />
          </div>
        </>
      )}
    </div>
  );
}

export default ProfileItem;
