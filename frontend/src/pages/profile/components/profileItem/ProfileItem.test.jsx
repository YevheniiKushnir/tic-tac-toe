import { test, expect, vi } from "vitest";
import { act, render, screen, userEvent } from "../../../../test/test-utils";
import ProfileItem from "./ProfileItem";

test("component should render correctly", async () => {
  const mockClick = vi.fn();
  const mockTitle = "mock_title";
  const mockPhoto = "mock_photo";
  const mockData = "mock_data";

  const { container } = render(
    <ProfileItem
      click={mockClick}
      data={mockData}
      photo={mockPhoto}
      title={mockTitle}
      disable={false}
    >
      <div data-testid="test_element">div</div>
    </ProfileItem>
  );

  expect(screen.getByTestId(/profile_item_title/i).textContent).toBe(mockTitle);
  expect(screen.getByTestId(/profile_item_photo/i)).toBeInTheDocument();
  expect(screen.getByTestId(/profile_item_photo/i)).toHaveAttribute(
    "src",
    mockPhoto
  );
  expect(screen.queryByTestId(/test_element/i)).not.toBeInTheDocument();
  expect(screen.getByRole("button", { name: /edit/i })).toBeInTheDocument();
  expect(container).toMatchSnapshot();

  await act(async () => {
    await userEvent.click(screen.getByRole("button", { name: /edit/i }));
  });

  expect(
    screen.queryByRole("button", { name: /edit/i })
  ).not.toBeInTheDocument();
  expect(screen.getByRole("button", { name: /cancel/i })).toBeInTheDocument();
  expect(screen.queryByTestId(/test_element/i)).toBeInTheDocument();
  expect(screen.queryByTestId(/profile_item_photo/i)).not.toBeInTheDocument();
  expect(container).toMatchSnapshot();

  await act(async () => {
    await userEvent.click(screen.getByTestId(/profile_item_save/i));
  });

  expect(mockClick).toHaveBeenCalled();
});
