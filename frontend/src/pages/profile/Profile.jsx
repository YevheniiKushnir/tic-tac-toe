import { useEffect, useReducer } from "react";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";

import {
  useGetUserProfileQuery,
  useUpdateEmailMutation,
  useUpdateNicknameMutation,
  useUpdatePasswordMutation,
} from "../../redux/services/userProfile";
import { userLogout } from "../../redux/features/userSlice";
import { leaveLocalGame } from "../../redux/features/localGame";

import "./Profile.scss";
import userAchieve from "../../assets/icons/Medal.png";
import trophy from "../../assets/icons/Trophy.png";
import handShake from "../../assets/icons/Handshake.png";

import Loader from "../../components/UI/loader/Loader";
import Input from "../../components/UI/input/Input";
import ProfileItem from "./components/profileItem/ProfileItem";

import { getPasswodDate, initialState, reducer } from "./helpers";

const Profile = () => {
  const appDispatch = useDispatch();
  const navigation = useNavigate();
  const [state, dispatch] = useReducer(reducer, initialState);
  const { data, isFetching, refetch } = useGetUserProfileQuery();
  const [updateNickname, { isLoading: isLoadingNickname }] =
    useUpdateNicknameMutation();
  const [updateEmail, { isLoading: isLoadingEmail }] = useUpdateEmailMutation();
  const [updatePassword, { isLoading: isLoadingPassword }] =
    useUpdatePasswordMutation();

  const handleUserLogout = async () => {
    await navigation("/");
    appDispatch(userLogout());
    appDispatch(leaveLocalGame());
  };

  useEffect(() => {
    dispatch({ type: "validate" });
  }, [
    state.name.value,
    state.email.value,
    state.password.value,
    state.oldPassword.value,
  ]);
  useEffect(() => {
    refetch();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  if (isFetching || !data) return <Loader content="Loading..." />;

  return (
    <div className="user-profile">
      <h2>Your reiting</h2>
      <div className="user-profile__content">
        <div className="user-profile__statistics">
          <p
            className="user-profile__statistics__raiting"
            data-testid="profile_topRaiting"
          >
            {data.profile.topRaiting}
          </p>
          <div className="user-profile__statistics__achieve">
            <img src={userAchieve} alt="user achieve icon" />
            <p data-testid="profile_raiting">{data.profile.raiting}</p>
          </div>
          <div className="user-profile__statistics__achieve">
            <img src={trophy} alt="user trophy icon" />
            <p data-testid="profile_won">Won: {data.profile.history.userWon}</p>
          </div>
          <div className="user-profile__statistics__achieve">
            <img src={handShake} alt="user handshake icon" />
            <p data-testid="profile_draw">Draw: {data.profile.history.draw}</p>
          </div>
          <div className="user-profile__statistics__achieve">
            <img src={trophy} alt="user achieve icon" />
            <p data-testid="profile_lose">
              Lost: {data.profile.history.userLose}
            </p>
          </div>
        </div>
        <ProfileItem photo={trophy} title={"Photo"}>
          photo
        </ProfileItem>
        <ProfileItem
          click={() => updateNickname(state.name.value)}
          disable={isLoadingNickname}
          data={data.user.name}
          title={"Nickname"}
        >
          <Input
            value={state.name.value}
            placeholder="Enter new nickname"
            dispatch={dispatch}
            dType="name"
            isErr={state.name.isErr}
            data-testid="input_name"
          />
        </ProfileItem>
        <ProfileItem
          click={() => updateEmail(state.email.value)}
          disable={isLoadingEmail}
          data={data.user.email}
          title={"Email address"}
        >
          <Input
            value={state.email.value}
            placeholder="Enter new email"
            dispatch={dispatch}
            dType="email"
            type="email"
            isErr={state.email.isErr}
            data-testid="input_email"
          />
        </ProfileItem>
        <ProfileItem
          click={() =>
            updatePassword({
              oldPassword: state.oldPassword.value,
              newPassword: state.password.value,
            })
          }
          disable={isLoadingPassword}
          data={getPasswodDate(data.user.date)}
          title={"Password"}
        >
          <Input
            label="Old password"
            value={state.oldPassword.value}
            placeholder="Enter old password"
            dispatch={dispatch}
            dType="oldPassword"
            type="password"
            isErr={state.oldPassword.isErr}
            validate={state.oldPassword.validate}
            data-testid="input_password_old"
          />
          <br />
          <Input
            label="New password"
            value={state.password.value}
            placeholder="Enter new password"
            dispatch={dispatch}
            dType="password"
            type="password"
            isErr={state.password.isErr}
            validate={state.password.validate}
            data-testid="input_password_new"
          />
        </ProfileItem>
        <button
          type="button"
          className="user-profile__logout"
          onClick={handleUserLogout}
        >
          Logout
        </button>
      </div>
    </div>
  );
};

export default Profile;
