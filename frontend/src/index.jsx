import React from "react";
import ReactDOM from "react-dom/client";
import { createBrowserRouter } from "react-router-dom";

import "./index.scss";

import store from "./redux/store";
import router from "./routes/root";
import AppProviders from "./AppProviders";

const createdRouter = createBrowserRouter(router);

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(<AppProviders router={createdRouter} store={store} />);
