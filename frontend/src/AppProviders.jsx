import { Provider as StoreProvider } from "react-redux";
import { RouterProvider } from "react-router-dom";

const AppProviders = ({ router, store, children }) => {
  return (
    <StoreProvider store={store}>
      <RouterProvider router={router}>{children}</RouterProvider>
    </StoreProvider>
  );
};

export default AppProviders;
