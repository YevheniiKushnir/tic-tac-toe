import { Navigate } from "react-router-dom";

import authRouter from "./auth.route";
import authenticatedRouter from "./authenticated.route";

import Info from "../pages/info/Info";
import Home from "../pages/home/Home";
import LocalGame from "../pages/game/localGame/LocalGame";

import FullPageErrorFallback from "../components/UI/error/FullPageErrorFallback";
import AppLayout from "../components/layouts/AppLayout/AppLayout";
import GameLayout from "../components/layouts/GameLayout/GameLayout";
import ScrollUp from "../components/scrollUp/ScrollUp";

import InvitingProvider from "../context/invitingContext";

const router = [
  {
    path: "/",
    element: (
      <ScrollUp>
        <InvitingProvider>
          <AppLayout />
        </InvitingProvider>
      </ScrollUp>
    ),
    errorElement: <FullPageErrorFallback />,
    children: [
      {
        index: true,
        element: (
          <GameLayout>
            <Home />
          </GameLayout>
        ),
      },
      {
        path: "terms&conditions",
        element: <Info />,
      },
      {
        path: "game/play-with-robot",
        element: (
          <GameLayout>
            <LocalGame />
          </GameLayout>
        ),
      },
      {
        path: "*",
        element: <Navigate to="/" replace />,
      },
      authenticatedRouter,
      authRouter,
    ],
  },
];

export default router;
