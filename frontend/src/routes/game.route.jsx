import { Outlet } from "react-router-dom";

import OnlineGame from "../pages/game/onlineGame/OnlineGame";

import GameLayout from "../components/layouts/GameLayout/GameLayout";

const gameRouter = {
  path: "game",
  element: (
    <GameLayout>
      <Outlet />
    </GameLayout>
  ),
  children: [
    {
      path: "play-online",
      element: <OnlineGame />,
    },
  ],
};

export default gameRouter;
