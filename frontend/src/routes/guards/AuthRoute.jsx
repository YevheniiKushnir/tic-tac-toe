import { useSelector } from "react-redux";
import { Navigate, Outlet } from "react-router-dom";

const AuthRoute = () => {
  const isAuth = useSelector((state) => state.user.isAuth);
  const userHistory = useSelector((state) => state.localGame.userHistory);
  const saveSessionEnable = useSelector(
    (state) => state.localGame.saveSessionEnable
  );

  if (
    (userHistory.userWon > 0 ||
      userHistory.userLose > 0 ||
      userHistory.draw > 0) &&
    !saveSessionEnable
  ) {
    return (
      <Navigate to="/game/play-with-robot" state={{ activeModal: true }} />
    );
  }

  if (!isAuth)
    return <Navigate to="/" state={{ isUnauthenticated: true }} replace />;

  return <Outlet />;
};

export default AuthRoute;
