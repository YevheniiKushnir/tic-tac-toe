import gameRouter from "./game.route";

import FullPageErrorFallback from "../components/UI/error/FullPageErrorFallback";
import Leader from "../pages/leader/Leader";
import Profile from "../pages/profile/Profile";

import AuthRoute from "./guards/AuthRoute";

const authenticatedRouter = {
  path: "/*",
  element: <AuthRoute />,
  errorElement: <FullPageErrorFallback />,
  children: [
    {
      path: "leader-board",
      element: <Leader />,
    },
    {
      path: "user-profile",
      element: <Profile />,
    },
    gameRouter,
  ],
};

export default authenticatedRouter;
