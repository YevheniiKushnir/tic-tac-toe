import SingIn from "../pages/singIn/SingIn";
import SingUp from "../pages/singUp/SingUp";

import AuthLayout from "../components/layouts/AuthLayout/AuthLayout";

const authRouter = {
  path: "/*",
  element: <AuthLayout />,
  children: [
    {
      path: "sing-up",
      element: <SingUp />,
    },
    {
      path: "sing-in",
      element: <SingIn />,
    },
  ],
};
export default authRouter;
