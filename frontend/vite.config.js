import { resolve } from "path";
import { defineConfig } from "vitest/config";
import react from "@vitejs/plugin-react";

export default defineConfig({
  plugins: [react()],
  server: {
    proxy: {
      "/api": {
        target: "http://localhost:8080/",
        changeOrigin: true,
      },
      "/live-game": {
        target: "http://localhost:8080/live-game",
        changeOrigin: true,
      },
    },
    port: 5173,
    open: true,
  },
  build: {
    outDir: resolve(__dirname, "../backend/client"),
    emptyOutDir: true,
  },
  test: {
    globals: true,
    environment: "jsdom",
    setupFiles: "./src/test/setup.js",
    css: true,
  },
});
